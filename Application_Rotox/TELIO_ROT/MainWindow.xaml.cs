﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using Syncfusion.XlsIO;
using System.Reflection;

namespace TELIO_ROT
{
    public partial class MainWindow : Window
    {
        //-----------------------[STYLE]-----------------------\\

        public void PlaceHolder(object sender, EventArgs e)
        // Supprimer le placeholder à SearchBox.
        {
            if (SearchBox.Text == "Rechercher")
            {
                SearchBox.Text = "";
            }
            SearchBox.Foreground = System.Windows.Media.Brushes.Black;
        }

        public void AddText(object sender, EventArgs e)
        // Ajouter le placeholder à SearchBox.
        {
            if (string.IsNullOrWhiteSpace(SearchBox.Text))
            {
                SearchBox.Text = "Rechercher";
            }
            SearchBox.Foreground = Brushes.Gray;
        }

        //-----------------------[INTERACTION]-----------------------\\

        public class DataChariot
        {
            public string Lot { get; set; }
            public int Nb_Copie { get; set; }
            public string Reference { get; set; }
            public string Longueur { get; set; }
            public string Quantite { get; set; }
            public string Chariot { get; set; }
            public string Case { get; set; }
        }

        public static class Globals
        // Variables globales.
        {
            public static String lot_name = "";
            public static String path_excel = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\Print.xlsx";
            public static List<DataChariot> DataChariot = new List<DataChariot>();
            public static String ROT_PATH = Properties.Settings.Default.path_ROT;
        }

        public MainWindow()
        {
            InitializeComponent();
            // Récupérer les chemins d'accès aux fichiers.
            Text_Path_ROT.Text = Globals.ROT_PATH;

            string path_ROT = Globals.ROT_PATH;

            try
            {
                // Récupérer tous les fichiers ROT présents dans le dossier.
                string[] files = Directory.GetFiles(path_ROT, "*.ROT");
                foreach (string file in files)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
            }

            // Récupérer les imprimantes connectées.
            ComboBox_Printer.Items.Add(Properties.Settings.Default.printer);
            ComboBox_Printer.SelectedItem = Properties.Settings.Default.printer;

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (printer != Properties.Settings.Default.printer)
                    ComboBox_Printer.Items.Add(printer);
            }

        }

        private void Select_Lot(object sender, RoutedEventArgs e)
        // Action lors de la sélection du lot.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                string lot_name = ListBox_Lot.SelectedValue.ToString();
                Globals.lot_name = lot_name;
                string path_ROT = Globals.ROT_PATH;
                string file_ROT = path_ROT + lot_name + ".ROT";
                List<DataChariot> DataChariot = Globals.DataChariot;
                DataChariot.Clear();

                // Lire les fichiers ROT.
                IEnumerable<string> lines_ROT = File.ReadLines(file_ROT);
                int nb_data = 1;
                int nb_copie = 1;
                foreach(string line in lines_ROT)
                {
                    string[] subs = line.Split('%');

                    // Récuperer les données voulues.
                    var list_data = new List<string>();
                    foreach (string data in subs)
                    {
                        if (data.Contains("PB"))
                        {
                            string result = data.Substring(2);
                            list_data.Add(result);
                        }
                        else if (data.Contains("SL"))
                        {
                            string result = data.Substring(2);
                            string longueur = result.Remove(result.Length - 1, 1).Trim();
                            list_data.Add(longueur);
                        }
                        else if (data.Contains("WG"))
                        {
                            string result = data.Substring(2).Trim();
                            list_data.Add(result);
                        }
                        else if (data.Contains("FN"))
                        {
                            string result = data.Substring(2).Trim(' ', 'D');
                            list_data.Add(result);
                        }
                    }
                    if (nb_data > 45)
                    {
                        ++nb_copie;
                        nb_data = 1;
                    }
                    DataChariot.Add(new DataChariot() { Lot = lot_name, Nb_Copie = nb_copie, Reference = list_data[0], Longueur = list_data[1], Quantite = "2", Chariot = list_data[2], Case = list_data[3] });
                    ++nb_data;
                }
                // Accéder à cette liste avec les fonctions Print.
                Globals.DataChariot = DataChariot;
            }
        }

        private void Refresh_button(object sender, RoutedEventArgs e)
        // Refresh l'application.
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void ShowSetting(object sender, RoutedEventArgs e)
        // Afficher les paramètres.
        {
            if (Setting.Visibility == Visibility.Hidden)
                Setting.Visibility = Visibility.Visible;
            else if (Setting.Visibility == Visibility.Visible)
                Setting.Visibility = Visibility.Hidden;
        }

        private void Search_Path_ROT(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers InterHM.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_InterHM_path = folderDlg.SelectedPath + @"\";
            if (new_InterHM_path != @"\")
                Text_Path_ROT.Text = new_InterHM_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers XML.
            if (new_InterHM_path != @"\")
            {
                Properties.Settings.Default.path_ROT = new_InterHM_path;
                Properties.Settings.Default.Save();
            }
        }

        private void New_Printer(object sender, SelectionChangedEventArgs e)
        // Imprimante sélectionnée.
        {
            Properties.Settings.Default.printer = ComboBox_Printer.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        // Barre de recherche pour les lots.
        {
            string word = SearchBox.Text.ToUpper();
            var list_lot_search = new List<string>();

            // Récupérer la liste des lots actuels.
            foreach (object liItem in ListBox_Lot.Items)
            {
                string result = liItem.ToString();
                list_lot_search.Add(result);
            }

            ListBox_Lot.Items.Clear();

            // Afficher les résultats correspondant au lot cherché.
            foreach (string lot in list_lot_search)
            {
                if (lot.Contains(word))
                {
                    ListBox_Lot.Items.Add(lot);
                }
            }
        }

        //-----------------------[IMPRIMER]-----------------------\\

        private void Print_File(object sender, EventArgs e)
        // Imprimer le lot sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                // Récupérer la collection du lot sélectionné.
                List<DataChariot> DataChariot = Globals.DataChariot;
                string lot_name = Globals.lot_name;

                // Créer une instance d'ExcelEngine.
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    // Instancier l'objet d'application Excel.
                    IApplication application = excelEngine.Excel;

                    // Définir la version par défaut de l'application.
                    application.DefaultVersion = ExcelVersion.Excel2016;

                    // Charger le classeur Excel existant dans IWorkbook.
                    IWorkbook workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_ROT.Setup.xlsx"));

                    // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                    IWorksheet worksheet = workbook.Worksheets[0];

                    // Alimenter les cellules.
                    int ligne = 6;
                    int colonne = 2;
                    double copie = Math.Ceiling(DataChariot.Count / 45.0);
                    int nb_copie = Convert.ToInt32(copie);
                    for (int i = 1; i <= copie; i++)
                    {
                        ligne = 6;
                        worksheet.Range["C6:G50"].Text = "";
                        foreach (var data in DataChariot)
                        {
                            if (lot_name == data.Lot)
                                if (i == data.Nb_Copie)
                                {
                                    worksheet[3, 7].Text = i.ToString();
                                    worksheet[3, 8].Text = data.Lot;
                                    worksheet[ligne, 1 + colonne].Text = data.Reference;
                                    worksheet[ligne, 2 + colonne].Text = data.Longueur;
                                    worksheet[ligne, 3 + colonne].Text = data.Quantite;
                                    worksheet[ligne, 4 + colonne].Text = data.Chariot;
                                    worksheet[ligne, 5 + colonne].Text = data.Case;

                                    ++ligne;
                                }
                        }
                        // Enregistrer le document Excel.
                        workbook.SaveAs(Globals.path_excel);

                        // Imprimer le document Excel.                            
                        SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

                        // Charger le fichier à imprimer.
                        ExcelFile workbook_print = ExcelFile.Load(Globals.path_excel);

                        // Paramétrer les options.
                        System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog() { AllowSomePages = true };
                        PrinterSettings printerSettings = printDialog.PrinterSettings;
                        PrintOptions printOptions = new PrintOptions() { SelectionType = SelectionType.EntireFile };

                        // Définissez les propriétés PrintOptions en fonction des propriétés PrinterSettings.
                        printOptions.CopyCount = printerSettings.Copies;
                        printOptions.FromPage = 0;
                        printOptions.ToPage = 0;
                        string PrinterName = ComboBox_Printer.Text;

                        workbook_print.Print(PrinterName, printOptions);
                        
                        // Supprimer le fichier créé.
                        File.Delete(Globals.path_excel);
                    }
                }
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }
    }
}