﻿using System;
using System.Collections.Generic;
using Syncfusion.XlsIO;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Xml;
using Brushes = System.Windows.Media.Brushes;
using Color = System.Windows.Media.Color;
using GemBox.Spreadsheet;
using System.Configuration;
using System.Reflection;

namespace TELIO_CHARIOT
{
    public partial class MainWindow : System.Windows.Window
    {
        //-----------------------[STYLE]-----------------------\\

        public void PlaceHolder(object sender, EventArgs e)
        // Supprimer le placeholder à SearchBox.
        {
            if (SearchBox.Text == "Rechercher")
            {
                SearchBox.Text = "";
            }
            SearchBox.Foreground = System.Windows.Media.Brushes.Black;
        }

        public void AddText(object sender, EventArgs e)
        // Ajouter le placeholder à SearchBox.
        {
            if (string.IsNullOrWhiteSpace(SearchBox.Text))
            {
                SearchBox.Text = "Rechercher";
            }
            SearchBox.Foreground = Brushes.Gray;
        }

        //-----------------------[INTERACTION]-----------------------\\

        public class DataChariot
        {
            public int Carriage { get; set; }
            public string EDN { get; set; }
            public int Chariot { get; set; }
            public int Emplacement { get; set; }
            public string Type { get; set; }
            public string Profil { get; set; }
            public string Position { get; set; }
            public string Casier { get; set; }
            public string Longueur { get; set; }
            public string Ktn { get; set; }
        }

        public class Chariot
        {
            public string Name { get; set; }
            public string Full_Name { get; set; }
            public string Colonne { get; set; }
            public string Ligne { get; set; }
            public string Type { get; set; }
            public int Max_NB_Chariot { get; set; }
            public bool Utiliser { get; set; }
        }
        
        public static class Globals
        // Variables globales.
        {
            public static String lot_name = "";
            public static String InterHM_PATH = Properties.Settings.Default.path_InterHM;
            public static String Ramasoft_PATH = Properties.Settings.Default.path_Ramasoft;
            public static String path_excel = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\Print.xlsx";
            public static List<DataChariot> DataChariot = new List<DataChariot>();
            public static List<Chariot> Chariot = new List<Chariot>();
        }

        public MainWindow()
        {
            InitializeComponent();

            // Récupérer les chariots enregistrés et ses paramètres.
            List<Chariot> Chariot = Globals.Chariot;
            foreach (SettingsProperty PropertyName in Properties.Settings.Default.Properties)
            {
                if (PropertyName.Name.StartsWith("C") == true && PropertyName.Name.Substring(1).All(char.IsDigit) == true)
                {
                    string result = Properties.Settings.Default[PropertyName.Name].ToString();
                    string[] subs = result.Split(';');
                    Chariot.Add(new Chariot() { Name = PropertyName.Name, Colonne = subs[0].Substring(11), Ligne = subs[1].Substring(9), Type = subs[2] });
                }
            }
            Globals.Chariot = Chariot;

            // Récupérer les chemins d'accès aux fichiers.
            Text_Path_InterHM.Text = Globals.InterHM_PATH;
            Text_Path_Ramasoft.Text = Globals.Ramasoft_PATH;

            // Récupérer les imprimantes connectées.
            ComboBox_Printer.Items.Add(Properties.Settings.Default.printer);
            ComboBox_Printer.SelectedItem = Properties.Settings.Default.printer;

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (printer != Properties.Settings.Default.printer)
                    ComboBox_Printer.Items.Add(printer);
            }
        }

        public void Button_Display(System.Windows.Controls.Button Display_Active, System.Windows.Controls.Button Display_Deactivate, Canvas Application_Active, Canvas Application_Deactivate)
        // Afficher la partie de traitement voulue.
        {
            Display_Active.Background = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            Display_Active.Foreground = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));

            Display_Deactivate.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
            Display_Deactivate.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));

            Application_Active.Visibility = Visibility.Visible;
            Application_Deactivate.Visibility = Visibility.Collapsed;

            ListBox_Lot.Items.Clear();

            ListBox_Lot.SelectedIndex = -1;
            ListBox_Lot.Visibility = Visibility.Visible;

            SearchBox.Text = "Rechercher";
        }

        private void Button_InterHM(object sender, RoutedEventArgs e)
        // Afficher la partie InterHM.
        {
            Button_Display(Display_InterHM, Display_Ramasoft, APPLICATION_InterHM, APPLICATION_Ramasoft);

            String path_InterHM = Globals.InterHM_PATH;

            try
            {
                // Récupérer tous les fichiers XML présents dans le dossier.
                string[] files = Directory.GetFiles(path_InterHM, "*.$ET");
                foreach (string file in files)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_InterHM.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_InterHM.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        private void Button_Ramasoft(object sender, RoutedEventArgs e)
        // Afficher la partie Ramasoft.
        {
            Button_Display(Display_Ramasoft, Display_InterHM, APPLICATION_Ramasoft, APPLICATION_InterHM);

            String path_Ramasoft = Globals.Ramasoft_PATH;

            try
            {
                // Récupérer tous les fichiers $ET présents dans le dossier.
                string[] files = Directory.GetFiles(path_Ramasoft, "*.XML");
                foreach (string file in files)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Ramasoft.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Ramasoft.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        public void RemoveLine(string[] subs, List<string> list_line_value)
        // Supprimer les lignes vides.
        {
            foreach (var sub in subs)
            {
                if (sub.Length > 30)
                {
                    list_line_value.Add(sub);
                }
            }
        }

        public void SearchData(List<string> list_search, List<string> list_find)
        // Récuperer les données voulues.
        {
            foreach (string data in list_search)
            {
                if (data.Contains("$027DC"))
                {
                    int pFrom = data.IndexOf("$027DC") + "$027DC".Length;
                    int pTo = data.LastIndexOf("");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027B"))
                {
                    int pFrom = data.IndexOf("*") + "*".Length;
                    int pTo = data.LastIndexOf("*");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("/"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("$027q1");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XM"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XS"))
                {
                    int pFrom = data.IndexOf("$027XS") + "$027XS".Length;
                    int pTo = data.LastIndexOf("");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
            }
        }

        private void Select_Lot(object sender, RoutedEventArgs e)
        // Action lors de la sélection du lot.
        {
            try
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();
                    string path_Ramasoft = Globals.Ramasoft_PATH;
                    string path_InterHM = Globals.InterHM_PATH;
                    Globals.lot_name = lot_name;
                    Globals.DataChariot.Clear();
                    ListBox_Type.Items.Clear();
                    ListBox_Chariot.Items.Clear();

                    List<DataChariot> DataChariot = Globals.DataChariot;
                    List<Chariot> Chariot_Propriete = Globals.Chariot;
                    foreach (var data in Chariot_Propriete)
                    {
                        data.Utiliser = false;
                        data.Max_NB_Chariot = 0;
                    }

                    if (APPLICATION_Ramasoft.Visibility == Visibility.Visible)
                    // Ramasoft.
                    {
                        // Créer un document XML.
                        XmlDocument xmlDocument = new XmlDocument();

                        string file_xml = path_Ramasoft + lot_name + ".XML";

                        // Lire le fichier XML.
                        xmlDocument.Load(file_xml);

                        // Créer une liste de nœuds XML avec l'expression XPath.
                        XmlNodeList nodeList = xmlDocument.SelectNodes("/File/OptiCuttingData/OptiData/BarData/PieceData");

                        // Récuperer les emplacements des pièces et leurs contenus.
                        foreach (XmlNode elem in nodeList)
                        {
                            // Récupérer le contenu de la pièce.
                            string Data;
                            if (lot_name.All(char.IsDigit) == false)
                                Data = elem["LabelPrintData"].InnerText;
                            else
                                Data = elem["PrintData"].InnerText;

                            string[] subs = Data.Split('\n');

                            // Supprimer les lignes vides.
                            var list_line_value = new List<string>();
                            RemoveLine(subs, list_line_value);

                            // Récuperer les données voulues.
                            var list_data = new List<string>();
                            SearchData(list_line_value, list_data);

                            // Récupérer l'emplacement de la pièce.
                            string CarriageNo = elem.Attributes[3].Value;
                            int Carriage = Convert.ToInt32(CarriageNo);

                            string CaseNo = elem.Attributes[4].Value;
                            int Chariot = Convert.ToInt32(CaseNo.Substring(0, 1));
                            if (Convert.ToInt32(CaseNo) == 0)
                            {
                                if (lot_name.All(char.IsDigit) == false)
                                    Chariot = Convert.ToInt32(list_data[16].Substring(0, 1));
                                else
                                    Chariot = Convert.ToInt32(list_data[11].Substring(0, 1));
                            }

                            // Connaître les différents types utilisés et le nombre de chariots.
                            foreach (var data in Chariot_Propriete)
                            {
                                if (Carriage == Convert.ToInt32(data.Name.Substring(1)))
                                {
                                    data.Utiliser = true;
                                    if (Chariot > data.Max_NB_Chariot)
                                    {
                                        data.Max_NB_Chariot = Chariot;
                                    }
                                }
                            }

                            // Emplacement de chaque pièce.
                            int emplacement = 0;
                            if (Convert.ToInt32(CaseNo) != 0)
                            {
                                emplacement = ((Convert.ToInt32(CaseNo.Substring(1, 2)) - 1) * 4) + Convert.ToInt32(CaseNo.Substring(3, 1));
                            }
                            else
                            {
                                if (lot_name.All(char.IsDigit) == false)
                                    emplacement = Convert.ToInt32(list_data[16].Substring(2));
                                else
                                    emplacement = Convert.ToInt32(list_data[11].Substring(2));
                            }

                            // Ajouter les données à la collection.
                            if (Chariot != 0)
                            {
                                if (lot_name.All(char.IsDigit) == false)
                                    DataChariot.Add(new DataChariot() { Carriage = Carriage, Chariot = Chariot, Emplacement = emplacement, Type = list_data[10].Substring(0, 1), Profil = list_data[7], Position = list_data[8], Casier = list_data[10], Longueur = list_data[2], Ktn = list_data[6] });
                                else
                                    DataChariot.Add(new DataChariot() { Carriage = Carriage, Chariot = Chariot, Emplacement = emplacement, Type = list_data[8].Substring(0, 1), Profil = list_data[2], Position = list_data[4], Casier = list_data[8], Longueur = list_data[1], Ktn = list_data[7] });
                            }
                        }
                        // Traiter les cas : CarriageNo = 0.
                        if (DataChariot.Any(p => p.Carriage == 1) == true)
                        {
                            foreach (var data in DataChariot)
                            {
                                if (data.Carriage == 0)
                                    if (data.Type == "O")
                                        data.Carriage = 1;
                                    else if (data.Type == "D")
                                        if (DataChariot.Any(p => p.Carriage == 2) == true)
                                            data.Carriage = 2;
                                        else if (DataChariot.Any(p => p.Carriage == 5) == true)
                                            data.Carriage = 5;
                            }
                        }
                        if (DataChariot.Any(p => p.Carriage == 2) == true)
                        {
                            foreach (var data in DataChariot)
                            {
                                if (data.Carriage == 0)
                                    if (data.Type == "O")
                                        data.Carriage = 1;
                                    else if (data.Type == "D")
                                        data.Carriage = 2;
                            }
                        }
                        if (DataChariot.Any(p => p.Carriage == 5) == true)
                        {
                            foreach (var data in DataChariot)
                            {
                                if (data.Carriage == 0)
                                    if (data.Type == "O")
                                        data.Carriage = 1;
                                    else if (data.Type == "D")
                                        data.Carriage = 5;
                            }
                        }

                        // Accéder à cette liste avec les fonctions Print.
                        Globals.DataChariot = DataChariot;
                        Globals.Chariot = Chariot_Propriete;

                        // Afficher les différents types.
                        foreach (var data in Chariot_Propriete)
                        {
                            if (data.Utiliser == true)
                            {
                                string type = "";
                                if (data.Type == "O")
                                    type = "Ouvrant";
                                else if (data.Type == "D")
                                    type = "Dormant";

                                string cases = (Convert.ToInt32(data.Ligne) * Convert.ToInt32(data.Colonne)).ToString();

                                ListBox_Type.Items.Add(type + " - " + cases + " cases");
                                data.Full_Name = type + " - " + cases + " cases";
                            }
                        }

                        // Gérer l'erreur d'une collection comportant le Carriage 2 et 5.
                        if (DataChariot.Any(p => p.Carriage == 2) == true && DataChariot.Any(p => p.Carriage == 5) == true)
                        {
                            System.Windows.MessageBox.Show("Conflit entre les chariots.\nContactez le service informatique.");
                            ListBox_Type.Items.Clear();
                            ListBox_Chariot.Items.Clear();
                            ListBox_Lot.SelectedIndex = -1;
                        }
                    }
                    else if (APPLICATION_InterHM.Visibility == Visibility.Visible)
                    // InterHM.
                    {
                        string file_et = path_InterHM + lot_name + ".$ET";
                        string file_tx = path_InterHM + lot_name + ".$TX";

                        // Lire les fichiers $ET et $TX.
                        IEnumerable<string> lines_et = File.ReadLines(file_et);
                        IEnumerable<string> lines_tx = File.ReadLines(file_tx);

                        string EDN = "start";
                        var list_find = new List<string>();
                        var list_data = new List<string>();
                        // Récupérer les données voulues.
                        foreach (string line in lines_et)
                        {
                            if (line.Substring(0, 7) == EDN)
                            {
                                list_find.Add(line + " ");
                            }
                            else if (EDN == "start")
                            {
                                EDN = line.Substring(0, 7);
                                DataChariot.Add(new DataChariot() { EDN = EDN });
                            }
                            else
                            {
                                SearchData(list_find, list_data);

                                foreach (var data in DataChariot)
                                {
                                    if (data.EDN == EDN)
                                    {
                                        if (lot_name.Substring(0, 1).All(char.IsDigit) == false)
                                        {
                                            string ktn = "KTN" + list_data[5].Substring(4);
                                            int carriage = 0;
                                            foreach (string link in lines_tx)
                                            {
                                                if (link.Contains(ktn) == true)
                                                {
                                                    carriage = Convert.ToInt32(link.Substring(14, 1));
                                                    data.Carriage = carriage;
                                                    break;
                                                }
                                            }

                                            // Connaître les différents types utilisés et le nombre de chariots.
                                            foreach (var chariot in Chariot_Propriete)
                                            {
                                                if (carriage == Convert.ToInt32(chariot.Name.Substring(1)))
                                                {
                                                    chariot.Utiliser = true;
                                                    if (Convert.ToInt32(list_data[15].Substring(0, 1)) > chariot.Max_NB_Chariot)
                                                    {
                                                        chariot.Max_NB_Chariot = Convert.ToInt32(list_data[15].Substring(0, 1));
                                                    }
                                                }
                                            }

                                            data.Chariot = Convert.ToInt32(list_data[15].Substring(0, 1));
                                            data.Emplacement = Convert.ToInt32(list_data[15].Substring(2));
                                            data.Type = list_data[9].Substring(0, 1);
                                            data.Profil = list_data[6];
                                            data.Position = list_data[7];
                                            data.Casier = list_data[9];
                                            data.Longueur = list_data[1];
                                            data.Ktn = list_data[5];
                                        }
                                        else
                                        {
                                            string ktn = "KTN" + list_data[6].Substring(4);
                                            int carriage = 0;
                                            foreach (string link in lines_tx)
                                            {
                                                if (link.Contains(ktn) == true)
                                                {
                                                    carriage = Convert.ToInt32(link.Substring(14, 1));
                                                    data.Carriage = carriage;
                                                    break;
                                                }
                                            }


                                            // Connaître les différents types utilisés et le nombre de chariots.
                                            foreach (var chariot in Chariot_Propriete)
                                            {
                                                if (carriage == Convert.ToInt32(chariot.Name.Substring(1)))
                                                {
                                                    chariot.Utiliser = true;
                                                    if (Convert.ToInt32(list_data[10].Substring(0, 1)) > chariot.Max_NB_Chariot)
                                                    {
                                                        chariot.Max_NB_Chariot = Convert.ToInt32(list_data[10].Substring(0, 1));
                                                    }
                                                }
                                            }

                                            data.Chariot = Convert.ToInt32(list_data[10].Substring(0, 1));
                                            data.Emplacement = Convert.ToInt32(list_data[10].Substring(2));
                                            data.Type = list_data[7].Substring(0, 1);
                                            data.Profil = list_data[1];
                                            data.Position = list_data[3];
                                            data.Casier = list_data[7];
                                            data.Longueur = list_data[0];
                                            data.Ktn = list_data[6];
                                        }
                                    }
                                }
                                list_find.Clear();
                                list_data.Clear();
                                EDN = line.Substring(0, 7);
                                DataChariot.Add(new DataChariot() { EDN = EDN });
                            }
                        }
                        SearchData(list_find, list_data);
                        foreach (var data in DataChariot)
                        {
                            if (data.EDN == EDN)
                            {
                                if (lot_name.Substring(0, 1).All(char.IsDigit) == false)
                                {
                                    string ktn = "KTN" + list_data[5].Substring(4);
                                    foreach (string link in lines_tx)
                                    {
                                        if (link.Contains(ktn) == true)
                                        {
                                            data.Carriage = Convert.ToInt32(link.Substring(14, 1));
                                        }
                                    }

                                    data.Chariot = Convert.ToInt32(list_data[15].Substring(0, 1));
                                    data.Emplacement = Convert.ToInt32(list_data[15].Substring(2));
                                    data.Type = list_data[9].Substring(0, 1);
                                    data.Profil = list_data[6];
                                    data.Position = list_data[7];
                                    data.Casier = list_data[9];
                                    data.Longueur = list_data[1];
                                    data.Ktn = list_data[5];
                                }
                                else
                                {
                                    string ktn = "KTN" + list_data[6].Substring(4);
                                    foreach (string link in lines_tx)
                                    {
                                        if (link.Contains(ktn) == true)
                                        {
                                            data.Carriage = Convert.ToInt32(link.Substring(14, 1));
                                        }
                                    }

                                    data.Chariot = Convert.ToInt32(list_data[10].Substring(0, 1));
                                    data.Emplacement = Convert.ToInt32(list_data[10].Substring(2));
                                    data.Type = list_data[7].Substring(0, 1);
                                    data.Profil = list_data[1];
                                    data.Position = list_data[3];
                                    data.Casier = list_data[7];
                                    data.Longueur = list_data[0];
                                    data.Ktn = list_data[6];
                                }
                            }
                        }
                        list_find.Clear();
                        list_data.Clear();

                        Globals.DataChariot = DataChariot;
                        Globals.Chariot = Chariot_Propriete;

                        // Afficher les différents types.
                        foreach (var data in Chariot_Propriete)
                        {
                            if (data.Utiliser == true)
                            {
                                string type = "";
                                if (data.Type == "O")
                                    type = "Ouvrant";
                                else if (data.Type == "D")
                                    type = "Dormant";

                                string cases = (Convert.ToInt32(data.Ligne) * Convert.ToInt32(data.Colonne)).ToString();

                                ListBox_Type.Items.Add(type + " - " + cases + " cases");
                                data.Full_Name = type + " - " + cases + " cases";
                            }
                        }
                    }
                }
            }
            catch { System.Windows.MessageBox.Show("Erreur lors de la sélection du lot"); }
        }

        private void Select_Type(object sender, RoutedEventArgs e)
        // Action lors de la sélection du type.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                ListBox_Chariot.SelectedIndex = -1;
                ListBox_Chariot.Items.Clear();
                if (ListBox_Type.SelectedValue != null)
                {
                    string type_name = ListBox_Type.SelectedValue.ToString();
                    List<Chariot> Chariot_Propriete = Globals.Chariot;

                    int nb_chariot = 0;

                    foreach (var data in Chariot_Propriete)
                    {
                        if (type_name == data.Full_Name)
                            nb_chariot = data.Max_NB_Chariot;
                    }

                    // Afficher le nombre de chariot.
                    int num_chariot = 1;

                    for (int i = 0; i <= nb_chariot; i++)
                    {
                        if (nb_chariot >= num_chariot)
                        {
                            ListBox_Chariot.Items.Add("Chariot n°" + num_chariot);
                            ++num_chariot;
                        }
                    }
                }
            }
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        // Barre de recherche pour les lots.
        {
            ListBox_Type.Items.Clear();
            ListBox_Chariot.Items.Clear();
            string word = SearchBox.Text.ToUpper();
            var list_lot_search = new List<string>();

            // Récupérer la liste des lots actuels.
            foreach (object liItem in ListBox_Lot.Items)
            {
                string result = liItem.ToString();
                list_lot_search.Add(result);
            }

            ListBox_Lot.Items.Clear();

            // Afficher les résultats correspondant au lot cherché.
            foreach (string lot in list_lot_search)
            {
                if (lot.Contains(word))
                {
                    ListBox_Lot.Items.Add(lot);
                }
            }
        }

        private void Notice(object sender, RoutedEventArgs e)
        // Renvoyer sur la notice.
        {
            String openPDFFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\notice_application_chariot.pdf";
            System.IO.File.WriteAllBytes(openPDFFile, global::TELIO_CHARIOT.Properties.Resources.notice_application_chariot);            
            System.Diagnostics.Process.Start(openPDFFile);
        }

        private void Refresh_button(object sender, RoutedEventArgs e)
        // Refresh l'application.
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void ShowSetting(object sender, RoutedEventArgs e)
        // Afficher les paramètres.
        {
            if (Setting.Visibility == Visibility.Hidden)
                Setting.Visibility = Visibility.Visible;
            else if (Setting.Visibility == Visibility.Visible)
                Setting.Visibility = Visibility.Hidden;
        }

        private void Search_Path_InterHM(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers InterHM.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_InterHM_path = folderDlg.SelectedPath + @"\";
            if (new_InterHM_path != @"\")
                Text_Path_InterHM.Text = new_InterHM_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers XML.
            if (new_InterHM_path != @"\")
            {
                Properties.Settings.Default.path_InterHM = new_InterHM_path;
                Properties.Settings.Default.Save();
            }
        }

        private void Search_Path_Ramasoft(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers Ramasoft.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_Ramasoft_path = folderDlg.SelectedPath + @"\";
            if (new_Ramasoft_path != @"\")
                Text_Path_Ramasoft.Text = new_Ramasoft_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers XML.
            if (new_Ramasoft_path != @"\")
            {
                Properties.Settings.Default.path_Ramasoft = new_Ramasoft_path;
                Properties.Settings.Default.Save();
            }
        }

        private void New_Printer(object sender, SelectionChangedEventArgs e)
        // Imprimante sélectionnée.
        {
            Properties.Settings.Default.printer = ComboBox_Printer.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        //-----------------------[IMPRIMER]-----------------------\\

        public void Print()
        {
            // Imprimer le document Excel.                            
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            // Charger le fichier à imprimer.
            ExcelFile workbook_print = ExcelFile.Load(Globals.path_excel);

            // Paramétrer les options.
            System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog() { AllowSomePages = true };
            PrinterSettings printerSettings = printDialog.PrinterSettings;
            PrintOptions printOptions = new PrintOptions() { SelectionType = SelectionType.EntireFile };

            // Définissez les propriétés PrintOptions en fonction des propriétés PrinterSettings.
            printOptions.CopyCount = printerSettings.Copies;
            printOptions.FromPage = 0;
            printOptions.ToPage = 0;
            string PrinterName = ComboBox_Printer.Text;

            workbook_print.Print(PrinterName, printOptions);

            // Supprimer le fichier créé.
            File.Delete(Globals.path_excel);
        }
        private void Print_Lot(object sender, EventArgs e)
        // Imprimer le lot sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                // Récupérer la collection du lot sélectionné.
                List<DataChariot> DataChariot = Globals.DataChariot;
                string lot_name = Globals.lot_name;
                List<Chariot> Chariot_Propriete = Globals.Chariot;

                // Créer une instance d'ExcelEngine.
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    // Instancier l'objet d'application Excel.
                    IApplication application = excelEngine.Excel;

                    // Définir la version par défaut de l'application.
                    application.DefaultVersion = ExcelVersion.Excel2016;

                    foreach (var data in Chariot_Propriete)
                    {
                        if (data.Utiliser == true)
                        {
                            if (Convert.ToInt32(data.Colonne) * Convert.ToInt32(data.Ligne) == 80)
                            {
                                // Charger le classeur Excel existant dans IWorkbook.
                                IWorkbook workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup80.xlsx"));
                                int alignement = 4;
                                int nb_colonne = 8;
                                int ligne = 0;
                                int colonne = 0;

                                // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                                IWorksheet worksheet = workbook.Worksheets[0];

                                // Définir le nombre de chariot à imprimer.
                                int nb_chariot = data.Max_NB_Chariot;

                                // Imprimer chaque chariot.
                                for (int i = 1; i <= nb_chariot; i++)
                                {
                                    worksheet.Range["A5:P14"].Text = "";
                                    // Alimenter les cellules.
                                    worksheet.Range["I2"].Text = lot_name;
                                    worksheet.Range["I3"].Text = i.ToString();
                                    worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                                    if (data.Type == "O")
                                        worksheet.Range["O3"].Text = "Ouvrant";
                                    else if (data.Type == "D")
                                        worksheet.Range["O3"].Text = "Dormant";

                                    foreach (var DATA in DataChariot)
                                    {
                                        if (DATA.Type == data.Type)
                                            if (DATA.Chariot == i)
                                            {
                                                double double_row = Math.Ceiling(DATA.Emplacement / Convert.ToDouble(nb_colonne));
                                                ligne = Convert.ToInt32(double_row);

                                                double double_nb_column = DATA.Emplacement - (Math.Floor(DATA.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                                if (double_nb_column == 0)
                                                    double_nb_column = nb_colonne;
                                                colonne = Convert.ToInt32(double_nb_column);

                                                worksheet[ligne + 4, colonne + alignement].Text = DATA.Emplacement + " - " + DATA.Profil + "\n" + DATA.Position + ", " + DATA.Casier + "\n" + DATA.Longueur + "\n" + DATA.Ktn;
                                            }
                                    }

                                    // Enregistrer le document Excel.
                                    workbook.SaveAs(Globals.path_excel);
                                    Print();
                                }
                            }
                            else if (Convert.ToInt32(data.Colonne) * Convert.ToInt32(data.Ligne) == 160)
                            {
                                // Charger le classeur Excel existant dans IWorkbook.
                                IWorkbook workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup160.xlsx"));
                                int alignement = 0;
                                int nb_colonne = 16;
                                int ligne = 0;
                                int colonne = 0;

                                // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                                IWorksheet worksheet = workbook.Worksheets[0];

                                // Définir le nombre de chariot à imprimer.
                                int nb_chariot = data.Max_NB_Chariot;

                                // Imprimer chaque chariot.
                                for (int i = 1; i <= nb_chariot; i++)
                                {
                                    worksheet.Range["A5:P14"].Text = "";
                                    // Alimenter les cellules.
                                    worksheet.Range["I2"].Text = lot_name;
                                    worksheet.Range["I3"].Text = i.ToString();
                                    worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                                    if (data.Type == "O")
                                        worksheet.Range["O3"].Text = "Ouvrant";
                                    else if (data.Type == "D")
                                        worksheet.Range["O3"].Text = "Dormant";

                                    foreach (var DATA in DataChariot)
                                    {
                                        if (DATA.Type == data.Type)
                                            if (DATA.Chariot == i)
                                            {
                                                double double_row = Math.Ceiling(DATA.Emplacement / Convert.ToDouble(nb_colonne));
                                                ligne = Convert.ToInt32(double_row);

                                                double double_nb_column = DATA.Emplacement - (Math.Floor(DATA.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                                if (double_nb_column == 0)
                                                    double_nb_column = nb_colonne;
                                                colonne = Convert.ToInt32(double_nb_column);

                                                worksheet[ligne + 4, colonne + alignement].Text = DATA.Emplacement + " - " + DATA.Profil + "\n" + DATA.Position + ", " + DATA.Casier + "\n" + DATA.Longueur + "\n" + DATA.Ktn;
                                            }
                                    }

                                    // Enregistrer le document Excel.
                                    workbook.SaveAs(Globals.path_excel);
                                    Print();
                                }
                            }
                        }
                    }
                }
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }

        private void Print_Type(object sender, RoutedEventArgs e)
        // Imprimer le type du lot sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                if (ListBox_Type.SelectedValue != null)
                {
                    // Récupérer la collection du lot sélectionné.
                    List<DataChariot> DataChariot = Globals.DataChariot;
                    string lot_name = Globals.lot_name;
                    string type_name = ListBox_Type.SelectedItem.ToString();
                    string type = "";
                    if (type_name.Contains("Ouvrant") == true)
                        type = "O";
                    else if (type_name.Contains("Dormant") == true)
                        type = "D";
                    List<Chariot> Chariot_Propriete = Globals.Chariot;

                    // Créer une instance d'ExcelEngine.
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        // Instancier l'objet d'application Excel.
                        IApplication application = excelEngine.Excel;

                        // Définir la version par défaut de l'application.
                        application.DefaultVersion = ExcelVersion.Excel2016;

                        // Charger le classeur Excel existant dans IWorkbook.
                        IWorkbook workbook;
                        int alignement = 0;
                        int nb_colonne = 0;
                        int ligne = 0;
                        int colonne = 0;
                        if (ListBox_Type.SelectedValue.ToString().Contains("80 cases") == true)
                        {
                            workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup80.xlsx"));
                            alignement = 4;
                            nb_colonne = 8;
                        }
                        else
                        {
                            workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup160.xlsx"));
                            nb_colonne = 16;
                        }

                        // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                        IWorksheet worksheet = workbook.Worksheets[0];

                        // Définir le nombre de chariot à imprimer.
                        int nb_chariot = 0;

                        foreach (var data in Chariot_Propriete)
                        {
                            if (data.Full_Name == type_name)
                                nb_chariot = data.Max_NB_Chariot;
                        }

                        // Imprimer chaque chariot.
                        for (int i = 1; i <= nb_chariot; i++)
                        {
                            worksheet.Range["A5:P14"].Text = "";
                            // Alimenter les cellules.
                            worksheet.Range["I2"].Text = lot_name;
                            worksheet.Range["I3"].Text = i.ToString();
                            worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                            if (type == "O")
                                worksheet.Range["O3"].Text = "Ouvrant";
                            else if (type == "D")
                                worksheet.Range["O3"].Text = "Dormant";

                            foreach (var data in DataChariot)
                            {
                                if (data.Type == type)
                                    if (data.Chariot == i)
                                    {
                                        double double_row = Math.Ceiling(data.Emplacement / Convert.ToDouble(nb_colonne));
                                        ligne = Convert.ToInt32(double_row);

                                        double double_nb_column = data.Emplacement - (Math.Floor(data.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                        if (double_nb_column == 0)
                                            double_nb_column = nb_colonne;
                                        colonne = Convert.ToInt32(double_nb_column);

                                        worksheet[ligne + 4, colonne + alignement].Text = data.Emplacement + " - " + data.Profil + "\n" + data.Position + ", " + data.Casier + "\n" + data.Longueur + "\n" + data.Ktn;
                                    }
                            }

                            // Enregistrer le document Excel.
                            workbook.SaveAs(Globals.path_excel);
                            Print();
                        }
                    }
                }
                else
                    System.Windows.MessageBox.Show("Aucun type sélectionné");
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }

        private void Print_Chariot(object sender, RoutedEventArgs e)
        // Imprimer le numéro de chariot sélectionné du type sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                if (ListBox_Type.SelectedValue != null)
                {
                    if (ListBox_Chariot.SelectedValue != null)
                    {
                        // Récupérer la collection du lot sélectionné.
                        List<DataChariot> DataChariot = Globals.DataChariot;
                        string lot_name = Globals.lot_name;
                        string type_name = ListBox_Type.SelectedItem.ToString();
                        string type = "";
                        if (type_name.Contains("Ouvrant") == true)
                            type = "O";
                        else if (type_name.Contains("Dormant") == true)
                            type = "D";

                        // Créer une instance d'ExcelEngine.
                        using (ExcelEngine excelEngine = new ExcelEngine())
                        {
                            // Instancier l'objet d'application Excel.
                            IApplication application = excelEngine.Excel;

                            // Définir la version par défaut de l'application.
                            application.DefaultVersion = ExcelVersion.Excel2016;

                            // Charger le classeur Excel existant dans IWorkbook.
                            IWorkbook workbook;
                            int alignement = 0;
                            int nb_colonne = 0;
                            int ligne = 0;
                            int colonne = 0;
                            if (ListBox_Type.SelectedValue.ToString().Contains("80 cases") == true)
                            {
                                workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup80.xlsx"));
                                alignement = 4;
                                nb_colonne = 8;
                            }
                            else
                            {
                                workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup160.xlsx"));
                                nb_colonne = 16;
                            }

                            // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                            IWorksheet worksheet = workbook.Worksheets[0];

                            // Définir le nombre de chariot sélectionné et les imprimer.
                            foreach (string item in ListBox_Chariot.SelectedItems)
                            {
                                int nb_chariot = Convert.ToInt32(item.Substring(10));

                                worksheet.Range["A5:P14"].Text = "";
                                // Alimenter les cellules.
                                worksheet.Range["I2"].Text = lot_name;
                                worksheet.Range["I3"].Text = nb_chariot.ToString();
                                worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                                if (type == "O")
                                    worksheet.Range["O3"].Text = "Ouvrant";
                                else if (type == "D")
                                    worksheet.Range["O3"].Text = "Dormant";

                                foreach (var data in DataChariot)
                                {
                                    if (data.Type == type)
                                        if (data.Chariot == nb_chariot)
                                        {
                                            double double_row = Math.Ceiling(data.Emplacement / Convert.ToDouble(nb_colonne));
                                            ligne = Convert.ToInt32(double_row);

                                            double double_nb_column = data.Emplacement - (Math.Floor(data.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                            if (double_nb_column == 0)
                                                double_nb_column = nb_colonne;
                                            colonne = Convert.ToInt32(double_nb_column);

                                            worksheet[ligne + 4, colonne + alignement].Text = data.Emplacement + " - " + data.Profil + "\n" + data.Position + ", " + data.Casier + "\n" + data.Longueur + "\n" + data.Ktn;
                                        }
                                }

                                // Enregistrer le document Excel.
                                workbook.SaveAs(Globals.path_excel);
                                Print();
                            }
                        }
                    }
                    else
                        System.Windows.MessageBox.Show("Aucun chariot sélectionné");
                }
                else
                    System.Windows.MessageBox.Show("Aucun type sélectionné");
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }
    }
}