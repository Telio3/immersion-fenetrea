﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Windows.Media;
using MessageBox = System.Windows.MessageBox;

namespace TELIO_ETIQUETTE
{ public partial class MainWindow : Window
    {
        //-----------------------[STYLE]-----------------------\\

        public void PlaceHolder(object sender, EventArgs e)
        // Supprimer le placeholder à SearchBox.
        {
            if (SearchBox.Text == "Rechercher")
            {
                SearchBox.Text = "";
            }
            SearchBox.Foreground = Brushes.Black;
        }

        public void AddText(object sender, EventArgs e)
        // Ajouter le placeholder à SearchBox.
        {
            if (string.IsNullOrWhiteSpace(SearchBox.Text))
            {
                SearchBox.Text = "Rechercher";
            }
            SearchBox.Foreground = Brushes.Gray;
        }

        //-----------------------[INTERACTION]-----------------------\\

        public class AllData
        // Constructeur DataGrid.
        {
            public bool Choisi { get; set; }

            public string Casier { get; set; }

            public string Position { get; set; }

            public string Ktn { get; set; }

            public string Chariot { get; set; }

            public string Repere { get; set; }

            public string DM { get; set; }

            public string CB { get; set; }

            public string Lg { get; set; }

            public string Lot { get; set; }

            public string Couleur { get; set; }

            public string Profil { get; set; }

            public string Info4 { get; set; }

            public string Nb_CB { get; set; }

            public string Renfort { get; set; }

            public string Info1 { get; set; }

            public string Info2 { get; set; }

            public string Info3 { get; set; }
        }

        public static class Globals
        // Variables globales.
        {
            public static String lot_name = "";
            public static String Sector = Properties.Settings.Default.sector;
            public static String PRINCIPAL_PATH_PVC = Properties.Settings.Default.path_principale_pvc;
            public static String COMPLEMENTAIRE_PATH_PVC = Properties.Settings.Default.path_complementaire_pvc;
            public static String SILOG_PATH_PVC = Properties.Settings.Default.path_silog_pvc;
            public static String PRINCIPAL_PATH_ALU = Properties.Settings.Default.path_principal_alu;
            public static String COMPLEMENTAIRE_PATH_ALU = Properties.Settings.Default.path_complementaire_alu;
            public static String PRINCIPAL_PATH = "";
            public static String COMPLEMENTAIRE_PATH = "";
            public static String SILOG_PATH = "";
        }

        public MainWindow()
        // Méthode au lancement de l'application.
        {
            InitializeComponent();

            // Définir les paramètres du secteur actuel.
            String sector = Globals.Sector.ToString();
            if (sector == PVC.ToString())
            {
                ComboBox_Sector.SelectedItem = PVC;
                Text_Path_principal.Text = Globals.PRINCIPAL_PATH_PVC;
                Text_Path_complementaire.Text = Globals.COMPLEMENTAIRE_PATH_PVC;
                Text_Path_silog.Text = Globals.SILOG_PATH_PVC;
                Globals.PRINCIPAL_PATH = Globals.PRINCIPAL_PATH_PVC;
                Globals.COMPLEMENTAIRE_PATH = Globals.COMPLEMENTAIRE_PATH_PVC;
                Globals.SILOG_PATH = Globals.SILOG_PATH_PVC;

                border_settings.Height = 450;
                path_silog.Visibility = Visibility.Visible;
                Thickness margin = bottom_settings.Margin;
                margin.Top = 80;
                bottom_settings.Margin = margin;

                Display_Principaux.Width = 102;
                Canvas.SetLeft(Display_Principaux, 26);

                Display_Complementaires.Width = 136;
                Canvas.SetLeft(Display_Complementaires, 128);

                Display_Silog.Visibility = Visibility.Visible;
            }
            else if (sector == ALU.ToString())
            {
                ComboBox_Sector.SelectedItem = ALU;
                Text_Path_principal.Text = Globals.PRINCIPAL_PATH_ALU;
                Text_Path_complementaire.Text = Globals.COMPLEMENTAIRE_PATH_ALU;
                Globals.PRINCIPAL_PATH = Globals.PRINCIPAL_PATH_ALU;
                Globals.COMPLEMENTAIRE_PATH = Globals.COMPLEMENTAIRE_PATH_ALU;

                border_settings.Height = 372;

                Display_Principaux.Width = 154;
                Canvas.SetLeft(Display_Principaux, 26);

                Display_Complementaires.Width = 155;
                Canvas.SetLeft(Display_Complementaires, 180);
            }

            // Récupérer les imprimantes connectées.
            ComboBox_Printer.Items.Add(Properties.Settings.Default.printer);
            ComboBox_Printer.SelectedItem = Properties.Settings.Default.printer;

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (printer.ToUpper().Contains("Z") && printer.ToUpper().Contains("B"))
                {
                    if (printer != Properties.Settings.Default.printer)
                    {
                        ComboBox_Printer.Items.Add(printer);
                    }
                }
            }
        }

        public void Button_Display(System.Windows.Controls.Button Display_Active, System.Windows.Controls.Button Display_Deactivate_1, System.Windows.Controls.Button Display_Deactivate_2, Canvas Application_Active, Canvas Application_Deactivate_1, Canvas Application_Deactivate_2)
        // Afficher la partie de traitement voulue.
        {
            Display_Active.Background = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            Display_Active.Foreground = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));

            Display_Deactivate_1.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
            Display_Deactivate_1.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            
            Application_Active.Visibility = Visibility.Visible;
            Application_Deactivate_1.Visibility = Visibility.Collapsed;

            String sector = Globals.Sector.ToString();
            if (sector == PVC.ToString())
            {
                Display_Deactivate_2.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Deactivate_2.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));

                Application_Deactivate_2.Visibility = Visibility.Collapsed;
            }

            ListBox_Lot.Items.Clear();
            ListBox_Profil.Items.Clear();
            Grid_AllData.Columns.Clear();

            ListBox_Lot.SelectedIndex = -1;
            ListBox_Lot.Visibility = Visibility.Visible;
            Grid_AllData.Visibility = Visibility.Collapsed;
            Rect_grid.Visibility = Visibility.Collapsed;

            SearchBox.Text = "Rechercher";
        }

        private void Button_Principal(object sender, RoutedEventArgs e)
        // Afficher la partie des débits principaux.
        {
            Button_Display(Display_Principaux, Display_Complementaires, Display_Silog, APPLICATION_PRINCIPAL, APPLICATION_COMPLEMENTAIRE, APPLICATION_SILOG);

            String path_principal = Globals.PRINCIPAL_PATH;

            try
            {
                string[] files_XML = Directory.GetFiles(path_principal, "*.XML");

                // Récupérer tous les fichiers XML présent dans le dossier.
                foreach (string file in files_XML)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    if (fileWExt.All(char.IsDigit) == false)
                        ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Principaux.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Principaux.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        private void Button_Complementaire(object sender, RoutedEventArgs e)
        // Afficher la partie des débits complémentaires.
        {
            Button_Display(Display_Complementaires, Display_Principaux, Display_Silog, APPLICATION_COMPLEMENTAIRE, APPLICATION_PRINCIPAL, APPLICATION_SILOG);

            String path_complementaire = Globals.COMPLEMENTAIRE_PATH;

            try
            {
                string[] files_AGS = Directory.GetFiles(path_complementaire, "*.AGS");

                // Récupérer tous les fichiers AGS présent dans le dossier.
                foreach (string file in files_AGS)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Complementaires.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Complementaires.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        private void Button_Silog(object sender, RoutedEventArgs e)
        // Afficher la partie des débits silog.
        {
            Button_Display(Display_Silog, Display_Principaux, Display_Complementaires, APPLICATION_SILOG, APPLICATION_PRINCIPAL, APPLICATION_COMPLEMENTAIRE);

            String path_silog = Globals.SILOG_PATH;

            try
            {
                string[] files_AGS = Directory.GetFiles(path_silog, "*.AGS");

                // Récupérer tous les fichiers AGS présent dans le dossier.
                foreach (string file in files_AGS)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    if (fileWExt.All(char.IsDigit) == true)
                        ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Silog.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Silog.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        public void SearchData(List<string> list_search, List<string> list_find)
        // Récuperer les données voulues.
        {
            foreach (string data in list_search)
            {
                if (data.Contains("$027DC"))
                {
                    int pFrom = data.IndexOf("$027DC") + "$027DC".Length;
                    int pTo = data.LastIndexOf("");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027B"))
                {
                    int pFrom = data.IndexOf("*") + "*".Length;
                    int pTo = data.LastIndexOf("*");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("/"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("$027q1");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XM"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XS"))
                {
                    int pFrom = data.IndexOf("$027XS") + "$027XS".Length;
                    int pTo = data.LastIndexOf("");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
            }
            for (int i = 0; i < list_find.Count; i++)
            {
                if (list_find[i].Contains("$"))
                    list_find[i] = "";
            }
        }

        public void SearchData_ALU(string[] list_search, List<string> list_find)
        // Récuperer les données voulues.
        {
            foreach (string data in list_search)
            {
                if (data.Contains("cas="))
                {
                    int pFrom = data.IndexOf("cas=") + "cas=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("pos="))
                {
                    int pFrom = data.IndexOf("pos=") + "pos=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("Ktn="))
                {
                    int pFrom = data.IndexOf("Ktn=") + "Ktn=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("."))
                {
                    int pFrom = data.IndexOf("FD") + "FD".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("rep="))
                {
                    int pFrom = data.IndexOf("rep=") + "rep=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("prf="))
                {
                    int pFrom = data.IndexOf("prf=") + "prf=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
            }
            list_find[5] = "ktn=" + list_find[5];
        }

        public void SearchData_AGS(string line, List<string> list_data)
        // Récuperer les données voulues.
        {

            list_data.Add(line.Split(new string[] { "T,CAS2," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,COTE," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "L," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,WAG1," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,REP," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            if (line.Contains("T,CB1,"))
                list_data.Add(line.Split(new string[] { "T,CB1," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            else
                list_data.Add("");
            list_data.Add(line.Split(new string[] { "T,CB," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,LP1," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,LOT," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "TYP," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "TYP," }, StringSplitOptions.None)[1].Split(',')[0].Trim());
            list_data.Add("");
            list_data.Add(line.Split(new string[] { "T,CB," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,LRO," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,INF," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add("");
            list_data.Add("");

            // Séparer la couleur du profil.
            int pFrom = list_data[9].IndexOf(",") + ",".Length;
            int pTo = list_data[9].LastIndexOf("") + 1;
            string result = list_data[9].Substring(pFrom, pTo - pFrom);
            list_data[9] = result;

            if (list_data[13] != "")
            {
                list_data[13] = list_data[13].Substring(0, list_data[13].Length - 1) + "." + list_data[13].Substring(list_data[13].Length - 1, 1);
            }
                
            list_data[2] = "ktn=" + list_data[2];
            list_data[7] = "Lg=" + list_data[7];
            list_data[13] = "Rf" + list_data[13];
        }

        public void RemoveLine(string[] subs, List<string> list_line_value)
        // Supprimer les lignes vides.
        {
            foreach (var sub in subs)
            {
                if (sub.Length > 30)
                {
                    list_line_value.Add(sub);
                }
            }
        }

        private void Select_Lot(object sender, RoutedEventArgs e)
        // Action lors de la sélection du lot.
        {
            ListBox_Profil.Items.Clear();
            if (Grid_AllData.Items.Count > 0)
            {
                Grid_AllData.ItemsSource = null;
                Grid_AllData.Items.Refresh();
            }
            Grid_AllData.Columns.Clear();
            ListBox_Profil.Visibility = Visibility.Visible;
            Grid_AllData.Visibility = Visibility.Collapsed;
            Rect_grid.Visibility = Visibility.Collapsed;

            if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();

                    String path_xml = Globals.PRINCIPAL_PATH;
                    Globals.lot_name = lot_name;

                    // Créer un document XML.
                    XmlDocument xmlDocument = new XmlDocument();

                    string file_xml = path_xml + lot_name + ".XML";

                    // Lire le fichier XML.
                    xmlDocument.Load(file_xml);

                    // Créer une liste de nœuds XML avec l'expression XPath.
                    string path_balise;

                    if (lot_name.All(char.IsDigit) == false)
                        path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                    else
                        path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                    XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                    var profil_name_duplicate = new List<string>();

                    // Parcourir chaque étiquette du fichier XML.
                    foreach (XmlNode xmlNode in xmlNodeList)
                    {
                        string Data = xmlNode["PrintData"].InnerText;
                        string[] subs = Data.Split('\n');

                        if (ComboBox_Sector.SelectedItem == PVC)
                        {
                            var list_line_value = new List<string>();

                            // Supprimer les lignes vides.
                            RemoveLine(subs, list_line_value);

                            // Récupérer les références profilées.
                            string line_profil_name;

                            if (lot_name.All(char.IsDigit) == false)
                                line_profil_name = list_line_value[7];
                            else
                                line_profil_name = list_line_value[2];

                            int pFrom = line_profil_name.IndexOf("$027XS") + "$027XS".Length;
                            int pTo = line_profil_name.LastIndexOf("");
                            String result = line_profil_name.Substring(pFrom, pTo - pFrom);
                            profil_name_duplicate.Add(result);
                        }
                        else if (ComboBox_Sector.SelectedItem == ALU)
                        {
                            int pFrom = subs[10].IndexOf("prf=") + "prf=".Length;
                            int pTo = subs[10].LastIndexOf("^FS");
                            String result = subs[10].Substring(pFrom, pTo - pFrom);
                            profil_name_duplicate.Add(result);
                        }
                    }

                    // Supprimer les doublons.
                    List<string> profil_name_whitout_duplicate = profil_name_duplicate.Distinct().ToList();

                    // Ajouter les profils dans la ListBox.
                    foreach (string profil_name in profil_name_whitout_duplicate)
                    {
                        ListBox_Profil.Items.Add(profil_name);
                    }
                }
            }
            else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible || APPLICATION_SILOG.Visibility == Visibility.Visible)
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();

                    String path_ags = "";
                    if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.COMPLEMENTAIRE_PATH;
                    }
                    else if (APPLICATION_SILOG.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.SILOG_PATH;
                    }
                    Globals.lot_name = lot_name;

                    string file_ags = path_ags + lot_name + ".AGS";

                    // Lire le fichier AGS.
                    IEnumerable<string> lines = File.ReadLines(file_ags);

                    var list_profil_duplicate = new List<string>();

                    // Récupérer les références profilées.
                    foreach (string line in lines)
                    {
                        string result = line.Split(new string[] { "TYP," }, StringSplitOptions.None)[1].Split(',')[0].Trim();
                        list_profil_duplicate.Add(result);
                    }

                    // Supprimer les doublons.
                    List<string> profil_name_whitout_duplicate = list_profil_duplicate.Distinct().ToList();

                    // Ajouter les profils dans la ListBox.
                    foreach (string profil_name in profil_name_whitout_duplicate)
                    {
                        ListBox_Profil.Items.Add(profil_name);
                    }
                }
            }
        }

        private void Select_Profil(object sender, RoutedEventArgs e)
        // Action lors de la sélection du profil.
        {
            Grid_AllData.Visibility = Visibility.Visible;
            Rect_grid.Visibility = Visibility.Visible;
            Grid_AllData.Columns.Clear();
            if (Grid_AllData.Items.Count > 0)
            {
                Grid_AllData.ItemsSource = null;
                Grid_AllData.Items.Refresh();
            }

            if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
            {
                String path_xml = Globals.PRINCIPAL_PATH;
                String lot_name = Globals.lot_name;

                // Créer un document XML.
                XmlDocument xmlDocument = new XmlDocument();

                string file_xml = path_xml + lot_name + ".XML";

                // Lire le fichier XML.
                xmlDocument.Load(file_xml);

                // Créer une liste de nœuds XML avec l'expression XPath.
                string path_balise;

                if (lot_name.All(char.IsDigit) == false)
                    path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                else
                    path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                List<AllData> All_Data = new List<AllData>();

                // Parcourir chaque étiquette du fichier XML.
                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string Data = xmlNode["PrintData"].InnerText;
                    string[] subs = Data.Split('\n');

                    if (ComboBox_Sector.SelectedItem == PVC)
                    {
                        var list_line_value = new List<string>();

                        // Supprimer les lignes vides.
                        RemoveLine(subs, list_line_value);

                        var list_data = new List<string>();

                        // Récuperer les données voulues.
                        SearchData(list_line_value, list_data);

                        // Récuperer que les données des profils recherchés.
                        if (ListBox_Profil.SelectedValue != null)
                        {
                            string profil_name = ListBox_Profil.SelectedValue.ToString();
                            if (lot_name.All(char.IsDigit) == false)
                            {
                                if (list_data[7] == profil_name)
                                {
                                    All_Data.Add(new AllData() { Choisi = false, Casier = list_data[10], Position = list_data[8], Ktn = list_data[6], Chariot = list_data[16], Repere = list_data[5], DM = list_data[0], CB = list_data[1], Lg = list_data[2], Lot = list_data[3], Couleur = list_data[4], Profil = list_data[7], Info4 = list_data[9], Renfort = list_data[11], Nb_CB = list_data[12], Info1 = list_data[13], Info2 = list_data[14], Info3 = list_data[15] });
                                }
                            }
                            else
                            {
                                if (list_data[2] == profil_name)
                                {
                                    list_data[6] = list_data[6].Substring(4);

                                    All_Data.Add(new AllData() { Choisi = false, Casier = list_data[8], Position = list_data[4], Ktn = list_data[7], Chariot = list_data[11], Repere = list_data[3], DM = "", CB = list_data[9], Lg = list_data[1], Lot = list_data[6], Couleur = "", Profil = list_data[2], Info4 = "", Renfort = list_data[5], Nb_CB = list_data[9], Info1 = list_data[10], Info2 = "", Info3 = "" });
                                }
                            }
                        }
                    }
                    else if (ComboBox_Sector.SelectedItem == ALU)
                    {
                        string AllZebra = xmlNode["PrintData"].InnerText;

                        var list_data = new List<string>();

                        // Récuperer les données voulues.
                        SearchData_ALU(subs, list_data);

                        // Récuperer que les données des profils recherchés.
                        if (ListBox_Profil.SelectedValue != null)
                        {
                            string profil_name = ListBox_Profil.SelectedValue.ToString();
                            if (list_data[3] == profil_name)
                            {
                                All_Data.Add(new AllData() { Choisi = false, Casier = list_data[1], Position = list_data[2], Ktn = list_data[5], Chariot = list_data[6], Repere = list_data[0], DM = "", CB = AllZebra, Lg = "", Lot = "", Couleur = "", Profil = "", Info4 = "", Renfort = "", Nb_CB = "", Info1 = "", Info2 = "", Info3 = "" });
                            }
                        }
                    }
                }
                // Mettre les données dans le DataGrid.      
                Grid_AllData.ItemsSource = All_Data;

                // Afficher que les colonnes souhaitées.
                for (int i = 6; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].Visibility = Visibility.Hidden;
                }

                // DataGrid en ReadOnly hormis la checkbox.
                Grid_AllData.Columns[0].IsReadOnly = false;
                for (int i = 1; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].IsReadOnly = true;
                }
            }
            else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible || APPLICATION_SILOG.Visibility == Visibility.Visible)
            {
                String path_ags = "";
                if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                {
                    path_ags = Globals.COMPLEMENTAIRE_PATH;
                }
                else if (APPLICATION_SILOG.Visibility == Visibility.Visible)
                {
                    path_ags = Globals.SILOG_PATH;
                }
                String lot_name = Globals.lot_name;

                string file_ags = path_ags + lot_name + ".AGS";

                // Lire le fichier AGS.
                IEnumerable<string> lines = File.ReadLines(file_ags);

                List<AllData> All_Data = new List<AllData>();

                // Récuperer les données voulues.
                foreach (string line in lines)
                {
                    var list_data = new List<string>();

                    SearchData_AGS(line, list_data);

                    // Récuperer que les données des profils recherchés.
                    if (ListBox_Profil.SelectedValue != null)
                    {
                        string profil_name = ListBox_Profil.SelectedValue.ToString();
                        if (list_data[10] == profil_name)
                        {
                            All_Data.Add(new AllData() { Choisi = false, Casier = list_data[0], Position = list_data[1], Ktn = list_data[2], Chariot = list_data[3], Repere = list_data[4], DM = list_data[5], CB = list_data[6], Lg = list_data[7], Lot = list_data[8], Couleur = list_data[9], Profil = list_data[10], Info4 = list_data[11], Nb_CB = list_data[12], Renfort = list_data[13], Info1 = list_data[14], Info2 = list_data[15], Info3 = list_data[16] });
                        }
                    }
                }
                // Mettre les données dans le DataGrid.      
                Grid_AllData.ItemsSource = All_Data;

                // Afficher que les colonnes souhaitées.
                for (int i = 6; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].Visibility = Visibility.Hidden;
                }

                // DataGrid en ReadOnly hormis la checkbox.
                Grid_AllData.Columns[0].IsReadOnly = false;
                for (int i = 1; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].IsReadOnly = true;
                }
            }
        }

        private void Select_All(object sender, RoutedEventArgs e)
        // Cocher toutes les étiquettes.
        {
            int endI = Grid_AllData.Items.Count;
            for (int i = 0; i < endI; i++)
            {
                AllData data = Grid_AllData.Items[i] as AllData;
                data.Choisi = true;
            }
            Grid_AllData.Items.Refresh();
        }

        private void PreviewKeyDownSelectAll(object sender, System.Windows.Input.KeyEventArgs e)
        // Raccourci -> Cocher toutes les étiquettes.
        {
            if (e.Key == Key.D1 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                int endI = Grid_AllData.Items.Count;
                for (int i = 0; i < endI; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    data.Choisi = true;
                }
                Grid_AllData.Items.Refresh();
            }
        }

        private void UnSelect_All(object sender, RoutedEventArgs e)
        // Décocher toutes les étiquettes.
        {
            int endI = Grid_AllData.Items.Count;
            for (int i = 0; i < endI; i++)
            {
                AllData data = Grid_AllData.Items[i] as AllData;
                data.Choisi = false;
            }
            Grid_AllData.Items.Refresh();
        }

        private void KeyDownUnSelectAll(object sender, System.Windows.Input.KeyEventArgs e)
        // Raccourci -> Décocher toutes les étiquettes.
        {
            if (e.Key == Key.D2 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                int endI = Grid_AllData.Items.Count;
                for (int i = 0; i < endI; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    data.Choisi = false;
                }
                Grid_AllData.Items.Refresh();
            }
        }

        public void MultipleSelection()
        // Cocher les étiquettes sélectionnées.
        {
            foreach (AllData item in Grid_AllData.SelectedItems)
            {
                item.Choisi = true;
                
            }
            Grid_AllData.Items.Refresh();
        }

        private void Multiple_Selection(object sender, RoutedEventArgs e)
        // Cocher les étiquettes sélectionnées.
        {
            MultipleSelection();
        }

        private void PreviewKeyUp_Multiple_Selection(object sender, System.Windows.Input.KeyEventArgs e)
        // Raccourci -> Cocher les étiquettes sélectionnées.
        {
            if (e.Key == Key.D3 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                MultipleSelection();
            }
        }

        private void OneClick_Checkbox(object sender, MouseButtonEventArgs e)
        // Simple clique sur la ligne pour sélectionner.
        {
            int nb_selected = Grid_AllData.SelectedItems.Count;
            if (nb_selected <= 1)
            {
                int index = Grid_AllData.SelectedIndex;
                AllData data = Grid_AllData.Items[index] as AllData;
                if (data.Choisi == false)
                    data.Choisi = true;
                else
                    data.Choisi = false;
                Grid_AllData.Items.Refresh();
            }
        }

        private void Notice(object sender, RoutedEventArgs e)
        // Renvoyer sur la notice.
        { 
            String openPDFFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\notice_application_etiquette.pdf";
            System.IO.File.WriteAllBytes(openPDFFile, global::TELIO_ETIQUETTE.Properties.Resources.notice_application_etiquette);
            System.Diagnostics.Process.Start(openPDFFile);
        }

        private void Refresh_button(object sender, RoutedEventArgs e)
        // Refresh l'application.
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void ShowSetting(object sender, RoutedEventArgs e)
        // Afficher les paramètres.
        {
            if (Setting.Visibility == Visibility.Hidden)
                Setting.Visibility = Visibility.Visible;
            else if (Setting.Visibility == Visibility.Visible)
                Setting.Visibility = Visibility.Hidden;
        }

        private void Search_Path_principal(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers des débits principaux.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_principal_path = folderDlg.SelectedPath + @"\";
            if (new_principal_path != @"\")
                Text_Path_principal.Text = new_principal_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers des débits principaux.
            if (new_principal_path != @"\")
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    Properties.Settings.Default.path_principale_pvc = new_principal_path;
                    Properties.Settings.Default.Save();
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    Properties.Settings.Default.path_principal_alu = new_principal_path;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void Search_Path_complementaire(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers des débits complémentaires.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_complementaire_path = folderDlg.SelectedPath + @"\";
            if (new_complementaire_path != @"\")
                Text_Path_complementaire.Text = new_complementaire_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers des débits complémentaires.
            if (new_complementaire_path != @"\")
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    Properties.Settings.Default.path_complementaire_pvc = new_complementaire_path;
                    Properties.Settings.Default.Save();
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    Properties.Settings.Default.path_complementaire_alu = new_complementaire_path;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void Search_Path_silog(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers des fichiers des débits silog.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_silog_path = folderDlg.SelectedPath + @"\";
            if (new_silog_path != @"\")
                Text_Path_silog.Text = new_silog_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers des débits silog.
            if (new_silog_path != @"\")
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    Properties.Settings.Default.path_silog_pvc = new_silog_path;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void New_Printer(object sender, SelectionChangedEventArgs e)
        // Imprimante sélectionnée.
        {
            Properties.Settings.Default.printer = ComboBox_Printer.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void Sector(object sender, SelectionChangedEventArgs e)
        // Secteur sélectionné.
        {
            Properties.Settings.Default.sector = ComboBox_Sector.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        // Barre de recherche pour les lots.
        {
            Grid_AllData.Columns.Clear();
            ListBox_Profil.Items.Clear();
            Grid_AllData.Visibility = Visibility.Collapsed;
            Rect_grid.Visibility = Visibility.Collapsed;

            string word = SearchBox.Text.ToUpper();
            var list_lot_search = new List<string>();

            // Récupérer la liste des lots actuels.
            foreach (object liItem in ListBox_Lot.Items)
            {
                string result = liItem.ToString();
                list_lot_search.Add(result);
            }

            ListBox_Lot.Items.Clear();

            // Afficher les résultats correspondant au lot cherché.
            foreach (string lot in list_lot_search)
            {
                if (lot.Contains(word))
                {
                    ListBox_Lot.Items.Add(lot);
                }
            }
        }

        //-----------------------[IMPRIMER]-----------------------\\

        public void ConPrinter(string zpl)
        // Procédure pour envoyer un code ZPL à l'imprimante ZEBRA.
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            pd.PrinterSettings = new PrinterSettings();
            pd.PrinterSettings.PrinterName = ComboBox_Printer.Text;
            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, zpl);
        }

        public void ZPL(int i_length, string[,] array)
        // Construire le zpl à envoyer à l'impression.
        {
            for (int i = 0; i < i_length; i++)
            {
                string zpl = "^XA^LH18,20^FO20,080^BY2^B3N,N,40,Y,N^FD" + array[i, 1] + "^FS^CFD,30,14^FO010,008^FD" + array[i, 3] + "^FS^FO010,045^FD" + array[i, 5] +
                    "^FS^CFD,28,14^FO145,008^FD" + array[i, 4] + "^FS^FO250,008^FD" + array[i, 2] + "^FS^FO400,008^FD" + array[i, 12] + "^FS^FO600,008^FD" + array[i, 16] +
                    "^FS^FO250,045^FD" + array[i, 13] + "^FS^FO350,045^FD" + array[i, 14] + "^FS^FO450,045^FD" + array[i, 15] + "^FS^FO560,045^FD" + array[i, 7] +
                    "^FS^FO655,080^FD" + array[i, 6] + "^FS^CFD,30,18^FO560,080^FD" + array[i, 10] + "^FS^CFD,30,22^FO680,045^FD" + array[i, 8] + "^FS^XZ";

                ConPrinter(zpl);
            }
        }

        public void ZPL_ALU(int i_length, string[,] array)
        // Construire le zpl à envoyer à l'impression.
        {
            for (int i = 0; i < i_length; i++)
            {
                string zpl = array[i, 0];

                ConPrinter(zpl);
            }
        }

        private void Print_Lot(object sender, RoutedEventArgs e)
        // Imprimer chaque étiquette du lot sélectionné.
        {
            if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    if (ListBox_Lot.SelectedValue != null)
                    {
                        string lot_name = ListBox_Lot.SelectedValue.ToString();

                        String path_xml = Globals.PRINCIPAL_PATH;

                        string file = path_xml + lot_name + ".XML";

                        // Créer un document XML.
                        XmlDocument xmlDocument = new XmlDocument();

                        // Lire le fichier XML.
                        xmlDocument.Load(file);

                        // Créer une liste de nœuds XML avec l'expression XPath.
                        string path_balise;

                        if (lot_name.All(char.IsDigit) == false)
                            path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                        else
                            path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                        XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                        // Parcourir chaque étiquette du fichier XML.
                        foreach (XmlNode xmlNode in xmlNodeList)
                        {
                            string Data = xmlNode["PrintData"].InnerText;
                            string[] subs = Data.Split('\n');

                            var list_line_value = new List<string>();

                            // Supprimer les lignes vides.
                            RemoveLine(subs, list_line_value);

                            var list_data = new List<string>();

                            // Récuperer les données voulues.
                            SearchData(list_line_value, list_data);

                            if (lot_name.All(char.IsDigit) == false)
                            {
                                // Construire le zpl à envoyer à l'impression.
                                string zpl = "^XA^LH18,20^FO20,080^BY2^B3N,N,40,Y,N^FD" + list_data[1] + "^FS^CFD,30,14^FO010,008^FD" + list_data[3] + "^FS^FO010,045^FD" + list_data[5] +
                                    "^FS^CFD,28,14^FO145,008^FD" + list_data[4] + "^FS^FO250,008^FD" + list_data[2] + "^FS^FO400,008^FD" + list_data[11] + "^FS^FO600,008^FD" + list_data[16] +
                                    "^FS^FO250,045^FD" + list_data[13] + "^FS^FO350,045^FD" + list_data[14] + "^FS^FO450,045^FD" + list_data[15] + "^FS^FO560,045^FD" + list_data[7] +
                                    "^FS^FO655,080^FD" + list_data[6] + "^FS^CFD,30,18^FO560,080^FD" + list_data[10] + "^FS^CFD,30,22^FO680,045^FD" + list_data[8] + "^FS^XZ";

                                ConPrinter(zpl);
                            }
                            else
                            {
                                list_data[6] = list_data[6].Substring(4);

                                // Construire le zpl à envoyer à l'impression.
                                string zpl = "^XA^LH18,20^FO50,080^BY2^B3N,N,40,Y,N^FD" + list_data[9] + "^FS^CFD,30,14^FO010,008^FD" + list_data[6] + "^FS^FO010,045^FD" + list_data[3] +
                                     "^FS^CFD,28,14^FO145,008^FD" + "" + "^FS^FO250,008^FD" + list_data[1] + "^FS^FO400,008^FD" + list_data[5] + "^FS^FO600,008^FD" + list_data[11] +
                                     "^FS^FO250,045^FD" + list_data[10] + "^FS^FO350,045^FD" + "" + "^FS^FO450,045^FD" + "" + "^FS^FO560,045^FD" + list_data[2] +
                                     "^FS^FO665,080^FD" + list_data[7] + "^FS^CFD,30,18^FO560,080^FD" + list_data[8] + "^FS^CFD,30,22^FO680,045^FD" + list_data[4] + "^FS^XZ";

                                ConPrinter(zpl);
                            }
                        }
                    }
                    else { MessageBox.Show("Aucun lot sélectionné"); }
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
                    {
                        if (ListBox_Lot.SelectedValue != null)
                        {
                            string lot_name = ListBox_Lot.SelectedValue.ToString();

                            String path_xml = Globals.PRINCIPAL_PATH;
                            Globals.lot_name = lot_name;

                            // Créer un document XML.
                            XmlDocument xmlDocument = new XmlDocument();

                            string file_xml = path_xml + lot_name + ".XML";

                            // Lire le fichier XML.
                            xmlDocument.Load(file_xml);

                            // Créer une liste de nœuds XML avec l'expression XPath.
                            string path_balise;

                            if (lot_name.All(char.IsDigit) == false)
                                path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                            else
                                path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                            XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                            var profil_name_duplicate = new List<string>();

                            // Parcourir chaque étiquette du fichier XML.
                            foreach (XmlNode xmlNode in xmlNodeList)
                            {
                                string Data = xmlNode["PrintData"].InnerText;

                                // Construire le zpl à envoyer à l'impression.
                                ConPrinter(Data);
                            }
                        }
                        else { MessageBox.Show("Aucun lot sélectionné"); }
                    }
                }
            }
            else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible || APPLICATION_SILOG.Visibility == Visibility.Visible)
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();

                    String path_ags = "";
                    if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.COMPLEMENTAIRE_PATH;
                    }
                    else if (APPLICATION_SILOG.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.SILOG_PATH;
                    }
                    Globals.lot_name = lot_name;

                    string file_ags = path_ags + lot_name + ".AGS";

                    // Lire le fichier AGS.
                    IEnumerable<string> lines = File.ReadLines(file_ags);

                    // Récupérer les références profilées.
                    foreach (string line in lines)
                    {
                        var list_data = new List<string>();

                        SearchData_AGS(line, list_data);

                        // Construire le zpl à envoyer à l'impression.
                        string zpl = "^XA^LH18,20^FO20,080^BY2^B3N,N,40,Y,N^FD" + list_data[6] + "^FS^CFD,30,14^FO010,008^FD" + list_data[8] + "^FS^FO010,045^FD" + list_data[4] +
                                   "^FS^CFD,28,14^FO145,008^FD" + list_data[9] + "^FS^FO250,008^FD" + list_data[7] + "^FS^FO400,008^FD" + list_data[13] + "^FS^FO600,008^FD" + list_data[3] +
                                   "^FS^FO250,045^FD" + list_data[14] + "^FS^FO350,045^FD" + "" + "^FS^FO450,045^FD" + "" + "^FS^FO560,045^FD" + list_data[10] +
                                   "^FS^FO655,080^FD" + list_data[2] + "^FS^CFD,30,18^FO560,080^FD" + list_data[0] + "^FS^CFD,30,22^FO680,045^FD" + list_data[1] + "^FS^XZ";

                        ConPrinter(zpl);
                    }
                }
                else { MessageBox.Show("Aucun lot sélectionné"); }
            }
        }

        private void Print_Profil(object sender, RoutedEventArgs e)
        // Imprimer chaque étiquette du profil sélectionné.
        {
            if (ListBox_Profil.SelectedValue != null)
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    string[,] array_data = new string[Grid_AllData.Items.Count, 17];

                    for (int i = 0; i < Grid_AllData.Items.Count; i++)
                    {
                        // Récuperer les données du DataGrid.
                        AllData data = Grid_AllData.Items[i] as AllData;
                        if (data != null)
                        {
                            string dataDM = data.DM;
                            string dataCB = data.CB;
                            string dataLg = data.Lg;
                            string dataLot = data.Lot;
                            string dataColor = data.Couleur;
                            string dataRepere = data.Repere;
                            string dataKtn = data.Ktn;
                            string dataProfil = data.Profil;
                            string dataPosition = data.Position;
                            string dataInfo4 = data.Info4;
                            string dataCasier = data.Casier;
                            string dataNb_CB = data.Nb_CB;
                            string dataRenfort = data.Renfort;
                            string dataInfo1 = data.Info1;
                            string dataInfo2 = data.Info2;
                            string dataInfo3 = data.Info3;
                            string dataChariot = data.Chariot;

                            // Insérer les données dans un tableau.
                            array_data[i, 0] = dataDM;
                            array_data[i, 1] = dataCB;
                            array_data[i, 2] = dataLg;
                            array_data[i, 3] = dataLot;
                            array_data[i, 4] = dataColor;
                            array_data[i, 5] = dataRepere;
                            array_data[i, 6] = dataKtn;
                            array_data[i, 7] = dataProfil;
                            array_data[i, 8] = dataPosition;
                            array_data[i, 9] = dataInfo4;
                            array_data[i, 10] = dataCasier;
                            array_data[i, 11] = dataNb_CB;
                            array_data[i, 12] = dataRenfort;
                            array_data[i, 13] = dataInfo1;
                            array_data[i, 14] = dataInfo2;
                            array_data[i, 15] = dataInfo3;
                            array_data[i, 16] = dataChariot;
                        }
                    }

                    ZPL(Grid_AllData.Items.Count, array_data);
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
                    {
                        string[,] array_data = new string[Grid_AllData.Items.Count, 1];

                        for (int i = 0; i < Grid_AllData.Items.Count; i++)
                        {
                            // Récuperer les données du DataGrid.
                            AllData data = Grid_AllData.Items[i] as AllData;
                            if (data != null)
                            {
                                string dataZPL = data.CB;

                                // Insérer les données dans un tableau.
                                array_data[i, 0] = dataZPL;
                            }
                        }

                        ZPL_ALU(Grid_AllData.Items.Count, array_data);
                    }
                    else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                    {
                        string[,] array_data = new string[Grid_AllData.Items.Count, 17];

                        for (int i = 0; i < Grid_AllData.Items.Count; i++)
                        {
                            // Récuperer les données du DataGrid.
                            AllData data = Grid_AllData.Items[i] as AllData;
                            if (data != null)
                            {
                                string dataDM = data.DM;
                                string dataCB = data.CB;
                                string dataLg = data.Lg;
                                string dataLot = data.Lot;
                                string dataColor = data.Couleur;
                                string dataRepere = data.Repere;
                                string dataKtn = data.Ktn;
                                string dataProfil = data.Profil;
                                string dataPosition = data.Position;
                                string dataInfo4 = data.Info4;
                                string dataCasier = data.Casier;
                                string dataNb_CB = data.Nb_CB;
                                string dataRenfort = data.Renfort;
                                string dataInfo1 = data.Info1;
                                string dataInfo2 = data.Info2;
                                string dataInfo3 = data.Info3;
                                string dataChariot = data.Chariot;

                                // Insérer les données dans un tableau.
                                array_data[i, 0] = dataDM;
                                array_data[i, 1] = dataCB;
                                array_data[i, 2] = dataLg;
                                array_data[i, 3] = dataLot;
                                array_data[i, 4] = dataColor;
                                array_data[i, 5] = dataRepere;
                                array_data[i, 6] = dataKtn;
                                array_data[i, 7] = dataProfil;
                                array_data[i, 8] = dataPosition;
                                array_data[i, 9] = dataInfo4;
                                array_data[i, 10] = dataCasier;
                                array_data[i, 11] = dataNb_CB;
                                array_data[i, 12] = dataRenfort;
                                array_data[i, 13] = dataInfo1;
                                array_data[i, 14] = dataInfo2;
                                array_data[i, 15] = dataInfo3;
                                array_data[i, 16] = dataChariot;
                            }
                        }

                        ZPL(Grid_AllData.Items.Count, array_data);
                    }
                }
            }
            else { MessageBox.Show("Aucun profil sélectionné"); }
        }

        private void Print_Etiquette(object sender, RoutedEventArgs e)
        // Imprimer les étiquettes sélectionnées.
        {
            if (ComboBox_Sector.SelectedItem == PVC)
            {
                // Définir la taille du tableau de données.
                int length_array = 0;
                for (int i = 0; i < Grid_AllData.Items.Count; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    if (data != null)
                    {
                        if (data.Choisi == true)
                        {
                            ++length_array;
                        }
                    }
                }

                int index = 0;
                string[,] array_data = new string[length_array, 17];

                for (int i = 0; i < Grid_AllData.Items.Count; i++)
                {
                    // Récuperer les données du DataGrid.
                    AllData data = Grid_AllData.Items[i] as AllData;
                    if (data != null)
                    {
                        string dataDM = data.DM;
                        string dataCB = data.CB;
                        string dataLg = data.Lg;
                        string dataLot = data.Lot;
                        string dataColor = data.Couleur;
                        string dataRepere = data.Repere;
                        string dataKtn = data.Ktn;
                        string dataProfil = data.Profil;
                        string dataPosition = data.Position;
                        string dataInfo4 = data.Info4;
                        string dataCasier = data.Casier;
                        string dataNb_CB = data.Nb_CB;
                        string dataRenfort = data.Renfort;
                        string dataInfo1 = data.Info1;
                        string dataInfo2 = data.Info2;
                        string dataInfo3 = data.Info3;
                        string dataChariot = data.Chariot;

                        // Insérer les données dans un tableau si elle sont sélectionnées.
                        if (data.Choisi == true)
                        {
                            array_data[index, 0] = dataDM;
                            array_data[index, 1] = dataCB;
                            array_data[index, 2] = dataLg;
                            array_data[index, 3] = dataLot;
                            array_data[index, 4] = dataColor;
                            array_data[index, 5] = dataRepere;
                            array_data[index, 6] = dataKtn;
                            array_data[index, 7] = dataProfil;
                            array_data[index, 8] = dataPosition;
                            array_data[index, 9] = dataInfo4;
                            array_data[index, 10] = dataCasier;
                            array_data[index, 11] = dataNb_CB;
                            array_data[index, 12] = dataRenfort;
                            array_data[index, 13] = dataInfo1;
                            array_data[index, 14] = dataInfo2;
                            array_data[index, 15] = dataInfo3;
                            array_data[index, 16] = dataChariot;

                            index++;
                        }
                    }
                }

                ZPL(length_array, array_data);
            }
            else if (ComboBox_Sector.SelectedItem == ALU)
            {
                // Définir la taille du tableau de données.
                int length_array = 0;
                for (int i = 0; i < Grid_AllData.Items.Count; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    if (data != null)
                    {
                        if (data.Choisi == true)
                        {
                            ++length_array;
                        }
                    }
                }

                int index = 0;
                if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
                {
                    string[,] array_data = new string[length_array, 1];

                    for (int i = 0; i < Grid_AllData.Items.Count; i++)
                    {
                        // Récuperer les données du DataGrid.
                        AllData data = Grid_AllData.Items[i] as AllData;
                        if (data != null)
                        {
                            string dataZPL = data.CB;

                            // Insérer les données dans un tableau si elle sont sélectionnées.
                            if (data.Choisi == true)
                            {
                                array_data[index, 0] = dataZPL;

                                index++;
                            }
                        }
                    }

                    ZPL_ALU(length_array, array_data);
                }
                else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                {
                    string[,] array_data = new string[length_array, 17];

                    for (int i = 0; i < Grid_AllData.Items.Count; i++)
                    {
                        // Récuperer les données du DataGrid.
                        AllData data = Grid_AllData.Items[i] as AllData;
                        if (data != null)
                        {
                            string dataDM = data.DM;
                            string dataCB = data.CB;
                            string dataLg = data.Lg;
                            string dataLot = data.Lot;
                            string dataColor = data.Couleur;
                            string dataRepere = data.Repere;
                            string dataKtn = data.Ktn;
                            string dataProfil = data.Profil;
                            string dataPosition = data.Position;
                            string dataInfo4 = data.Info4;
                            string dataCasier = data.Casier;
                            string dataNb_CB = data.Nb_CB;
                            string dataRenfort = data.Renfort;
                            string dataInfo1 = data.Info1;
                            string dataInfo2 = data.Info2;
                            string dataInfo3 = data.Info3;
                            string dataChariot = data.Chariot;

                            // Insérer les données dans un tableau si elle sont sélectionnées.
                            if (data.Choisi == true)
                            {
                                array_data[index, 0] = dataDM;
                                array_data[index, 1] = dataCB;
                                array_data[index, 2] = dataLg;
                                array_data[index, 3] = dataLot;
                                array_data[index, 4] = dataColor;
                                array_data[index, 5] = dataRepere;
                                array_data[index, 6] = dataKtn;
                                array_data[index, 7] = dataProfil;
                                array_data[index, 8] = dataPosition;
                                array_data[index, 9] = dataInfo4;
                                array_data[index, 10] = dataCasier;
                                array_data[index, 11] = dataNb_CB;
                                array_data[index, 12] = dataRenfort;
                                array_data[index, 13] = dataInfo1;
                                array_data[index, 14] = dataInfo2;
                                array_data[index, 15] = dataInfo3;
                                array_data[index, 16] = dataChariot;

                                index++;
                            }
                        }
                    }

                    ZPL(length_array, array_data);
                }
            }
        }
    }

    public class RawPrinterHelper
    {
        // Déclarations de structure et d'API :
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)] public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)] public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)] public string pDataType;
        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        // Lorsque la fonction reçoit un nom d'imprimante et un tableau non géré
        // d'octets, la fonction envoie ces octets à la file d'attente d'impression.
        // Renvoie vrai en cas de succès, faux en cas d'échec.
        {
            Int32 dwError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false; // Supposez l'échec à moins que vous ne réussissiez spécifiquement.

            di.pDocName = "My C#.NET RAW Document";
            di.pDataType = "RAW";

            // Ouvrez l'imprimante.
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Lancez un document.
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Lancez la page.
                    if (StartPagePrinter(hPrinter))
                    {
                        // Écrivez vos octets.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }
            // Si vous n'avez pas réussi, GetLastError peut donner plus d'informations.
            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }

        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;
            Int32 dwCount;
            // Combien de caractères y a-t-il dans la chaîne ?
            dwCount = szString.Length;
            // Supposons que l'imprimante attend du texte ANSI, puis convertissez
            // la chaîne en texte ANSI.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Envoie la chaîne ANSI convertie à l'imprimante.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }
    }
}