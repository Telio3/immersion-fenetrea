let lang = getUrlParam("lang") || "fr"

init()

function init(){
    let json = FR_LANG

    if(lang === "en")
        json = EN_LANG

    setStringText("title_page", json)
    setStringText("category_1", json)
    setStringText("category_4", json)
    setStringText("landing_title", json)
    setStringText("now_language", json)
    setStringText("other_language", json)
    setStringText("landing_desc", json)
    setStringText("fenetrea_desc", json)
    setStringText("start_internship", json)
    setStringText("conduct_internship", json)
    setStringText("projects_carried_out", json)
    setStringText("text_label", json)
    setStringText("text_carriage", json)
    setStringText("text_rotox", json)
    setStringText("text_webservice", json)
    setImage("activity_fenetrea", json)
    setTitle("file_exemple", json)
    setTitle("notice", json)
    setTitle("extract_code", json)
    setTitle("title_webservice", json)
}

function setStringText(key, json){
    document.getElementById(key).innerHTML = json[key]
}

function setImage(key, json){
    document.getElementById(key).setAttribute("src", json[key])
}

function setTitle(key, json){
    document.getElementById(key).setAttribute("title", json[key])
}