function smoothScroll(classe){
    document.querySelector(classe).scrollIntoView({
        behavior: 'smooth'
    });
}

let actual_height = 1;

function ColorNavBar()
{
    for (let i = 1; i <= 4; i++) {
        let before_navbar = document.getElementById("category_"+i);
        before_navbar.style.color = "#ffffff77";
    }

    let now_navbar = document.getElementById("category_"+actual_height);
    now_navbar.style.color = "#ffffff";
}

$(window).on('scroll', function() {
    let $height = $(window).scrollTop();
    if($height > 0 && $height < 890) {
        actual_height = 1;
        ColorNavBar()
    }
    else if($height > 891 && $height < 1980) {
        actual_height = 2;
        ColorNavBar()
    }
    else if($height > 1981 && $height < 3550) {
        actual_height = 3;
        ColorNavBar()
    }
    else if($height > 3551) {
      actual_height = 4;
      ColorNavBar()
    }
});

let display_language = 1;
function DisplayLanguage() {
    if (display_language == 1) {
        document.getElementById("other_language").style.display = "block";
        document.getElementById("now_language").style.marginTop = "50px";
        document.getElementById("now_language").style.marginBottom = "20px";
        display_language = 2;
    }
    else if (display_language == 2) {
        document.getElementById("other_language").style.display = "none";
        document.getElementById("now_language").style.marginTop = "0px";
        document.getElementById("now_language").style.marginBottom = "0px";
        display_language = 1;
    }
}

function ChangeLanguage() {
    if(lang === "en") {
        window.location = "?lang=fr";
    }
  
    if(lang === "fr") {
        window.location = "?lang=en";
    }
}
  

let noscroll_var;
function noscroll(){
  if(noscroll_var){
    document.getElementsByTagName("html")[0].style.overflowY = "";
    document.body.style.paddingRight = "0";
    noscroll_var = false
  }else{
    document.getElementsByTagName("html")[0].setAttribute('style', 'overflow-y: hidden !important');
    noscroll_var = true
  }
}

let now_code = "";
function Display_Code(id_code) {
    let application_code = document.getElementById(id_code);
    document.getElementById("display_code").classList.add("visible");
    document.getElementById("display_code").classList.remove("collapse");

    application_code.classList.add("visible");
    application_code.classList.remove("collapse");

    noscroll();
    now_code = id_code;
}

function Close_Code() {
    document.getElementById("display_code").classList.add("collapse");
    document.getElementById("display_code").classList.remove("visible");
    document.getElementById(now_code).classList.add("collapse");
    document.getElementById(now_code).classList.remove("visible");
    noscroll();
}

let application_etiquette = document.getElementById("application_etiquette");
let str_etiquette = `using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Windows.Media;
using MessageBox = System.Windows.MessageBox;

namespace TELIO_ETIQUETTE
{ public partial class MainWindow : Window
    {
        //-----------------------[STYLE]-----------------------\\

        public void PlaceHolder(object sender, EventArgs e)
        // Supprimer le placeholder à SearchBox.
        {
            if (SearchBox.Text == "Rechercher")
            {
                SearchBox.Text = "";
            }
            SearchBox.Foreground = Brushes.Black;
        }

        public void AddText(object sender, EventArgs e)
        // Ajouter le placeholder à SearchBox.
        {
            if (string.IsNullOrWhiteSpace(SearchBox.Text))
            {
                SearchBox.Text = "Rechercher";
            }
            SearchBox.Foreground = Brushes.Gray;
        }

        //-----------------------[INTERACTION]-----------------------\\

        public class AllData
        // Constructeur DataGrid.
        {
            public bool Choisi { get; set; }

            public string Casier { get; set; }

            public string Position { get; set; }

            public string Ktn { get; set; }

            public string Chariot { get; set; }

            public string Repere { get; set; }

            public string DM { get; set; }

            public string CB { get; set; }

            public string Lg { get; set; }

            public string Lot { get; set; }

            public string Couleur { get; set; }

            public string Profil { get; set; }

            public string Info4 { get; set; }

            public string Nb_CB { get; set; }

            public string Renfort { get; set; }

            public string Info1 { get; set; }

            public string Info2 { get; set; }

            public string Info3 { get; set; }
        }

        public static class Globals
        // Variables globales.
        {
            public static String lot_name = "";
            public static String Sector = Properties.Settings.Default.sector;
            public static String PRINCIPAL_PATH_PVC = Properties.Settings.Default.path_principale_pvc;
            public static String COMPLEMENTAIRE_PATH_PVC = Properties.Settings.Default.path_complementaire_pvc;
            public static String SILOG_PATH_PVC = Properties.Settings.Default.path_silog_pvc;
            public static String PRINCIPAL_PATH_ALU = Properties.Settings.Default.path_principal_alu;
            public static String COMPLEMENTAIRE_PATH_ALU = Properties.Settings.Default.path_complementaire_alu;
            public static String PRINCIPAL_PATH = "";
            public static String COMPLEMENTAIRE_PATH = "";
            public static String SILOG_PATH = "";
        }

        public MainWindow()
        // Méthode au lancement de l'application.
        {
            InitializeComponent();

            // Définir les paramètres du secteur actuel.
            String sector = Globals.Sector.ToString();
            if (sector == PVC.ToString())
            {
                ComboBox_Sector.SelectedItem = PVC;
                Text_Path_principal.Text = Globals.PRINCIPAL_PATH_PVC;
                Text_Path_complementaire.Text = Globals.COMPLEMENTAIRE_PATH_PVC;
                Text_Path_silog.Text = Globals.SILOG_PATH_PVC;
                Globals.PRINCIPAL_PATH = Globals.PRINCIPAL_PATH_PVC;
                Globals.COMPLEMENTAIRE_PATH = Globals.COMPLEMENTAIRE_PATH_PVC;
                Globals.SILOG_PATH = Globals.SILOG_PATH_PVC;

                border_settings.Height = 450;
                path_silog.Visibility = Visibility.Visible;
                Thickness margin = bottom_settings.Margin;
                margin.Top = 80;
                bottom_settings.Margin = margin;

                Display_Principaux.Width = 102;
                Canvas.SetLeft(Display_Principaux, 26);

                Display_Complementaires.Width = 136;
                Canvas.SetLeft(Display_Complementaires, 128);

                Display_Silog.Visibility = Visibility.Visible;
            }
            else if (sector == ALU.ToString())
            {
                ComboBox_Sector.SelectedItem = ALU;
                Text_Path_principal.Text = Globals.PRINCIPAL_PATH_ALU;
                Text_Path_complementaire.Text = Globals.COMPLEMENTAIRE_PATH_ALU;
                Globals.PRINCIPAL_PATH = Globals.PRINCIPAL_PATH_ALU;
                Globals.COMPLEMENTAIRE_PATH = Globals.COMPLEMENTAIRE_PATH_ALU;

                border_settings.Height = 372;

                Display_Principaux.Width = 154;
                Canvas.SetLeft(Display_Principaux, 26);

                Display_Complementaires.Width = 155;
                Canvas.SetLeft(Display_Complementaires, 180);
            }

            // Récupérer les imprimantes connectées.
            ComboBox_Printer.Items.Add(Properties.Settings.Default.printer);
            ComboBox_Printer.SelectedItem = Properties.Settings.Default.printer;

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (printer.ToUpper().Contains("Z") && printer.ToUpper().Contains("B"))
                {
                    if (printer != Properties.Settings.Default.printer)
                    {
                        ComboBox_Printer.Items.Add(printer);
                    }
                }
            }
        }

        public void Button_Display(System.Windows.Controls.Button Display_Active, System.Windows.Controls.Button Display_Deactivate_1, System.Windows.Controls.Button Display_Deactivate_2, Canvas Application_Active, Canvas Application_Deactivate_1, Canvas Application_Deactivate_2)
        // Afficher la partie de traitement voulue.
        {
            Display_Active.Background = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            Display_Active.Foreground = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));

            Display_Deactivate_1.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
            Display_Deactivate_1.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            
            Application_Active.Visibility = Visibility.Visible;
            Application_Deactivate_1.Visibility = Visibility.Collapsed;

            String sector = Globals.Sector.ToString();
            if (sector == PVC.ToString())
            {
                Display_Deactivate_2.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Deactivate_2.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));

                Application_Deactivate_2.Visibility = Visibility.Collapsed;
            }

            ListBox_Lot.Items.Clear();
            ListBox_Profil.Items.Clear();
            Grid_AllData.Columns.Clear();

            ListBox_Lot.SelectedIndex = -1;
            ListBox_Lot.Visibility = Visibility.Visible;
            Grid_AllData.Visibility = Visibility.Collapsed;
            Rect_grid.Visibility = Visibility.Collapsed;

            SearchBox.Text = "Rechercher";
        }

        private void Button_Principal(object sender, RoutedEventArgs e)
        // Afficher la partie des débits principaux.
        {
            Button_Display(Display_Principaux, Display_Complementaires, Display_Silog, APPLICATION_PRINCIPAL, APPLICATION_COMPLEMENTAIRE, APPLICATION_SILOG);

            String path_principal = Globals.PRINCIPAL_PATH;

            try
            {
                string[] files_XML = Directory.GetFiles(path_principal, "*.XML");

                // Récupérer tous les fichiers XML présent dans le dossier.
                foreach (string file in files_XML)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    if (fileWExt.All(char.IsDigit) == false)
                        ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Principaux.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Principaux.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        private void Button_Complementaire(object sender, RoutedEventArgs e)
        // Afficher la partie des débits complémentaires.
        {
            Button_Display(Display_Complementaires, Display_Principaux, Display_Silog, APPLICATION_COMPLEMENTAIRE, APPLICATION_PRINCIPAL, APPLICATION_SILOG);

            String path_complementaire = Globals.COMPLEMENTAIRE_PATH;

            try
            {
                string[] files_AGS = Directory.GetFiles(path_complementaire, "*.AGS");

                // Récupérer tous les fichiers AGS présent dans le dossier.
                foreach (string file in files_AGS)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Complementaires.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Complementaires.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        private void Button_Silog(object sender, RoutedEventArgs e)
        // Afficher la partie des débits silog.
        {
            Button_Display(Display_Silog, Display_Principaux, Display_Complementaires, APPLICATION_SILOG, APPLICATION_PRINCIPAL, APPLICATION_COMPLEMENTAIRE);

            String path_silog = Globals.SILOG_PATH;

            try
            {
                string[] files_AGS = Directory.GetFiles(path_silog, "*.AGS");

                // Récupérer tous les fichiers AGS présent dans le dossier.
                foreach (string file in files_AGS)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    if (fileWExt.All(char.IsDigit) == true)
                        ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Silog.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Silog.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        public void SearchData(List<string> list_search, List<string> list_find)
        // Récuperer les données voulues.
        {
            foreach (string data in list_search)
            {
                if (data.Contains("$027DC"))
                {
                    int pFrom = data.IndexOf("$027DC") + "$027DC".Length;
                    int pTo = data.LastIndexOf("");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027B"))
                {
                    int pFrom = data.IndexOf("*") + "*".Length;
                    int pTo = data.LastIndexOf("*");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("/"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("$027q1");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XM"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XS"))
                {
                    int pFrom = data.IndexOf("$027XS") + "$027XS".Length;
                    int pTo = data.LastIndexOf("");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
            }
            for (int i = 0; i < list_find.Count; i++)
            {
                if (list_find[i].Contains("$"))
                    list_find[i] = "";
            }
        }

        public void SearchData_ALU(string[] list_search, List<string> list_find)
        // Récuperer les données voulues.
        {
            foreach (string data in list_search)
            {
                if (data.Contains("cas="))
                {
                    int pFrom = data.IndexOf("cas=") + "cas=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("pos="))
                {
                    int pFrom = data.IndexOf("pos=") + "pos=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("Ktn="))
                {
                    int pFrom = data.IndexOf("Ktn=") + "Ktn=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("."))
                {
                    int pFrom = data.IndexOf("FD") + "FD".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("rep="))
                {
                    int pFrom = data.IndexOf("rep=") + "rep=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("prf="))
                {
                    int pFrom = data.IndexOf("prf=") + "prf=".Length;
                    int pTo = data.LastIndexOf("^FS");

                    String result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
            }
            list_find[5] = "ktn=" + list_find[5];
        }

        public void SearchData_AGS(string line, List<string> list_data)
        // Récuperer les données voulues.
        {

            list_data.Add(line.Split(new string[] { "T,CAS2," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,COTE," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "L," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,WAG1," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,REP," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            if (line.Contains("T,CB1,"))
                list_data.Add(line.Split(new string[] { "T,CB1," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            else
                list_data.Add("");
            list_data.Add(line.Split(new string[] { "T,CB," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,LP1," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,LOT," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "TYP," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "TYP," }, StringSplitOptions.None)[1].Split(',')[0].Trim());
            list_data.Add("");
            list_data.Add(line.Split(new string[] { "T,CB," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,LRO," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add(line.Split(new string[] { "T,INF," }, StringSplitOptions.None)[1].Split(';')[0].Trim());
            list_data.Add("");
            list_data.Add("");

            // Séparer la couleur du profil.
            int pFrom = list_data[9].IndexOf(",") + ",".Length;
            int pTo = list_data[9].LastIndexOf("") + 1;
            string result = list_data[9].Substring(pFrom, pTo - pFrom);
            list_data[9] = result;

            if (list_data[13] != "")
            {
                list_data[13] = list_data[13].Substring(0, list_data[13].Length - 1) + "." + list_data[13].Substring(list_data[13].Length - 1, 1);
            }
                
            list_data[2] = "ktn=" + list_data[2];
            list_data[7] = "Lg=" + list_data[7];
            list_data[13] = "Rf" + list_data[13];
        }

        public void RemoveLine(string[] subs, List<string> list_line_value)
        // Supprimer les lignes vides.
        {
            foreach (var sub in subs)
            {
                if (sub.Length > 30)
                {
                    list_line_value.Add(sub);
                }
            }
        }

        private void Select_Lot(object sender, RoutedEventArgs e)
        // Action lors de la sélection du lot.
        {
            ListBox_Profil.Items.Clear();
            if (Grid_AllData.Items.Count > 0)
            {
                Grid_AllData.ItemsSource = null;
                Grid_AllData.Items.Refresh();
            }
            Grid_AllData.Columns.Clear();
            ListBox_Profil.Visibility = Visibility.Visible;
            Grid_AllData.Visibility = Visibility.Collapsed;
            Rect_grid.Visibility = Visibility.Collapsed;

            if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();

                    String path_xml = Globals.PRINCIPAL_PATH;
                    Globals.lot_name = lot_name;

                    // Créer un document XML.
                    XmlDocument xmlDocument = new XmlDocument();

                    string file_xml = path_xml + lot_name + ".XML";

                    // Lire le fichier XML.
                    xmlDocument.Load(file_xml);

                    // Créer une liste de nœuds XML avec l'expression XPath.
                    string path_balise;

                    if (lot_name.All(char.IsDigit) == false)
                        path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                    else
                        path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                    XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                    var profil_name_duplicate = new List<string>();

                    // Parcourir chaque étiquette du fichier XML.
                    foreach (XmlNode xmlNode in xmlNodeList)
                    {
                        string Data = xmlNode["PrintData"].InnerText;
                        string[] subs = Data.Split('\n');

                        if (ComboBox_Sector.SelectedItem == PVC)
                        {
                            var list_line_value = new List<string>();

                            // Supprimer les lignes vides.
                            RemoveLine(subs, list_line_value);

                            // Récupérer les références profilées.
                            string line_profil_name;

                            if (lot_name.All(char.IsDigit) == false)
                                line_profil_name = list_line_value[7];
                            else
                                line_profil_name = list_line_value[2];

                            int pFrom = line_profil_name.IndexOf("$027XS") + "$027XS".Length;
                            int pTo = line_profil_name.LastIndexOf("");
                            String result = line_profil_name.Substring(pFrom, pTo - pFrom);
                            profil_name_duplicate.Add(result);
                        }
                        else if (ComboBox_Sector.SelectedItem == ALU)
                        {
                            int pFrom = subs[10].IndexOf("prf=") + "prf=".Length;
                            int pTo = subs[10].LastIndexOf("^FS");
                            String result = subs[10].Substring(pFrom, pTo - pFrom);
                            profil_name_duplicate.Add(result);
                        }
                    }

                    // Supprimer les doublons.
                    List<string> profil_name_whitout_duplicate = profil_name_duplicate.Distinct().ToList();

                    // Ajouter les profils dans la ListBox.
                    foreach (string profil_name in profil_name_whitout_duplicate)
                    {
                        ListBox_Profil.Items.Add(profil_name);
                    }
                }
            }
            else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible || APPLICATION_SILOG.Visibility == Visibility.Visible)
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();

                    String path_ags = "";
                    if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.COMPLEMENTAIRE_PATH;
                    }
                    else if (APPLICATION_SILOG.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.SILOG_PATH;
                    }
                    Globals.lot_name = lot_name;

                    string file_ags = path_ags + lot_name + ".AGS";

                    // Lire le fichier AGS.
                    IEnumerable<string> lines = File.ReadLines(file_ags);

                    var list_profil_duplicate = new List<string>();

                    // Récupérer les références profilées.
                    foreach (string line in lines)
                    {
                        string result = line.Split(new string[] { "TYP," }, StringSplitOptions.None)[1].Split(',')[0].Trim();
                        list_profil_duplicate.Add(result);
                    }

                    // Supprimer les doublons.
                    List<string> profil_name_whitout_duplicate = list_profil_duplicate.Distinct().ToList();

                    // Ajouter les profils dans la ListBox.
                    foreach (string profil_name in profil_name_whitout_duplicate)
                    {
                        ListBox_Profil.Items.Add(profil_name);
                    }
                }
            }
        }

        private void Select_Profil(object sender, RoutedEventArgs e)
        // Action lors de la sélection du profil.
        {
            Grid_AllData.Visibility = Visibility.Visible;
            Rect_grid.Visibility = Visibility.Visible;
            Grid_AllData.Columns.Clear();
            if (Grid_AllData.Items.Count > 0)
            {
                Grid_AllData.ItemsSource = null;
                Grid_AllData.Items.Refresh();
            }

            if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
            {
                String path_xml = Globals.PRINCIPAL_PATH;
                String lot_name = Globals.lot_name;

                // Créer un document XML.
                XmlDocument xmlDocument = new XmlDocument();

                string file_xml = path_xml + lot_name + ".XML";

                // Lire le fichier XML.
                xmlDocument.Load(file_xml);

                // Créer une liste de nœuds XML avec l'expression XPath.
                string path_balise;

                if (lot_name.All(char.IsDigit) == false)
                    path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                else
                    path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                List<AllData> All_Data = new List<AllData>();

                // Parcourir chaque étiquette du fichier XML.
                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string Data = xmlNode["PrintData"].InnerText;
                    string[] subs = Data.Split('\n');

                    if (ComboBox_Sector.SelectedItem == PVC)
                    {
                        var list_line_value = new List<string>();

                        // Supprimer les lignes vides.
                        RemoveLine(subs, list_line_value);

                        var list_data = new List<string>();

                        // Récuperer les données voulues.
                        SearchData(list_line_value, list_data);

                        // Récuperer que les données des profils recherchés.
                        if (ListBox_Profil.SelectedValue != null)
                        {
                            string profil_name = ListBox_Profil.SelectedValue.ToString();
                            if (lot_name.All(char.IsDigit) == false)
                            {
                                if (list_data[7] == profil_name)
                                {
                                    All_Data.Add(new AllData() { Choisi = false, Casier = list_data[10], Position = list_data[8], Ktn = list_data[6], Chariot = list_data[16], Repere = list_data[5], DM = list_data[0], CB = list_data[1], Lg = list_data[2], Lot = list_data[3], Couleur = list_data[4], Profil = list_data[7], Info4 = list_data[9], Renfort = list_data[11], Nb_CB = list_data[12], Info1 = list_data[13], Info2 = list_data[14], Info3 = list_data[15] });
                                }
                            }
                            else
                            {
                                if (list_data[2] == profil_name)
                                {
                                    list_data[6] = list_data[6].Substring(4);

                                    All_Data.Add(new AllData() { Choisi = false, Casier = list_data[8], Position = list_data[4], Ktn = list_data[7], Chariot = list_data[11], Repere = list_data[3], DM = "", CB = list_data[9], Lg = list_data[1], Lot = list_data[6], Couleur = "", Profil = list_data[2], Info4 = "", Renfort = list_data[5], Nb_CB = list_data[9], Info1 = list_data[10], Info2 = "", Info3 = "" });
                                }
                            }
                        }
                    }
                    else if (ComboBox_Sector.SelectedItem == ALU)
                    {
                        string AllZebra = xmlNode["PrintData"].InnerText;

                        var list_data = new List<string>();

                        // Récuperer les données voulues.
                        SearchData_ALU(subs, list_data);

                        // Récuperer que les données des profils recherchés.
                        if (ListBox_Profil.SelectedValue != null)
                        {
                            string profil_name = ListBox_Profil.SelectedValue.ToString();
                            if (list_data[3] == profil_name)
                            {
                                All_Data.Add(new AllData() { Choisi = false, Casier = list_data[1], Position = list_data[2], Ktn = list_data[5], Chariot = list_data[6], Repere = list_data[0], DM = "", CB = AllZebra, Lg = "", Lot = "", Couleur = "", Profil = "", Info4 = "", Renfort = "", Nb_CB = "", Info1 = "", Info2 = "", Info3 = "" });
                            }
                        }
                    }
                }
                // Mettre les données dans le DataGrid.      
                Grid_AllData.ItemsSource = All_Data;

                // Afficher que les colonnes souhaitées.
                for (int i = 6; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].Visibility = Visibility.Hidden;
                }

                // DataGrid en ReadOnly hormis la checkbox.
                Grid_AllData.Columns[0].IsReadOnly = false;
                for (int i = 1; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].IsReadOnly = true;
                }
            }
            else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible || APPLICATION_SILOG.Visibility == Visibility.Visible)
            {
                String path_ags = "";
                if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                {
                    path_ags = Globals.COMPLEMENTAIRE_PATH;
                }
                else if (APPLICATION_SILOG.Visibility == Visibility.Visible)
                {
                    path_ags = Globals.SILOG_PATH;
                }
                String lot_name = Globals.lot_name;

                string file_ags = path_ags + lot_name + ".AGS";

                // Lire le fichier AGS.
                IEnumerable<string> lines = File.ReadLines(file_ags);

                List<AllData> All_Data = new List<AllData>();

                // Récuperer les données voulues.
                foreach (string line in lines)
                {
                    var list_data = new List<string>();

                    SearchData_AGS(line, list_data);

                    // Récuperer que les données des profils recherchés.
                    if (ListBox_Profil.SelectedValue != null)
                    {
                        string profil_name = ListBox_Profil.SelectedValue.ToString();
                        if (list_data[10] == profil_name)
                        {
                            All_Data.Add(new AllData() { Choisi = false, Casier = list_data[0], Position = list_data[1], Ktn = list_data[2], Chariot = list_data[3], Repere = list_data[4], DM = list_data[5], CB = list_data[6], Lg = list_data[7], Lot = list_data[8], Couleur = list_data[9], Profil = list_data[10], Info4 = list_data[11], Nb_CB = list_data[12], Renfort = list_data[13], Info1 = list_data[14], Info2 = list_data[15], Info3 = list_data[16] });
                        }
                    }
                }
                // Mettre les données dans le DataGrid.      
                Grid_AllData.ItemsSource = All_Data;

                // Afficher que les colonnes souhaitées.
                for (int i = 6; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].Visibility = Visibility.Hidden;
                }

                // DataGrid en ReadOnly hormis la checkbox.
                Grid_AllData.Columns[0].IsReadOnly = false;
                for (int i = 1; i <= 17; i++)
                {
                    Grid_AllData.Columns[i].IsReadOnly = true;
                }
            }
        }

        private void Select_All(object sender, RoutedEventArgs e)
        // Cocher toutes les étiquettes.
        {
            int endI = Grid_AllData.Items.Count;
            for (int i = 0; i < endI; i++)
            {
                AllData data = Grid_AllData.Items[i] as AllData;
                data.Choisi = true;
            }
            Grid_AllData.Items.Refresh();
        }

        private void PreviewKeyDownSelectAll(object sender, System.Windows.Input.KeyEventArgs e)
        // Raccourci -> Cocher toutes les étiquettes.
        {
            if (e.Key == Key.D1 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                int endI = Grid_AllData.Items.Count;
                for (int i = 0; i < endI; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    data.Choisi = true;
                }
                Grid_AllData.Items.Refresh();
            }
        }

        private void UnSelect_All(object sender, RoutedEventArgs e)
        // Décocher toutes les étiquettes.
        {
            int endI = Grid_AllData.Items.Count;
            for (int i = 0; i < endI; i++)
            {
                AllData data = Grid_AllData.Items[i] as AllData;
                data.Choisi = false;
            }
            Grid_AllData.Items.Refresh();
        }

        private void KeyDownUnSelectAll(object sender, System.Windows.Input.KeyEventArgs e)
        // Raccourci -> Décocher toutes les étiquettes.
        {
            if (e.Key == Key.D2 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                int endI = Grid_AllData.Items.Count;
                for (int i = 0; i < endI; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    data.Choisi = false;
                }
                Grid_AllData.Items.Refresh();
            }
        }

        public void MultipleSelection()
        // Cocher les étiquettes sélectionnées.
        {
            foreach (AllData item in Grid_AllData.SelectedItems)
            {
                item.Choisi = true;
                
            }
            Grid_AllData.Items.Refresh();
        }

        private void Multiple_Selection(object sender, RoutedEventArgs e)
        // Cocher les étiquettes sélectionnées.
        {
            MultipleSelection();
        }

        private void PreviewKeyUp_Multiple_Selection(object sender, System.Windows.Input.KeyEventArgs e)
        // Raccourci -> Cocher les étiquettes sélectionnées.
        {
            if (e.Key == Key.D3 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                MultipleSelection();
            }
        }

        private void OneClick_Checkbox(object sender, MouseButtonEventArgs e)
        // Simple clique sur la ligne pour sélectionner.
        {
            int nb_selected = Grid_AllData.SelectedItems.Count;
            if (nb_selected <= 1)
            {
                int index = Grid_AllData.SelectedIndex;
                AllData data = Grid_AllData.Items[index] as AllData;
                if (data.Choisi == false)
                    data.Choisi = true;
                else
                    data.Choisi = false;
                Grid_AllData.Items.Refresh();
            }
        }

        private void Notice(object sender, RoutedEventArgs e)
        // Renvoyer sur la notice.
        { 
            String openPDFFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\notice_application_etiquette.pdf";
            System.IO.File.WriteAllBytes(openPDFFile, global::TELIO_ETIQUETTE.Properties.Resources.notice_application_etiquette);
            System.Diagnostics.Process.Start(openPDFFile);
        }

        private void Refresh_button(object sender, RoutedEventArgs e)
        // Refresh l'application.
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void ShowSetting(object sender, RoutedEventArgs e)
        // Afficher les paramètres.
        {
            if (Setting.Visibility == Visibility.Hidden)
                Setting.Visibility = Visibility.Visible;
            else if (Setting.Visibility == Visibility.Visible)
                Setting.Visibility = Visibility.Hidden;
        }

        private void Search_Path_principal(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers des débits principaux.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_principal_path = folderDlg.SelectedPath + @"\";
            if (new_principal_path != @"\")
                Text_Path_principal.Text = new_principal_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers des débits principaux.
            if (new_principal_path != @"\")
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    Properties.Settings.Default.path_principale_pvc = new_principal_path;
                    Properties.Settings.Default.Save();
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    Properties.Settings.Default.path_principal_alu = new_principal_path;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void Search_Path_complementaire(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers des débits complémentaires.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_complementaire_path = folderDlg.SelectedPath + @"\";
            if (new_complementaire_path != @"\")
                Text_Path_complementaire.Text = new_complementaire_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers des débits complémentaires.
            if (new_complementaire_path != @"\")
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    Properties.Settings.Default.path_complementaire_pvc = new_complementaire_path;
                    Properties.Settings.Default.Save();
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    Properties.Settings.Default.path_complementaire_alu = new_complementaire_path;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void Search_Path_silog(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers des fichiers des débits silog.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_silog_path = folderDlg.SelectedPath + @"\";
            if (new_silog_path != @"\")
                Text_Path_silog.Text = new_silog_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers des débits silog.
            if (new_silog_path != @"\")
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    Properties.Settings.Default.path_silog_pvc = new_silog_path;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void New_Printer(object sender, SelectionChangedEventArgs e)
        // Imprimante sélectionnée.
        {
            Properties.Settings.Default.printer = ComboBox_Printer.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void Sector(object sender, SelectionChangedEventArgs e)
        // Secteur sélectionné.
        {
            Properties.Settings.Default.sector = ComboBox_Sector.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        // Barre de recherche pour les lots.
        {
            Grid_AllData.Columns.Clear();
            ListBox_Profil.Items.Clear();
            Grid_AllData.Visibility = Visibility.Collapsed;
            Rect_grid.Visibility = Visibility.Collapsed;

            string word = SearchBox.Text.ToUpper();
            var list_lot_search = new List<string>();

            // Récupérer la liste des lots actuels.
            foreach (object liItem in ListBox_Lot.Items)
            {
                string result = liItem.ToString();
                list_lot_search.Add(result);
            }

            ListBox_Lot.Items.Clear();

            // Afficher les résultats correspondant au lot cherché.
            foreach (string lot in list_lot_search)
            {
                if (lot.Contains(word))
                {
                    ListBox_Lot.Items.Add(lot);
                }
            }
        }

        //-----------------------[IMPRIMER]-----------------------\\

        public void ConPrinter(string zpl)
        // Procédure pour envoyer un code ZPL à l'imprimante ZEBRA.
        {
            System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
            pd.PrinterSettings = new PrinterSettings();
            pd.PrinterSettings.PrinterName = ComboBox_Printer.Text;
            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, zpl);
        }

        public void ZPL(int i_length, string[,] array)
        // Construire le zpl à envoyer à l'impression.
        {
            for (int i = 0; i < i_length; i++)
            {
                string zpl = "^XA^LH18,20^FO20,080^BY2^B3N,N,40,Y,N^FD" + array[i, 1] + "^FS^CFD,30,14^FO010,008^FD" + array[i, 3] + "^FS^FO010,045^FD" + array[i, 5] +
                    "^FS^CFD,28,14^FO145,008^FD" + array[i, 4] + "^FS^FO250,008^FD" + array[i, 2] + "^FS^FO400,008^FD" + array[i, 12] + "^FS^FO600,008^FD" + array[i, 16] +
                    "^FS^FO250,045^FD" + array[i, 13] + "^FS^FO350,045^FD" + array[i, 14] + "^FS^FO450,045^FD" + array[i, 15] + "^FS^FO560,045^FD" + array[i, 7] +
                    "^FS^FO655,080^FD" + array[i, 6] + "^FS^CFD,30,18^FO560,080^FD" + array[i, 10] + "^FS^CFD,30,22^FO680,045^FD" + array[i, 8] + "^FS^XZ";

                ConPrinter(zpl);
            }
        }

        public void ZPL_ALU(int i_length, string[,] array)
        // Construire le zpl à envoyer à l'impression.
        {
            for (int i = 0; i < i_length; i++)
            {
                string zpl = array[i, 0];

                ConPrinter(zpl);
            }
        }

        private void Print_Lot(object sender, RoutedEventArgs e)
        // Imprimer chaque étiquette du lot sélectionné.
        {
            if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    if (ListBox_Lot.SelectedValue != null)
                    {
                        string lot_name = ListBox_Lot.SelectedValue.ToString();

                        String path_xml = Globals.PRINCIPAL_PATH;

                        string file = path_xml + lot_name + ".XML";

                        // Créer un document XML.
                        XmlDocument xmlDocument = new XmlDocument();

                        // Lire le fichier XML.
                        xmlDocument.Load(file);

                        // Créer une liste de nœuds XML avec l'expression XPath.
                        string path_balise;

                        if (lot_name.All(char.IsDigit) == false)
                            path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                        else
                            path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                        XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                        // Parcourir chaque étiquette du fichier XML.
                        foreach (XmlNode xmlNode in xmlNodeList)
                        {
                            string Data = xmlNode["PrintData"].InnerText;
                            string[] subs = Data.Split('\n');

                            var list_line_value = new List<string>();

                            // Supprimer les lignes vides.
                            RemoveLine(subs, list_line_value);

                            var list_data = new List<string>();

                            // Récuperer les données voulues.
                            SearchData(list_line_value, list_data);

                            if (lot_name.All(char.IsDigit) == false)
                            {
                                // Construire le zpl à envoyer à l'impression.
                                string zpl = "^XA^LH18,20^FO20,080^BY2^B3N,N,40,Y,N^FD" + list_data[1] + "^FS^CFD,30,14^FO010,008^FD" + list_data[3] + "^FS^FO010,045^FD" + list_data[5] +
                                    "^FS^CFD,28,14^FO145,008^FD" + list_data[4] + "^FS^FO250,008^FD" + list_data[2] + "^FS^FO400,008^FD" + list_data[11] + "^FS^FO600,008^FD" + list_data[16] +
                                    "^FS^FO250,045^FD" + list_data[13] + "^FS^FO350,045^FD" + list_data[14] + "^FS^FO450,045^FD" + list_data[15] + "^FS^FO560,045^FD" + list_data[7] +
                                    "^FS^FO655,080^FD" + list_data[6] + "^FS^CFD,30,18^FO560,080^FD" + list_data[10] + "^FS^CFD,30,22^FO680,045^FD" + list_data[8] + "^FS^XZ";

                                ConPrinter(zpl);
                            }
                            else
                            {
                                list_data[6] = list_data[6].Substring(4);

                                // Construire le zpl à envoyer à l'impression.
                                string zpl = "^XA^LH18,20^FO50,080^BY2^B3N,N,40,Y,N^FD" + list_data[9] + "^FS^CFD,30,14^FO010,008^FD" + list_data[6] + "^FS^FO010,045^FD" + list_data[3] +
                                     "^FS^CFD,28,14^FO145,008^FD" + "" + "^FS^FO250,008^FD" + list_data[1] + "^FS^FO400,008^FD" + list_data[5] + "^FS^FO600,008^FD" + list_data[11] +
                                     "^FS^FO250,045^FD" + list_data[10] + "^FS^FO350,045^FD" + "" + "^FS^FO450,045^FD" + "" + "^FS^FO560,045^FD" + list_data[2] +
                                     "^FS^FO665,080^FD" + list_data[7] + "^FS^CFD,30,18^FO560,080^FD" + list_data[8] + "^FS^CFD,30,22^FO680,045^FD" + list_data[4] + "^FS^XZ";

                                ConPrinter(zpl);
                            }
                        }
                    }
                    else { MessageBox.Show("Aucun lot sélectionné"); }
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
                    {
                        if (ListBox_Lot.SelectedValue != null)
                        {
                            string lot_name = ListBox_Lot.SelectedValue.ToString();

                            String path_xml = Globals.PRINCIPAL_PATH;
                            Globals.lot_name = lot_name;

                            // Créer un document XML.
                            XmlDocument xmlDocument = new XmlDocument();

                            string file_xml = path_xml + lot_name + ".XML";

                            // Lire le fichier XML.
                            xmlDocument.Load(file_xml);

                            // Créer une liste de nœuds XML avec l'expression XPath.
                            string path_balise;

                            if (lot_name.All(char.IsDigit) == false)
                                path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData/LabelPrintData";
                            else
                                path_balise = "/File/OptiCuttingData/OptiData/BarData/PieceData";

                            XmlNodeList xmlNodeList = xmlDocument.SelectNodes(path_balise);

                            var profil_name_duplicate = new List<string>();

                            // Parcourir chaque étiquette du fichier XML.
                            foreach (XmlNode xmlNode in xmlNodeList)
                            {
                                string Data = xmlNode["PrintData"].InnerText;

                                // Construire le zpl à envoyer à l'impression.
                                ConPrinter(Data);
                            }
                        }
                        else { MessageBox.Show("Aucun lot sélectionné"); }
                    }
                }
            }
            else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible || APPLICATION_SILOG.Visibility == Visibility.Visible)
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();

                    String path_ags = "";
                    if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.COMPLEMENTAIRE_PATH;
                    }
                    else if (APPLICATION_SILOG.Visibility == Visibility.Visible)
                    {
                        path_ags = Globals.SILOG_PATH;
                    }
                    Globals.lot_name = lot_name;

                    string file_ags = path_ags + lot_name + ".AGS";

                    // Lire le fichier AGS.
                    IEnumerable<string> lines = File.ReadLines(file_ags);

                    // Récupérer les références profilées.
                    foreach (string line in lines)
                    {
                        var list_data = new List<string>();

                        SearchData_AGS(line, list_data);

                        // Construire le zpl à envoyer à l'impression.
                        string zpl = "^XA^LH18,20^FO20,080^BY2^B3N,N,40,Y,N^FD" + list_data[6] + "^FS^CFD,30,14^FO010,008^FD" + list_data[8] + "^FS^FO010,045^FD" + list_data[4] +
                                   "^FS^CFD,28,14^FO145,008^FD" + list_data[9] + "^FS^FO250,008^FD" + list_data[7] + "^FS^FO400,008^FD" + list_data[13] + "^FS^FO600,008^FD" + list_data[3] +
                                   "^FS^FO250,045^FD" + list_data[14] + "^FS^FO350,045^FD" + "" + "^FS^FO450,045^FD" + "" + "^FS^FO560,045^FD" + list_data[10] +
                                   "^FS^FO655,080^FD" + list_data[2] + "^FS^CFD,30,18^FO560,080^FD" + list_data[0] + "^FS^CFD,30,22^FO680,045^FD" + list_data[1] + "^FS^XZ";

                        ConPrinter(zpl);
                    }
                }
                else { MessageBox.Show("Aucun lot sélectionné"); }
            }
        }

        private void Print_Profil(object sender, RoutedEventArgs e)
        // Imprimer chaque étiquette du profil sélectionné.
        {
            if (ListBox_Profil.SelectedValue != null)
            {
                if (ComboBox_Sector.SelectedItem == PVC)
                {
                    string[,] array_data = new string[Grid_AllData.Items.Count, 17];

                    for (int i = 0; i < Grid_AllData.Items.Count; i++)
                    {
                        // Récuperer les données du DataGrid.
                        AllData data = Grid_AllData.Items[i] as AllData;
                        if (data != null)
                        {
                            string dataDM = data.DM;
                            string dataCB = data.CB;
                            string dataLg = data.Lg;
                            string dataLot = data.Lot;
                            string dataColor = data.Couleur;
                            string dataRepere = data.Repere;
                            string dataKtn = data.Ktn;
                            string dataProfil = data.Profil;
                            string dataPosition = data.Position;
                            string dataInfo4 = data.Info4;
                            string dataCasier = data.Casier;
                            string dataNb_CB = data.Nb_CB;
                            string dataRenfort = data.Renfort;
                            string dataInfo1 = data.Info1;
                            string dataInfo2 = data.Info2;
                            string dataInfo3 = data.Info3;
                            string dataChariot = data.Chariot;

                            // Insérer les données dans un tableau.
                            array_data[i, 0] = dataDM;
                            array_data[i, 1] = dataCB;
                            array_data[i, 2] = dataLg;
                            array_data[i, 3] = dataLot;
                            array_data[i, 4] = dataColor;
                            array_data[i, 5] = dataRepere;
                            array_data[i, 6] = dataKtn;
                            array_data[i, 7] = dataProfil;
                            array_data[i, 8] = dataPosition;
                            array_data[i, 9] = dataInfo4;
                            array_data[i, 10] = dataCasier;
                            array_data[i, 11] = dataNb_CB;
                            array_data[i, 12] = dataRenfort;
                            array_data[i, 13] = dataInfo1;
                            array_data[i, 14] = dataInfo2;
                            array_data[i, 15] = dataInfo3;
                            array_data[i, 16] = dataChariot;
                        }
                    }

                    ZPL(Grid_AllData.Items.Count, array_data);
                }
                else if (ComboBox_Sector.SelectedItem == ALU)
                {
                    if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
                    {
                        string[,] array_data = new string[Grid_AllData.Items.Count, 1];

                        for (int i = 0; i < Grid_AllData.Items.Count; i++)
                        {
                            // Récuperer les données du DataGrid.
                            AllData data = Grid_AllData.Items[i] as AllData;
                            if (data != null)
                            {
                                string dataZPL = data.CB;

                                // Insérer les données dans un tableau.
                                array_data[i, 0] = dataZPL;
                            }
                        }

                        ZPL_ALU(Grid_AllData.Items.Count, array_data);
                    }
                    else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                    {
                        string[,] array_data = new string[Grid_AllData.Items.Count, 17];

                        for (int i = 0; i < Grid_AllData.Items.Count; i++)
                        {
                            // Récuperer les données du DataGrid.
                            AllData data = Grid_AllData.Items[i] as AllData;
                            if (data != null)
                            {
                                string dataDM = data.DM;
                                string dataCB = data.CB;
                                string dataLg = data.Lg;
                                string dataLot = data.Lot;
                                string dataColor = data.Couleur;
                                string dataRepere = data.Repere;
                                string dataKtn = data.Ktn;
                                string dataProfil = data.Profil;
                                string dataPosition = data.Position;
                                string dataInfo4 = data.Info4;
                                string dataCasier = data.Casier;
                                string dataNb_CB = data.Nb_CB;
                                string dataRenfort = data.Renfort;
                                string dataInfo1 = data.Info1;
                                string dataInfo2 = data.Info2;
                                string dataInfo3 = data.Info3;
                                string dataChariot = data.Chariot;

                                // Insérer les données dans un tableau.
                                array_data[i, 0] = dataDM;
                                array_data[i, 1] = dataCB;
                                array_data[i, 2] = dataLg;
                                array_data[i, 3] = dataLot;
                                array_data[i, 4] = dataColor;
                                array_data[i, 5] = dataRepere;
                                array_data[i, 6] = dataKtn;
                                array_data[i, 7] = dataProfil;
                                array_data[i, 8] = dataPosition;
                                array_data[i, 9] = dataInfo4;
                                array_data[i, 10] = dataCasier;
                                array_data[i, 11] = dataNb_CB;
                                array_data[i, 12] = dataRenfort;
                                array_data[i, 13] = dataInfo1;
                                array_data[i, 14] = dataInfo2;
                                array_data[i, 15] = dataInfo3;
                                array_data[i, 16] = dataChariot;
                            }
                        }

                        ZPL(Grid_AllData.Items.Count, array_data);
                    }
                }
            }
            else { MessageBox.Show("Aucun profil sélectionné"); }
        }

        private void Print_Etiquette(object sender, RoutedEventArgs e)
        // Imprimer les étiquettes sélectionnées.
        {
            if (ComboBox_Sector.SelectedItem == PVC)
            {
                // Définir la taille du tableau de données.
                int length_array = 0;
                for (int i = 0; i < Grid_AllData.Items.Count; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    if (data != null)
                    {
                        if (data.Choisi == true)
                        {
                            ++length_array;
                        }
                    }
                }

                int index = 0;
                string[,] array_data = new string[length_array, 17];

                for (int i = 0; i < Grid_AllData.Items.Count; i++)
                {
                    // Récuperer les données du DataGrid.
                    AllData data = Grid_AllData.Items[i] as AllData;
                    if (data != null)
                    {
                        string dataDM = data.DM;
                        string dataCB = data.CB;
                        string dataLg = data.Lg;
                        string dataLot = data.Lot;
                        string dataColor = data.Couleur;
                        string dataRepere = data.Repere;
                        string dataKtn = data.Ktn;
                        string dataProfil = data.Profil;
                        string dataPosition = data.Position;
                        string dataInfo4 = data.Info4;
                        string dataCasier = data.Casier;
                        string dataNb_CB = data.Nb_CB;
                        string dataRenfort = data.Renfort;
                        string dataInfo1 = data.Info1;
                        string dataInfo2 = data.Info2;
                        string dataInfo3 = data.Info3;
                        string dataChariot = data.Chariot;

                        // Insérer les données dans un tableau si elle sont sélectionnées.
                        if (data.Choisi == true)
                        {
                            array_data[index, 0] = dataDM;
                            array_data[index, 1] = dataCB;
                            array_data[index, 2] = dataLg;
                            array_data[index, 3] = dataLot;
                            array_data[index, 4] = dataColor;
                            array_data[index, 5] = dataRepere;
                            array_data[index, 6] = dataKtn;
                            array_data[index, 7] = dataProfil;
                            array_data[index, 8] = dataPosition;
                            array_data[index, 9] = dataInfo4;
                            array_data[index, 10] = dataCasier;
                            array_data[index, 11] = dataNb_CB;
                            array_data[index, 12] = dataRenfort;
                            array_data[index, 13] = dataInfo1;
                            array_data[index, 14] = dataInfo2;
                            array_data[index, 15] = dataInfo3;
                            array_data[index, 16] = dataChariot;

                            index++;
                        }
                    }
                }

                ZPL(length_array, array_data);
            }
            else if (ComboBox_Sector.SelectedItem == ALU)
            {
                // Définir la taille du tableau de données.
                int length_array = 0;
                for (int i = 0; i < Grid_AllData.Items.Count; i++)
                {
                    AllData data = Grid_AllData.Items[i] as AllData;
                    if (data != null)
                    {
                        if (data.Choisi == true)
                        {
                            ++length_array;
                        }
                    }
                }

                int index = 0;
                if (APPLICATION_PRINCIPAL.Visibility == Visibility.Visible)
                {
                    string[,] array_data = new string[length_array, 1];

                    for (int i = 0; i < Grid_AllData.Items.Count; i++)
                    {
                        // Récuperer les données du DataGrid.
                        AllData data = Grid_AllData.Items[i] as AllData;
                        if (data != null)
                        {
                            string dataZPL = data.CB;

                            // Insérer les données dans un tableau si elle sont sélectionnées.
                            if (data.Choisi == true)
                            {
                                array_data[index, 0] = dataZPL;

                                index++;
                            }
                        }
                    }

                    ZPL_ALU(length_array, array_data);
                }
                else if (APPLICATION_COMPLEMENTAIRE.Visibility == Visibility.Visible)
                {
                    string[,] array_data = new string[length_array, 17];

                    for (int i = 0; i < Grid_AllData.Items.Count; i++)
                    {
                        // Récuperer les données du DataGrid.
                        AllData data = Grid_AllData.Items[i] as AllData;
                        if (data != null)
                        {
                            string dataDM = data.DM;
                            string dataCB = data.CB;
                            string dataLg = data.Lg;
                            string dataLot = data.Lot;
                            string dataColor = data.Couleur;
                            string dataRepere = data.Repere;
                            string dataKtn = data.Ktn;
                            string dataProfil = data.Profil;
                            string dataPosition = data.Position;
                            string dataInfo4 = data.Info4;
                            string dataCasier = data.Casier;
                            string dataNb_CB = data.Nb_CB;
                            string dataRenfort = data.Renfort;
                            string dataInfo1 = data.Info1;
                            string dataInfo2 = data.Info2;
                            string dataInfo3 = data.Info3;
                            string dataChariot = data.Chariot;

                            // Insérer les données dans un tableau si elle sont sélectionnées.
                            if (data.Choisi == true)
                            {
                                array_data[index, 0] = dataDM;
                                array_data[index, 1] = dataCB;
                                array_data[index, 2] = dataLg;
                                array_data[index, 3] = dataLot;
                                array_data[index, 4] = dataColor;
                                array_data[index, 5] = dataRepere;
                                array_data[index, 6] = dataKtn;
                                array_data[index, 7] = dataProfil;
                                array_data[index, 8] = dataPosition;
                                array_data[index, 9] = dataInfo4;
                                array_data[index, 10] = dataCasier;
                                array_data[index, 11] = dataNb_CB;
                                array_data[index, 12] = dataRenfort;
                                array_data[index, 13] = dataInfo1;
                                array_data[index, 14] = dataInfo2;
                                array_data[index, 15] = dataInfo3;
                                array_data[index, 16] = dataChariot;

                                index++;
                            }
                        }
                    }

                    ZPL(length_array, array_data);
                }
            }
        }
    }

    public class RawPrinterHelper
    {
        // Déclarations de structure et d'API :
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)] public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)] public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)] public string pDataType;
        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        // Lorsque la fonction reçoit un nom d'imprimante et un tableau non géré
        // d'octets, la fonction envoie ces octets à la file d'attente d'impression.
        // Renvoie vrai en cas de succès, faux en cas d'échec.
        {
            Int32 dwError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false; // Supposez l'échec à moins que vous ne réussissiez spécifiquement.

            di.pDocName = "My C#.NET RAW Document";
            di.pDataType = "RAW";

            // Ouvrez l'imprimante.
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Lancez un document.
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Lancez la page.
                    if (StartPagePrinter(hPrinter))
                    {
                        // Écrivez vos octets.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }
            // Si vous n'avez pas réussi, GetLastError peut donner plus d'informations.
            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }

        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;
            Int32 dwCount;
            // Combien de caractères y a-t-il dans la chaîne ?
            dwCount = szString.Length;
            // Supposons que l'imprimante attend du texte ANSI, puis convertissez
            // la chaîne en texte ANSI.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Envoie la chaîne ANSI convertie à l'imprimante.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }
    }
}



/*-------------CODE WPF DE L'APPLICATION-------------*/


<Window x:Class="TELIO_ETIQUETTE.MainWindow"
        WindowStartupLocation="CenterScreen"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        mc:Ignorable="d"
        Title="Télio ETIQUETTE" Height="900" Width="1200" Background="#272537" WindowState="Normal" ResizeMode="CanResize" MinHeight="900" MinWidth="1200" PreviewKeyDown="PreviewKeyDownSelectAll" KeyUp="PreviewKeyUp_Multiple_Selection" KeyDown="KeyDownUnSelectAll">
    <Grid VerticalAlignment="Center" HorizontalAlignment="Center" Width="1180" Height="869">

        <Canvas Name="APPLICATION_STRUCTURE">
            <Canvas>
                <TextBox Name="SearchBox" Text="Rechercher" ToolTip="Rechercher un lot" GotFocus="PlaceHolder" LostFocus="AddText" HorizontalAlignment="Left" Height="33" Margin="71,24,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="210" BorderThickness="3" BorderBrush="#FF5D5881" FontSize="18" TextAlignment="Center" KeyUp="SearchBox_KeyUp" Foreground="#99000000"/>
                <Button Click="Refresh_button" ToolTip="Annuler la recherche" Cursor="Hand" Canvas.Left="286" Width="44" Height="47" Canvas.Top="25">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/close.png" Margin="0,0,16,15"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <Canvas>
                <Rectangle Height="707" Margin="27,115,0,0" Width="307" Fill="#CCFFFFFF"/>
                <Rectangle Height="25" Margin="27,90,0,0" Width="307" Fill="#FFD4D3D7"/>
            </Canvas>
            <Canvas>
                <Rectangle Height="293" Margin="397,115,0,0" Width="236" Fill="#CCFFFFFF"/>
                <Rectangle Height="25" Margin="397,90,0,0" Width="236" Fill="#FFD4D3D7"/>
            </Canvas>
            <Canvas>
                <Rectangle Height="34" Margin="700,90,0,0" Width="454" Fill="#FFD4D3D7"/>
                <Rectangle Height="698" Width="454" Fill="#FFD4D3D7" Canvas.Left="700" Canvas.Top="124" ScrollViewer.HorizontalScrollBarVisibility="Visible"/>
                <Rectangle Name="Rect_grid" Visibility="Collapsed" Fill="#FF5D5881" Height="34" Canvas.Left="888" Canvas.Top="90" Width="266"/>
            </Canvas>
            <Canvas>
                <Button Name="Display_Principaux" Click="Button_Principal" ToolTip="Traiter les fichiers des Débits Principaux" Content="Débits Principaux" Height="32" Canvas.Top="119" BorderBrush="#FF272537" Foreground="#FF272537"/>
                <Button Name="Display_Complementaires" Click="Button_Complementaire" ToolTip="Traiter les fichiers des Débits Complémentaires" Content="Débits Complémentaires" Height="32" Canvas.Top="119" BorderBrush="#FF272537" Foreground="#FF272537" Background="#FFDDDDDD"/>
                <Button Visibility="Collapsed" Name="Display_Silog" Click="Button_Silog" ToolTip="Traiter les fichiers des Débits Silog" Content="Débits Silog" Width="71" Height="32" Canvas.Left="264" Canvas.Top="119" BorderBrush="#FF272537" Foreground="#FF272537" Background="#FFDDDDDD"/>
            </Canvas>

            <Canvas>
                <Button Name="Imprimer_Lot"  HorizontalContentAlignment="Center" HorizontalAlignment="Left" Margin="397,492,0,0" VerticalAlignment="Top" Width="236" Height="70" Click="Print_Lot" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand">
                    <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                        <Image VerticalAlignment="Center" Source="/images/print.png" Stretch="None" Width="85" />
                        <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="121">Imprimer Lot</TextBlock>
                    </StackPanel>
                </Button>
                <Button Name="Imprimer_Profil" HorizontalContentAlignment="Center" HorizontalAlignment="Left" Margin="397,609,0,0" VerticalAlignment="Top" Width="236" Height="70" Click="Print_Profil" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand">
                    <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                        <Image VerticalAlignment="Center" Source="/images/print.png" Stretch="None" Width="85" />
                        <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="121">Imprimer Profil</TextBlock>
                    </StackPanel>
                </Button>
                <Button Name="Imprimer_Etiquette" HorizontalContentAlignment="Center" HorizontalAlignment="Left" Margin="397,726,0,0" VerticalAlignment="Top" Width="236" Height="70" Click="Print_Etiquette" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand">
                    <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                        <Image VerticalAlignment="Center" Source="/images/print.png" Stretch="None" Width="85" />
                        <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="151">Imprimer Etiquette</TextBlock>
                    </StackPanel>
                </Button>
            </Canvas>
            <Canvas>
                <Button Click="Notice" ToolTip="Notice" Cursor="Hand" Canvas.Left="1012" Canvas.Top="17" Width="50">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/interrogation.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
                <Button Click="Refresh_button" ToolTip="Redémarrer l'application" Cursor="Hand" Canvas.Left="1062" Canvas.Top="13" Width="55" Height="58">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/refresh.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
                <Button Margin="1117,17,17,805" Click="ShowSetting" ToolTip="Paramètres" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/setting.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
        </Canvas>

        <Canvas Name="APPLICATION_PRINCIPAL" Visibility="Hidden"/>
        <Canvas Name="APPLICATION_COMPLEMENTAIRE" Visibility="Hidden"/>
        <Canvas Name="APPLICATION_SILOG" Visibility="Hidden"/>

        <Canvas>
            <ListBox Name="ListBox_Lot" Visibility="Hidden" ToolTip="Sélectionnez votre lot" ItemContainerStyle="{DynamicResource _ListBoxItemStyle}" SelectionMode="Single" HorizontalAlignment="Left" VerticalAlignment="Top" Width="306" Background="#FFD4D3D7" BorderBrush="#FFD4D3D7" FontSize="20" Cursor="Hand" Canvas.Left="28" Canvas.Top="152" MaxHeight="670">
                <ListBox.Resources>
                    <Style TargetType="ListBox">
                        <EventSetter Event="MouseUp" Handler="Select_Lot"/>
                    </Style>
                </ListBox.Resources>
            </ListBox>
            <ListBox Name="ListBox_Profil" Visibility="Hidden" ToolTip="Sélectionnez votre profil" ItemContainerStyle="{DynamicResource _ListBoxItemStyle}" HorizontalAlignment="Left" Margin="397,115,0,0" VerticalAlignment="Top" Width="236" Background="#FFD4D3D7" BorderBrush="#FFD4D3D7" FontSize="20" Cursor="Hand" MaxHeight="293">
                <ListBox.Resources>
                    <Style TargetType="ListBox">
                        <EventSetter Event="MouseUp" Handler="Select_Profil"/>
                    </Style>
                </ListBox.Resources>
            </ListBox>
            <DataGrid Name="Grid_AllData" Visibility="Hidden" ToolTip="Sélectionnez vos étiquettes à imprimer" Cursor="Hand" CanUserAddRows="False" HorizontalAlignment="Left" VerticalAlignment="Top" Width="465" FontSize="14" MinColumnWidth="60" ColumnWidth="SizeToCells" Canvas.Left="690" Canvas.Top="116" BorderBrush="#00688CAF" MaxHeight="706">
                <DataGrid.Resources>
                    <Style TargetType="DataGridCell">
                        <EventSetter Event="PreviewMouseLeftButtonUp" Handler="OneClick_Checkbox"/>
                        <Setter Property="TextBlock.TextAlignment" Value="Center"/>
                    </Style>
                    <Style BasedOn="{StaticResource {x:Type DataGridColumnHeader}}" TargetType="{x:Type DataGridColumnHeader}" >
                        <Setter Property="Background" Value="#FF5D5881" />
                        <Setter Property="Foreground" Value="White" />
                        <Setter Property="BorderBrush" Value="Black"/>
                        <Setter Property="BorderThickness" Value="1 1 1 1"/>
                        <Setter Property="Margin" Value="-1,-1,0,0" />
                        <Setter Property="Height" Value="28" />
                        <Setter Property="Width" Value="auto"/>
                        <Setter Property="HorizontalContentAlignment" Value="Center"/>
                    </Style>
                </DataGrid.Resources>
                <DataGrid.ContextMenu>
                    <ContextMenu>
                        <MenuItem Header="Tout sélectionner" Click="Select_All" InputGestureText="CTRL+1">
                            <MenuItem.Icon>
                                <Image Source="/images/checkbox.png"/>
                            </MenuItem.Icon>
                        </MenuItem>
                        <MenuItem Header="Tout retirer" Click="UnSelect_All" InputGestureText="CTRL+2">
                            <MenuItem.Icon>
                                <Image Source="/images/black_close.png"/>
                            </MenuItem.Icon>
                        </MenuItem>
                        <MenuItem Header="Sélectionner les éléments choisis" Click="Multiple_Selection" InputGestureText="CTRL+3">
                            <MenuItem.Icon>
                                <Image Source="/images/list.png"/>
                            </MenuItem.Icon>
                        </MenuItem>
                    </ContextMenu>
                </DataGrid.ContextMenu>
            </DataGrid>
        </Canvas>


        <Canvas>
            <Rectangle Fill="#272537" Height="704" Canvas.Left="673" Canvas.Top="119" Width="27"/>

            <Rectangle Fill="#FF5D5881" Height="50" Margin="10,69,0,0" Width="209" RenderTransformOrigin="0.404,0.46"/>
            <Image Source="/images/lot.png" Height="57" Width="39" Canvas.Left="27" Canvas.Top="66"/>
            <Label Content="Lot" Margin="71,62,0,0" FontSize="36" Foreground="White" RenderTransformOrigin="1.417,0.586"/>

            <Rectangle Fill="#FF5D5881" Height="50" Margin="380,69,0,0" Width="209" RenderTransformOrigin="0.404,0.46"/>
            <Label Content="Profil" Margin="440,62,0,0" FontSize="36" Foreground="White"/>
            <Image Source="/images/profil.png" Height="47" Width="38" Canvas.Left="397" Canvas.Top="70"/>

            <Rectangle Fill="#FF5D5881" Height="50" Margin="683,69,0,0" Width="210" RenderTransformOrigin="0.404,0.46"/>
            <Label Content="Données" Margin="730,61,0,0" FontSize="36" Foreground="White"/>
            <Image Source="/images/data.png" Height="55" Width="38" Canvas.Left="695" Canvas.Top="66"/>
        </Canvas>

        <Canvas Name="Setting" Visibility="Hidden">
            <Border Name="border_settings" Width="448" Background="#272537" CornerRadius="10" Canvas.Left="723" Canvas.Top="68"/>
            <Canvas>
                <TextBlock Canvas.Left="740" Canvas.Top="85" FontSize="13" Foreground="White"><Run Text="Secteur d'activité :"/></TextBlock>
                <ComboBox Name="ComboBox_Sector" SelectionChanged="Sector" Canvas.Left="740" Canvas.Top="118" Width="267" BorderBrush="White">
                    <ComboBoxItem Name="PVC">PVC</ComboBoxItem>
                    <ComboBoxItem Name="ALU">ALU</ComboBoxItem>
                </ComboBox>
                <Image Source="/images/factory.png" Canvas.Left="1021" Canvas.Top="115" Height="29" Width="32" RenderTransformOrigin="0.545,1.253"/>
            </Canvas>
            <Canvas>
                <TextBlock Canvas.Left="739" Canvas.Top="167" FontSize="13" Foreground="White"><Run Text="Saisissez le chemin d'accès vers les fichiers des débits principaux :"/></TextBlock>
                <TextBox Name="Text_Path_principal" PreviewMouseDown="Search_Path_principal" IsReadOnly="True" Canvas.Left="739" Canvas.Top="193" Width="336" Height="33" HorizontalAlignment="Center" Padding="5" VerticalContentAlignment="Center" FontSize="16" FontStyle="Italic"/>
                <Button Click="Search_Path_principal" Canvas.Left="1087" Canvas.Top="193" Height="33" Width="34" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/data.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <Canvas>
                <TextBlock Canvas.Left="739" Canvas.Top="260" FontSize="13" Foreground="White"><Run Text="Saisissez le chemin d'accès vers les fichiers des débits complémentaires :"/></TextBlock>
                <TextBox Name="Text_Path_complementaire" PreviewMouseDown="Search_Path_complementaire" IsReadOnly="True" Canvas.Left="739" Canvas.Top="286" Width="336" Height="33" HorizontalAlignment="Center" Padding="5" VerticalContentAlignment="Center" FontSize="16" FontStyle="Italic"/>
                <Button Click="Search_Path_complementaire" Canvas.Left="1089" Canvas.Top="286" Height="33" Width="34" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/data.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <Canvas Name="path_silog" Visibility="Collapsed">
                <TextBlock Canvas.Left="739" Canvas.Top="352" FontSize="13" Foreground="White"><Run Text="Saisissez le chemin d'accès vers les fichiers des débits silog :"/></TextBlock>
                <TextBox Name="Text_Path_silog" PreviewMouseDown="Search_Path_silog" IsReadOnly="True" Canvas.Left="739" Canvas.Top="374" Width="336" Height="32" HorizontalAlignment="Center" Padding="5" VerticalContentAlignment="Center" FontSize="16" FontStyle="Italic"/>
                <Button Click="Search_Path_silog" Canvas.Left="1089" Canvas.Top="374" Height="33" Width="34" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/data.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <Canvas Name="bottom_settings">
                <Canvas>
                    <TextBlock Canvas.Left="739" Canvas.Top="348" Foreground="White" FontSize="14.5" Margin="0">Sélection de l'imprimante :</TextBlock>
                    <ComboBox Name="ComboBox_Printer" SelectionChanged="New_Printer" Canvas.Left="739" Canvas.Top="374" Width="267" BorderBrush="White"/>
                    <Image Source="/images/print.png" Canvas.Left="1019" Canvas.Top="371" Height="29" Width="33"/>
                </Canvas>
                <TextBlock Canvas.Left="739" Canvas.Top="419" Foreground="#FF999494" FontSize="10"><Run Text="Veuillez redémarrer l'application après modification"/></TextBlock>
                <Button Click="Refresh_button" Canvas.Left="970" Canvas.Top="414" Height="26" Width="14" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/refresh.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <Canvas.Effect>
                <DropShadowEffect BlurRadius="25" Direction="-90" RenderingBias="Quality" ShadowDepth="2"/>
            </Canvas.Effect>
        </Canvas>

    </Grid>
    <Window.Resources>
        <Style x:Key="_ListBoxItemStyle" TargetType="ListBoxItem">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="ListBoxItem">
                        <Border Name="_Border"
                                Padding="2"
                                SnapsToDevicePixels="true">
                            <ContentPresenter />
                        </Border>
                        <ControlTemplate.Triggers>
                            <Trigger Property="IsSelected" Value="true">
                                <Setter TargetName="_Border" Property="Background" Value="#272537"/>
                                <Setter Property="Foreground" Value="White"/>
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>
    </Window.Resources>
</Window>`
application_etiquette.innerText += str_etiquette;


let application_chariot = document.getElementById("application_chariot");
let str_chariot = `using System;
using System.Collections.Generic;
using Syncfusion.XlsIO;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Xml;
using Brushes = System.Windows.Media.Brushes;
using Color = System.Windows.Media.Color;
using GemBox.Spreadsheet;
using System.Configuration;
using System.Reflection;

namespace TELIO_CHARIOT
{
    public partial class MainWindow : System.Windows.Window
    {
        //-----------------------[STYLE]-----------------------\\

        public void PlaceHolder(object sender, EventArgs e)
        // Supprimer le placeholder à SearchBox.
        {
            if (SearchBox.Text == "Rechercher")
            {
                SearchBox.Text = "";
            }
            SearchBox.Foreground = System.Windows.Media.Brushes.Black;
        }

        public void AddText(object sender, EventArgs e)
        // Ajouter le placeholder à SearchBox.
        {
            if (string.IsNullOrWhiteSpace(SearchBox.Text))
            {
                SearchBox.Text = "Rechercher";
            }
            SearchBox.Foreground = Brushes.Gray;
        }

        //-----------------------[INTERACTION]-----------------------\\

        public class DataChariot
        {
            public int Carriage { get; set; }
            public string EDN { get; set; }
            public int Chariot { get; set; }
            public int Emplacement { get; set; }
            public string Type { get; set; }
            public string Profil { get; set; }
            public string Position { get; set; }
            public string Casier { get; set; }
            public string Longueur { get; set; }
            public string Ktn { get; set; }
        }

        public class Chariot
        {
            public string Name { get; set; }
            public string Full_Name { get; set; }
            public string Colonne { get; set; }
            public string Ligne { get; set; }
            public string Type { get; set; }
            public int Max_NB_Chariot { get; set; }
            public bool Utiliser { get; set; }
        }
        
        public static class Globals
        // Variables globales.
        {
            public static String lot_name = "";
            public static String InterHM_PATH = Properties.Settings.Default.path_InterHM;
            public static String Ramasoft_PATH = Properties.Settings.Default.path_Ramasoft;
            public static String path_excel = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\Print.xlsx";
            public static List<DataChariot> DataChariot = new List<DataChariot>();
            public static List<Chariot> Chariot = new List<Chariot>();
        }

        public MainWindow()
        {
            InitializeComponent();

            // Récupérer les chariots enregistrés et ses paramètres.
            List<Chariot> Chariot = Globals.Chariot;
            foreach (SettingsProperty PropertyName in Properties.Settings.Default.Properties)
            {
                if (PropertyName.Name.StartsWith("C") == true && PropertyName.Name.Substring(1).All(char.IsDigit) == true)
                {
                    string result = Properties.Settings.Default[PropertyName.Name].ToString();
                    string[] subs = result.Split(';');
                    Chariot.Add(new Chariot() { Name = PropertyName.Name, Colonne = subs[0].Substring(11), Ligne = subs[1].Substring(9), Type = subs[2] });
                }
            }
            Globals.Chariot = Chariot;

            // Récupérer les chemins d'accès aux fichiers.
            Text_Path_InterHM.Text = Globals.InterHM_PATH;
            Text_Path_Ramasoft.Text = Globals.Ramasoft_PATH;

            // Récupérer les imprimantes connectées.
            ComboBox_Printer.Items.Add(Properties.Settings.Default.printer);
            ComboBox_Printer.SelectedItem = Properties.Settings.Default.printer;

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (printer != Properties.Settings.Default.printer)
                    ComboBox_Printer.Items.Add(printer);
            }
        }

        public void Button_Display(System.Windows.Controls.Button Display_Active, System.Windows.Controls.Button Display_Deactivate, Canvas Application_Active, Canvas Application_Deactivate)
        // Afficher la partie de traitement voulue.
        {
            Display_Active.Background = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            Display_Active.Foreground = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));

            Display_Deactivate.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
            Display_Deactivate.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));

            Application_Active.Visibility = Visibility.Visible;
            Application_Deactivate.Visibility = Visibility.Collapsed;

            ListBox_Lot.Items.Clear();

            ListBox_Lot.SelectedIndex = -1;
            ListBox_Lot.Visibility = Visibility.Visible;

            SearchBox.Text = "Rechercher";
        }

        private void Button_InterHM(object sender, RoutedEventArgs e)
        // Afficher la partie InterHM.
        {
            Button_Display(Display_InterHM, Display_Ramasoft, APPLICATION_InterHM, APPLICATION_Ramasoft);

            String path_InterHM = Globals.InterHM_PATH;

            try
            {
                // Récupérer tous les fichiers XML présents dans le dossier.
                string[] files = Directory.GetFiles(path_InterHM, "*.$ET");
                foreach (string file in files)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_InterHM.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_InterHM.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        private void Button_Ramasoft(object sender, RoutedEventArgs e)
        // Afficher la partie Ramasoft.
        {
            Button_Display(Display_Ramasoft, Display_InterHM, APPLICATION_Ramasoft, APPLICATION_InterHM);

            String path_Ramasoft = Globals.Ramasoft_PATH;

            try
            {
                // Récupérer tous les fichiers $ET présents dans le dossier.
                string[] files = Directory.GetFiles(path_Ramasoft, "*.XML");
                foreach (string file in files)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
                Display_Ramasoft.Background = new SolidColorBrush(Color.FromArgb(255, 221, 211, 221));
                Display_Ramasoft.Foreground = new SolidColorBrush(Color.FromArgb(255, 39, 37, 55));
            }
        }

        public void RemoveLine(string[] subs, List<string> list_line_value)
        // Supprimer les lignes vides.
        {
            foreach (var sub in subs)
            {
                if (sub.Length > 30)
                {
                    list_line_value.Add(sub);
                }
            }
        }

        public void SearchData(List<string> list_search, List<string> list_find)
        // Récuperer les données voulues.
        {
            foreach (string data in list_search)
            {
                if (data.Contains("$027DC"))
                {
                    int pFrom = data.IndexOf("$027DC") + "$027DC".Length;
                    int pTo = data.LastIndexOf("");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027B"))
                {
                    int pFrom = data.IndexOf("*") + "*".Length;
                    int pTo = data.LastIndexOf("*");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("/"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("$027q1");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XM"))
                {
                    int pFrom = data.IndexOf("$027XM") + "$027XM".Length;
                    int pTo = data.LastIndexOf("");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
                else if (data.Contains("$027XS"))
                {
                    int pFrom = data.IndexOf("$027XS") + "$027XS".Length;
                    int pTo = data.LastIndexOf("");

                    string result = data.Substring(pFrom, pTo - pFrom);
                    list_find.Add(result);
                }
            }
        }

        private void Select_Lot(object sender, RoutedEventArgs e)
        // Action lors de la sélection du lot.
        {
            try
            {
                if (ListBox_Lot.SelectedValue != null)
                {
                    string lot_name = ListBox_Lot.SelectedValue.ToString();
                    string path_Ramasoft = Globals.Ramasoft_PATH;
                    string path_InterHM = Globals.InterHM_PATH;
                    Globals.lot_name = lot_name;
                    Globals.DataChariot.Clear();
                    ListBox_Type.Items.Clear();
                    ListBox_Chariot.Items.Clear();

                    List<DataChariot> DataChariot = Globals.DataChariot;
                    List<Chariot> Chariot_Propriete = Globals.Chariot;
                    foreach (var data in Chariot_Propriete)
                    {
                        data.Utiliser = false;
                        data.Max_NB_Chariot = 0;
                    }

                    if (APPLICATION_Ramasoft.Visibility == Visibility.Visible)
                    // Ramasoft.
                    {
                        // Créer un document XML.
                        XmlDocument xmlDocument = new XmlDocument();

                        string file_xml = path_Ramasoft + lot_name + ".XML";

                        // Lire le fichier XML.
                        xmlDocument.Load(file_xml);

                        // Créer une liste de nœuds XML avec l'expression XPath.
                        XmlNodeList nodeList = xmlDocument.SelectNodes("/File/OptiCuttingData/OptiData/BarData/PieceData");

                        // Récuperer les emplacements des pièces et leurs contenus.
                        foreach (XmlNode elem in nodeList)
                        {
                            // Récupérer le contenu de la pièce.
                            string Data;
                            if (lot_name.All(char.IsDigit) == false)
                                Data = elem["LabelPrintData"].InnerText;
                            else
                                Data = elem["PrintData"].InnerText;

                            string[] subs = Data.Split('\n');

                            // Supprimer les lignes vides.
                            var list_line_value = new List<string>();
                            RemoveLine(subs, list_line_value);

                            // Récuperer les données voulues.
                            var list_data = new List<string>();
                            SearchData(list_line_value, list_data);

                            // Récupérer l'emplacement de la pièce.
                            string CarriageNo = elem.Attributes[3].Value;
                            int Carriage = Convert.ToInt32(CarriageNo);

                            string CaseNo = elem.Attributes[4].Value;
                            int Chariot = Convert.ToInt32(CaseNo.Substring(0, 1));
                            if (Convert.ToInt32(CaseNo) == 0)
                            {
                                if (lot_name.All(char.IsDigit) == false)
                                    Chariot = Convert.ToInt32(list_data[16].Substring(0, 1));
                                else
                                    Chariot = Convert.ToInt32(list_data[11].Substring(0, 1));
                            }

                            // Connaître les différents types utilisés et le nombre de chariots.
                            foreach (var data in Chariot_Propriete)
                            {
                                if (Carriage == Convert.ToInt32(data.Name.Substring(1)))
                                {
                                    data.Utiliser = true;
                                    if (Chariot > data.Max_NB_Chariot)
                                    {
                                        data.Max_NB_Chariot = Chariot;
                                    }
                                }
                            }

                            // Emplacement de chaque pièce.
                            int emplacement = 0;
                            if (Convert.ToInt32(CaseNo) != 0)
                            {
                                emplacement = ((Convert.ToInt32(CaseNo.Substring(1, 2)) - 1) * 4) + Convert.ToInt32(CaseNo.Substring(3, 1));
                            }
                            else
                            {
                                if (lot_name.All(char.IsDigit) == false)
                                    emplacement = Convert.ToInt32(list_data[16].Substring(2));
                                else
                                    emplacement = Convert.ToInt32(list_data[11].Substring(2));
                            }

                            // Ajouter les données à la collection.
                            if (Chariot != 0)
                            {
                                if (lot_name.All(char.IsDigit) == false)
                                    DataChariot.Add(new DataChariot() { Carriage = Carriage, Chariot = Chariot, Emplacement = emplacement, Type = list_data[10].Substring(0, 1), Profil = list_data[7], Position = list_data[8], Casier = list_data[10], Longueur = list_data[2], Ktn = list_data[6] });
                                else
                                    DataChariot.Add(new DataChariot() { Carriage = Carriage, Chariot = Chariot, Emplacement = emplacement, Type = list_data[8].Substring(0, 1), Profil = list_data[2], Position = list_data[4], Casier = list_data[8], Longueur = list_data[1], Ktn = list_data[7] });
                            }
                        }
                        // Traiter les cas : CarriageNo = 0.
                        if (DataChariot.Any(p => p.Carriage == 1) == true)
                        {
                            foreach (var data in DataChariot)
                            {
                                if (data.Carriage == 0)
                                    if (data.Type == "O")
                                        data.Carriage = 1;
                                    else if (data.Type == "D")
                                        if (DataChariot.Any(p => p.Carriage == 2) == true)
                                            data.Carriage = 2;
                                        else if (DataChariot.Any(p => p.Carriage == 5) == true)
                                            data.Carriage = 5;
                            }
                        }
                        if (DataChariot.Any(p => p.Carriage == 2) == true)
                        {
                            foreach (var data in DataChariot)
                            {
                                if (data.Carriage == 0)
                                    if (data.Type == "O")
                                        data.Carriage = 1;
                                    else if (data.Type == "D")
                                        data.Carriage = 2;
                            }
                        }
                        if (DataChariot.Any(p => p.Carriage == 5) == true)
                        {
                            foreach (var data in DataChariot)
                            {
                                if (data.Carriage == 0)
                                    if (data.Type == "O")
                                        data.Carriage = 1;
                                    else if (data.Type == "D")
                                        data.Carriage = 5;
                            }
                        }

                        // Accéder à cette liste avec les fonctions Print.
                        Globals.DataChariot = DataChariot;
                        Globals.Chariot = Chariot_Propriete;

                        // Afficher les différents types.
                        foreach (var data in Chariot_Propriete)
                        {
                            if (data.Utiliser == true)
                            {
                                string type = "";
                                if (data.Type == "O")
                                    type = "Ouvrant";
                                else if (data.Type == "D")
                                    type = "Dormant";

                                string cases = (Convert.ToInt32(data.Ligne) * Convert.ToInt32(data.Colonne)).ToString();

                                ListBox_Type.Items.Add(type + " - " + cases + " cases");
                                data.Full_Name = type + " - " + cases + " cases";
                            }
                        }

                        // Gérer l'erreur d'une collection comportant le Carriage 2 et 5.
                        if (DataChariot.Any(p => p.Carriage == 2) == true && DataChariot.Any(p => p.Carriage == 5) == true)
                        {
                            System.Windows.MessageBox.Show("Conflit entre les chariots.\nContactez le service informatique.");
                            ListBox_Type.Items.Clear();
                            ListBox_Chariot.Items.Clear();
                            ListBox_Lot.SelectedIndex = -1;
                        }
                    }
                    else if (APPLICATION_InterHM.Visibility == Visibility.Visible)
                    // InterHM.
                    {
                        string file_et = path_InterHM + lot_name + ".$ET";
                        string file_tx = path_InterHM + lot_name + ".$TX";

                        // Lire les fichiers $ET et $TX.
                        IEnumerable<string> lines_et = File.ReadLines(file_et);
                        IEnumerable<string> lines_tx = File.ReadLines(file_tx);

                        string EDN = "start";
                        var list_find = new List<string>();
                        var list_data = new List<string>();
                        // Récupérer les données voulues.
                        foreach (string line in lines_et)
                        {
                            if (line.Substring(0, 7) == EDN)
                            {
                                list_find.Add(line + " ");
                            }
                            else if (EDN == "start")
                            {
                                EDN = line.Substring(0, 7);
                                DataChariot.Add(new DataChariot() { EDN = EDN });
                            }
                            else
                            {
                                SearchData(list_find, list_data);

                                foreach (var data in DataChariot)
                                {
                                    if (data.EDN == EDN)
                                    {
                                        if (lot_name.Substring(0, 1).All(char.IsDigit) == false)
                                        {
                                            string ktn = "KTN" + list_data[5].Substring(4);
                                            int carriage = 0;
                                            foreach (string link in lines_tx)
                                            {
                                                if (link.Contains(ktn) == true)
                                                {
                                                    carriage = Convert.ToInt32(link.Substring(14, 1));
                                                    data.Carriage = carriage;
                                                    break;
                                                }
                                            }

                                            // Connaître les différents types utilisés et le nombre de chariots.
                                            foreach (var chariot in Chariot_Propriete)
                                            {
                                                if (carriage == Convert.ToInt32(chariot.Name.Substring(1)))
                                                {
                                                    chariot.Utiliser = true;
                                                    if (Convert.ToInt32(list_data[15].Substring(0, 1)) > chariot.Max_NB_Chariot)
                                                    {
                                                        chariot.Max_NB_Chariot = Convert.ToInt32(list_data[15].Substring(0, 1));
                                                    }
                                                }
                                            }

                                            data.Chariot = Convert.ToInt32(list_data[15].Substring(0, 1));
                                            data.Emplacement = Convert.ToInt32(list_data[15].Substring(2));
                                            data.Type = list_data[9].Substring(0, 1);
                                            data.Profil = list_data[6];
                                            data.Position = list_data[7];
                                            data.Casier = list_data[9];
                                            data.Longueur = list_data[1];
                                            data.Ktn = list_data[5];
                                        }
                                        else
                                        {
                                            string ktn = "KTN" + list_data[6].Substring(4);
                                            int carriage = 0;
                                            foreach (string link in lines_tx)
                                            {
                                                if (link.Contains(ktn) == true)
                                                {
                                                    carriage = Convert.ToInt32(link.Substring(14, 1));
                                                    data.Carriage = carriage;
                                                    break;
                                                }
                                            }


                                            // Connaître les différents types utilisés et le nombre de chariots.
                                            foreach (var chariot in Chariot_Propriete)
                                            {
                                                if (carriage == Convert.ToInt32(chariot.Name.Substring(1)))
                                                {
                                                    chariot.Utiliser = true;
                                                    if (Convert.ToInt32(list_data[10].Substring(0, 1)) > chariot.Max_NB_Chariot)
                                                    {
                                                        chariot.Max_NB_Chariot = Convert.ToInt32(list_data[10].Substring(0, 1));
                                                    }
                                                }
                                            }

                                            data.Chariot = Convert.ToInt32(list_data[10].Substring(0, 1));
                                            data.Emplacement = Convert.ToInt32(list_data[10].Substring(2));
                                            data.Type = list_data[7].Substring(0, 1);
                                            data.Profil = list_data[1];
                                            data.Position = list_data[3];
                                            data.Casier = list_data[7];
                                            data.Longueur = list_data[0];
                                            data.Ktn = list_data[6];
                                        }
                                    }
                                }
                                list_find.Clear();
                                list_data.Clear();
                                EDN = line.Substring(0, 7);
                                DataChariot.Add(new DataChariot() { EDN = EDN });
                            }
                        }
                        SearchData(list_find, list_data);
                        foreach (var data in DataChariot)
                        {
                            if (data.EDN == EDN)
                            {
                                if (lot_name.Substring(0, 1).All(char.IsDigit) == false)
                                {
                                    string ktn = "KTN" + list_data[5].Substring(4);
                                    foreach (string link in lines_tx)
                                    {
                                        if (link.Contains(ktn) == true)
                                        {
                                            data.Carriage = Convert.ToInt32(link.Substring(14, 1));
                                        }
                                    }

                                    data.Chariot = Convert.ToInt32(list_data[15].Substring(0, 1));
                                    data.Emplacement = Convert.ToInt32(list_data[15].Substring(2));
                                    data.Type = list_data[9].Substring(0, 1);
                                    data.Profil = list_data[6];
                                    data.Position = list_data[7];
                                    data.Casier = list_data[9];
                                    data.Longueur = list_data[1];
                                    data.Ktn = list_data[5];
                                }
                                else
                                {
                                    string ktn = "KTN" + list_data[6].Substring(4);
                                    foreach (string link in lines_tx)
                                    {
                                        if (link.Contains(ktn) == true)
                                        {
                                            data.Carriage = Convert.ToInt32(link.Substring(14, 1));
                                        }
                                    }

                                    data.Chariot = Convert.ToInt32(list_data[10].Substring(0, 1));
                                    data.Emplacement = Convert.ToInt32(list_data[10].Substring(2));
                                    data.Type = list_data[7].Substring(0, 1);
                                    data.Profil = list_data[1];
                                    data.Position = list_data[3];
                                    data.Casier = list_data[7];
                                    data.Longueur = list_data[0];
                                    data.Ktn = list_data[6];
                                }
                            }
                        }
                        list_find.Clear();
                        list_data.Clear();

                        Globals.DataChariot = DataChariot;
                        Globals.Chariot = Chariot_Propriete;

                        // Afficher les différents types.
                        foreach (var data in Chariot_Propriete)
                        {
                            if (data.Utiliser == true)
                            {
                                string type = "";
                                if (data.Type == "O")
                                    type = "Ouvrant";
                                else if (data.Type == "D")
                                    type = "Dormant";

                                string cases = (Convert.ToInt32(data.Ligne) * Convert.ToInt32(data.Colonne)).ToString();

                                ListBox_Type.Items.Add(type + " - " + cases + " cases");
                                data.Full_Name = type + " - " + cases + " cases";
                            }
                        }
                    }
                }
            }
            catch { System.Windows.MessageBox.Show("Erreur lors de la sélection du lot"); }
        }

        private void Select_Type(object sender, RoutedEventArgs e)
        // Action lors de la sélection du type.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                ListBox_Chariot.SelectedIndex = -1;
                ListBox_Chariot.Items.Clear();
                if (ListBox_Type.SelectedValue != null)
                {
                    string type_name = ListBox_Type.SelectedValue.ToString();
                    List<Chariot> Chariot_Propriete = Globals.Chariot;

                    int nb_chariot = 0;

                    foreach (var data in Chariot_Propriete)
                    {
                        if (type_name == data.Full_Name)
                            nb_chariot = data.Max_NB_Chariot;
                    }

                    // Afficher le nombre de chariot.
                    int num_chariot = 1;

                    for (int i = 0; i <= nb_chariot; i++)
                    {
                        if (nb_chariot >= num_chariot)
                        {
                            ListBox_Chariot.Items.Add("Chariot n°" + num_chariot);
                            ++num_chariot;
                        }
                    }
                }
            }
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        // Barre de recherche pour les lots.
        {
            ListBox_Type.Items.Clear();
            ListBox_Chariot.Items.Clear();
            string word = SearchBox.Text.ToUpper();
            var list_lot_search = new List<string>();

            // Récupérer la liste des lots actuels.
            foreach (object liItem in ListBox_Lot.Items)
            {
                string result = liItem.ToString();
                list_lot_search.Add(result);
            }

            ListBox_Lot.Items.Clear();

            // Afficher les résultats correspondant au lot cherché.
            foreach (string lot in list_lot_search)
            {
                if (lot.Contains(word))
                {
                    ListBox_Lot.Items.Add(lot);
                }
            }
        }

        private void Notice(object sender, RoutedEventArgs e)
        // Renvoyer sur la notice.
        {
            String openPDFFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\notice_application_chariot.pdf";
            System.IO.File.WriteAllBytes(openPDFFile, global::TELIO_CHARIOT.Properties.Resources.notice_application_chariot);            
            System.Diagnostics.Process.Start(openPDFFile);
        }

        private void Refresh_button(object sender, RoutedEventArgs e)
        // Refresh l'application.
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void ShowSetting(object sender, RoutedEventArgs e)
        // Afficher les paramètres.
        {
            if (Setting.Visibility == Visibility.Hidden)
                Setting.Visibility = Visibility.Visible;
            else if (Setting.Visibility == Visibility.Visible)
                Setting.Visibility = Visibility.Hidden;
        }

        private void Search_Path_InterHM(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers InterHM.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_InterHM_path = folderDlg.SelectedPath + @"\";
            if (new_InterHM_path != @"\")
                Text_Path_InterHM.Text = new_InterHM_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers XML.
            if (new_InterHM_path != @"\")
            {
                Properties.Settings.Default.path_InterHM = new_InterHM_path;
                Properties.Settings.Default.Save();
            }
        }

        private void Search_Path_Ramasoft(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers Ramasoft.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_Ramasoft_path = folderDlg.SelectedPath + @"\";
            if (new_Ramasoft_path != @"\")
                Text_Path_Ramasoft.Text = new_Ramasoft_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers XML.
            if (new_Ramasoft_path != @"\")
            {
                Properties.Settings.Default.path_Ramasoft = new_Ramasoft_path;
                Properties.Settings.Default.Save();
            }
        }

        private void New_Printer(object sender, SelectionChangedEventArgs e)
        // Imprimante sélectionnée.
        {
            Properties.Settings.Default.printer = ComboBox_Printer.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        //-----------------------[IMPRIMER]-----------------------\\

        public void Print()
        {
            // Imprimer le document Excel.                            
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            // Charger le fichier à imprimer.
            ExcelFile workbook_print = ExcelFile.Load(Globals.path_excel);

            // Paramétrer les options.
            System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog() { AllowSomePages = true };
            PrinterSettings printerSettings = printDialog.PrinterSettings;
            PrintOptions printOptions = new PrintOptions() { SelectionType = SelectionType.EntireFile };

            // Définissez les propriétés PrintOptions en fonction des propriétés PrinterSettings.
            printOptions.CopyCount = printerSettings.Copies;
            printOptions.FromPage = 0;
            printOptions.ToPage = 0;
            string PrinterName = ComboBox_Printer.Text;

            workbook_print.Print(PrinterName, printOptions);

            // Supprimer le fichier créé.
            File.Delete(Globals.path_excel);
        }
        private void Print_Lot(object sender, EventArgs e)
        // Imprimer le lot sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                // Récupérer la collection du lot sélectionné.
                List<DataChariot> DataChariot = Globals.DataChariot;
                string lot_name = Globals.lot_name;
                List<Chariot> Chariot_Propriete = Globals.Chariot;

                // Créer une instance d'ExcelEngine.
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    // Instancier l'objet d'application Excel.
                    IApplication application = excelEngine.Excel;

                    // Définir la version par défaut de l'application.
                    application.DefaultVersion = ExcelVersion.Excel2016;

                    foreach (var data in Chariot_Propriete)
                    {
                        if (data.Utiliser == true)
                        {
                            if (Convert.ToInt32(data.Colonne) * Convert.ToInt32(data.Ligne) == 80)
                            {
                                // Charger le classeur Excel existant dans IWorkbook.
                                IWorkbook workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup80.xlsx"));
                                int alignement = 4;
                                int nb_colonne = 8;
                                int ligne = 0;
                                int colonne = 0;

                                // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                                IWorksheet worksheet = workbook.Worksheets[0];

                                // Définir le nombre de chariot à imprimer.
                                int nb_chariot = data.Max_NB_Chariot;

                                // Imprimer chaque chariot.
                                for (int i = 1; i <= nb_chariot; i++)
                                {
                                    worksheet.Range["A5:P14"].Text = "";
                                    // Alimenter les cellules.
                                    worksheet.Range["I2"].Text = lot_name;
                                    worksheet.Range["I3"].Text = i.ToString();
                                    worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                                    if (data.Type == "O")
                                        worksheet.Range["O3"].Text = "Ouvrant";
                                    else if (data.Type == "D")
                                        worksheet.Range["O3"].Text = "Dormant";

                                    foreach (var DATA in DataChariot)
                                    {
                                        if (DATA.Type == data.Type)
                                            if (DATA.Chariot == i)
                                            {
                                                double double_row = Math.Ceiling(DATA.Emplacement / Convert.ToDouble(nb_colonne));
                                                ligne = Convert.ToInt32(double_row);

                                                double double_nb_column = DATA.Emplacement - (Math.Floor(DATA.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                                if (double_nb_column == 0)
                                                    double_nb_column = nb_colonne;
                                                colonne = Convert.ToInt32(double_nb_column);

                                                worksheet[ligne + 4, colonne + alignement].Text = DATA.Emplacement + " - " + DATA.Profil + "\n" + DATA.Position + ", " + DATA.Casier + "\n" + DATA.Longueur + "\n" + DATA.Ktn;
                                            }
                                    }

                                    // Enregistrer le document Excel.
                                    workbook.SaveAs(Globals.path_excel);
                                    Print();
                                }
                            }
                            else if (Convert.ToInt32(data.Colonne) * Convert.ToInt32(data.Ligne) == 160)
                            {
                                // Charger le classeur Excel existant dans IWorkbook.
                                IWorkbook workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup160.xlsx"));
                                int alignement = 0;
                                int nb_colonne = 16;
                                int ligne = 0;
                                int colonne = 0;

                                // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                                IWorksheet worksheet = workbook.Worksheets[0];

                                // Définir le nombre de chariot à imprimer.
                                int nb_chariot = data.Max_NB_Chariot;

                                // Imprimer chaque chariot.
                                for (int i = 1; i <= nb_chariot; i++)
                                {
                                    worksheet.Range["A5:P14"].Text = "";
                                    // Alimenter les cellules.
                                    worksheet.Range["I2"].Text = lot_name;
                                    worksheet.Range["I3"].Text = i.ToString();
                                    worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                                    if (data.Type == "O")
                                        worksheet.Range["O3"].Text = "Ouvrant";
                                    else if (data.Type == "D")
                                        worksheet.Range["O3"].Text = "Dormant";

                                    foreach (var DATA in DataChariot)
                                    {
                                        if (DATA.Type == data.Type)
                                            if (DATA.Chariot == i)
                                            {
                                                double double_row = Math.Ceiling(DATA.Emplacement / Convert.ToDouble(nb_colonne));
                                                ligne = Convert.ToInt32(double_row);

                                                double double_nb_column = DATA.Emplacement - (Math.Floor(DATA.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                                if (double_nb_column == 0)
                                                    double_nb_column = nb_colonne;
                                                colonne = Convert.ToInt32(double_nb_column);

                                                worksheet[ligne + 4, colonne + alignement].Text = DATA.Emplacement + " - " + DATA.Profil + "\n" + DATA.Position + ", " + DATA.Casier + "\n" + DATA.Longueur + "\n" + DATA.Ktn;
                                            }
                                    }

                                    // Enregistrer le document Excel.
                                    workbook.SaveAs(Globals.path_excel);
                                    Print();
                                }
                            }
                        }
                    }
                }
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }

        private void Print_Type(object sender, RoutedEventArgs e)
        // Imprimer le type du lot sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                if (ListBox_Type.SelectedValue != null)
                {
                    // Récupérer la collection du lot sélectionné.
                    List<DataChariot> DataChariot = Globals.DataChariot;
                    string lot_name = Globals.lot_name;
                    string type_name = ListBox_Type.SelectedItem.ToString();
                    string type = "";
                    if (type_name.Contains("Ouvrant") == true)
                        type = "O";
                    else if (type_name.Contains("Dormant") == true)
                        type = "D";
                    List<Chariot> Chariot_Propriete = Globals.Chariot;

                    // Créer une instance d'ExcelEngine.
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        // Instancier l'objet d'application Excel.
                        IApplication application = excelEngine.Excel;

                        // Définir la version par défaut de l'application.
                        application.DefaultVersion = ExcelVersion.Excel2016;

                        // Charger le classeur Excel existant dans IWorkbook.
                        IWorkbook workbook;
                        int alignement = 0;
                        int nb_colonne = 0;
                        int ligne = 0;
                        int colonne = 0;
                        if (ListBox_Type.SelectedValue.ToString().Contains("80 cases") == true)
                        {
                            workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup80.xlsx"));
                            alignement = 4;
                            nb_colonne = 8;
                        }
                        else
                        {
                            workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup160.xlsx"));
                            nb_colonne = 16;
                        }

                        // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                        IWorksheet worksheet = workbook.Worksheets[0];

                        // Définir le nombre de chariot à imprimer.
                        int nb_chariot = 0;

                        foreach (var data in Chariot_Propriete)
                        {
                            if (data.Full_Name == type_name)
                                nb_chariot = data.Max_NB_Chariot;
                        }

                        // Imprimer chaque chariot.
                        for (int i = 1; i <= nb_chariot; i++)
                        {
                            worksheet.Range["A5:P14"].Text = "";
                            // Alimenter les cellules.
                            worksheet.Range["I2"].Text = lot_name;
                            worksheet.Range["I3"].Text = i.ToString();
                            worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                            if (type == "O")
                                worksheet.Range["O3"].Text = "Ouvrant";
                            else if (type == "D")
                                worksheet.Range["O3"].Text = "Dormant";

                            foreach (var data in DataChariot)
                            {
                                if (data.Type == type)
                                    if (data.Chariot == i)
                                    {
                                        double double_row = Math.Ceiling(data.Emplacement / Convert.ToDouble(nb_colonne));
                                        ligne = Convert.ToInt32(double_row);

                                        double double_nb_column = data.Emplacement - (Math.Floor(data.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                        if (double_nb_column == 0)
                                            double_nb_column = nb_colonne;
                                        colonne = Convert.ToInt32(double_nb_column);

                                        worksheet[ligne + 4, colonne + alignement].Text = data.Emplacement + " - " + data.Profil + "\n" + data.Position + ", " + data.Casier + "\n" + data.Longueur + "\n" + data.Ktn;
                                    }
                            }

                            // Enregistrer le document Excel.
                            workbook.SaveAs(Globals.path_excel);
                            Print();
                        }
                    }
                }
                else
                    System.Windows.MessageBox.Show("Aucun type sélectionné");
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }

        private void Print_Chariot(object sender, RoutedEventArgs e)
        // Imprimer le numéro de chariot sélectionné du type sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                if (ListBox_Type.SelectedValue != null)
                {
                    if (ListBox_Chariot.SelectedValue != null)
                    {
                        // Récupérer la collection du lot sélectionné.
                        List<DataChariot> DataChariot = Globals.DataChariot;
                        string lot_name = Globals.lot_name;
                        string type_name = ListBox_Type.SelectedItem.ToString();
                        string type = "";
                        if (type_name.Contains("Ouvrant") == true)
                            type = "O";
                        else if (type_name.Contains("Dormant") == true)
                            type = "D";

                        // Créer une instance d'ExcelEngine.
                        using (ExcelEngine excelEngine = new ExcelEngine())
                        {
                            // Instancier l'objet d'application Excel.
                            IApplication application = excelEngine.Excel;

                            // Définir la version par défaut de l'application.
                            application.DefaultVersion = ExcelVersion.Excel2016;

                            // Charger le classeur Excel existant dans IWorkbook.
                            IWorkbook workbook;
                            int alignement = 0;
                            int nb_colonne = 0;
                            int ligne = 0;
                            int colonne = 0;
                            if (ListBox_Type.SelectedValue.ToString().Contains("80 cases") == true)
                            {
                                workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup80.xlsx"));
                                alignement = 4;
                                nb_colonne = 8;
                            }
                            else
                            {
                                workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_CHARIOT.Setup160.xlsx"));
                                nb_colonne = 16;
                            }

                            // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                            IWorksheet worksheet = workbook.Worksheets[0];

                            // Définir le nombre de chariot sélectionné et les imprimer.
                            foreach (string item in ListBox_Chariot.SelectedItems)
                            {
                                int nb_chariot = Convert.ToInt32(item.Substring(10));

                                worksheet.Range["A5:P14"].Text = "";
                                // Alimenter les cellules.
                                worksheet.Range["I2"].Text = lot_name;
                                worksheet.Range["I3"].Text = nb_chariot.ToString();
                                worksheet.Range["O2"].Text = DateTime.Now.ToString("dd-MM-yyyy");
                                if (type == "O")
                                    worksheet.Range["O3"].Text = "Ouvrant";
                                else if (type == "D")
                                    worksheet.Range["O3"].Text = "Dormant";

                                foreach (var data in DataChariot)
                                {
                                    if (data.Type == type)
                                        if (data.Chariot == nb_chariot)
                                        {
                                            double double_row = Math.Ceiling(data.Emplacement / Convert.ToDouble(nb_colonne));
                                            ligne = Convert.ToInt32(double_row);

                                            double double_nb_column = data.Emplacement - (Math.Floor(data.Emplacement / Convert.ToDouble(nb_colonne)) * nb_colonne);
                                            if (double_nb_column == 0)
                                                double_nb_column = nb_colonne;
                                            colonne = Convert.ToInt32(double_nb_column);

                                            worksheet[ligne + 4, colonne + alignement].Text = data.Emplacement + " - " + data.Profil + "\n" + data.Position + ", " + data.Casier + "\n" + data.Longueur + "\n" + data.Ktn;
                                        }
                                }

                                // Enregistrer le document Excel.
                                workbook.SaveAs(Globals.path_excel);
                                Print();
                            }
                        }
                    }
                    else
                        System.Windows.MessageBox.Show("Aucun chariot sélectionné");
                }
                else
                    System.Windows.MessageBox.Show("Aucun type sélectionné");
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }
    }
}



/*-------------CODE WPF DE L'APPLICATION-------------*/


<Window x:Class="TELIO_CHARIOT.MainWindow"
        WindowStartupLocation="CenterScreen"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        mc:Ignorable="d"
        Title="Télio CHARIOT" Height="900" Width="700" Background="#272537" WindowState="Normal" ResizeMode="CanResize" MinHeight="900" MinWidth="700">
    <Grid VerticalAlignment="Center" HorizontalAlignment="Center" Width="675" Height="869" Margin="0,0,-13,0">
        <Canvas>
            <TextBox Name="SearchBox" Text="Rechercher" ToolTip="Rechercher un lot" GotFocus="PlaceHolder" LostFocus="AddText" HorizontalAlignment="Left" Height="33" Margin="71,24,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="210" BorderThickness="3" BorderBrush="#FF5D5881" FontSize="18" TextAlignment="Center" KeyUp="SearchBox_KeyUp" Foreground="#99000000"/>
            <Button Click="Refresh_button" ToolTip="Annuler la recherche" Cursor="Hand" Canvas.Left="286" Width="44" Height="47" Canvas.Top="25">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/close.png" Margin="0,0,16,15"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
        </Canvas>
        <Canvas>
            <Button Click="Notice" ToolTip="Notice" Cursor="Hand" Canvas.Left="507" Width="50" Canvas.Top="10">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/interrogation.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
            <Button Click="Refresh_button" ToolTip="Redémarrer l'application" Cursor="Hand" Canvas.Left="557" Width="55" Height="58" Canvas.Top="6">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/refresh.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
            <Button Click="ShowSetting" ToolTip="Paramètres" Cursor="Hand" Canvas.Left="613" Canvas.Top="10">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/setting.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
        </Canvas>
        <Canvas>
            <Rectangle Height="732" Width="307" Fill="#CCFFFFFF" Canvas.Left="27" Canvas.Top="90"/>
            <Rectangle Fill="#FF5D5881" Height="50" Margin="10,69,0,0" Width="209" RenderTransformOrigin="0.404,0.46"/>
            <Image Source="/images/lot.png" Height="57" Width="39" Canvas.Left="27" Canvas.Top="66"/>
            <Label Content="Lot" Margin="71,62,0,0" FontSize="36" Foreground="White" RenderTransformOrigin="1.417,0.586"/>
            <ListBox Name="ListBox_Lot" ToolTip="Sélectionnez votre lot" ItemContainerStyle="{DynamicResource _ListBoxItemStyle}" SelectionMode="Single" HorizontalAlignment="Left" VerticalAlignment="Top" Width="307" Background="#FFD4D3D7" BorderBrush="#FFD4D3D7" FontSize="20" Cursor="Hand" MaxHeight="670" Canvas.Left="27" Canvas.Top="151">
                <ListBox.Resources>
                    <Style TargetType="ListBox">
                        <EventSetter Event="MouseUp" Handler="Select_Lot"/>
                    </Style>
                </ListBox.Resources>
            </ListBox>
            <Button Name="Display_InterHM" Click="Button_InterHM" ToolTip="Traiter les fichiers InterHM" Content="InterHM" Width="154" Height="32" Canvas.Left="26" Canvas.Top="119" BorderBrush="#FF272537" Foreground="#FF272537"/>
            <Button Name="Display_Ramasoft" Click="Button_Ramasoft" ToolTip="Traiter les fichiers Ramasoft" Content="Ramasoft" Width="155" Height="32" Canvas.Left="180" Canvas.Top="119" BorderBrush="#FF272537" Foreground="#FF272537" Background="#FFDDDDDD"/>
        </Canvas>
        <Canvas>
            <Rectangle Height="232" Width="240" Fill="#FFD4D3D7" Canvas.Left="394" Canvas.Top="94" ScrollViewer.HorizontalScrollBarVisibility="Visible"/>
            <Rectangle Fill="#FF5D5881" Height="50" Width="210" Canvas.Left="377" Canvas.Top="71"/>
            <Label Content="Type" FontSize="36" Foreground="White" Canvas.Left="426" Canvas.Top="63"/>
            <Image Source="/images/data.png" Height="55" Width="38" Canvas.Left="388" Canvas.Top="67"/>
            <ListBox Name="ListBox_Type" ToolTip="Sélectionnez votre type" ItemContainerStyle="{DynamicResource _ListBoxItemStyle}" SelectionMode="Single" HorizontalAlignment="Left" VerticalAlignment="Top" Width="240" Background="#FFD4D3D7" BorderBrush="#FFD4D3D7" FontSize="20" Cursor="Hand" MaxHeight="204" Canvas.Left="394" Canvas.Top="122">
                <ListBox.Resources>
                    <Style TargetType="ListBox">
                        <EventSetter Event="MouseUp" Handler="Select_Type"/>
                    </Style>
                </ListBox.Resources>
            </ListBox>
        </Canvas>
        <Canvas>
            <Rectangle Height="177" Width="240" Fill="#FFD4D3D7" Canvas.Left="394" Canvas.Top="378" ScrollViewer.HorizontalScrollBarVisibility="Visible"/>
            <Rectangle Fill="#FF5D5881" Height="50" Width="210" Canvas.Left="377" Canvas.Top="355"/>
            <Label Content="Chariot" FontSize="36" Foreground="White" Canvas.Left="430" Canvas.Top="347"/>
            <Image Source="/images/storage.png" Height="55" Width="38" Canvas.Left="388" Canvas.Top="351"/>
            <ListBox Name="ListBox_Chariot" ToolTip="Sélectionnez votre chariot" ItemContainerStyle="{DynamicResource _ListBoxItemStyle}" SelectionMode="Multiple" HorizontalAlignment="Left" VerticalAlignment="Top" Width="240" Background="#FFD4D3D7" BorderBrush="#FFD4D3D7" FontSize="20" Cursor="Hand" MaxHeight="154" Canvas.Left="394" Canvas.Top="405"/>
        </Canvas>

        <Canvas Name="APPLICATION_InterHM" Visibility="Hidden"/>
        <Canvas Name="APPLICATION_Ramasoft" Visibility="Hidden"/>

        <Button Name="Imprimer_Lot" HorizontalContentAlignment="Center" HorizontalAlignment="Left" Margin="395,574,0,0" VerticalAlignment="Top" Width="236" Height="70" Click="Print_Lot" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand">
            <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                <Image VerticalAlignment="Center" Source="/images/print.png" Stretch="None" Width="85" />
                <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="121">Imprimer Lot</TextBlock>
            </StackPanel>
        </Button>

        <Button Name="Imprimer_Type" HorizontalContentAlignment="Center" HorizontalAlignment="Left" Margin="395,665,0,0" VerticalAlignment="Top" Width="236" Height="70" Click="Print_Type" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand">
            <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                <Image VerticalAlignment="Center" Source="images/print.png" Stretch="None" Width="85" />
                <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="135">Imprimer Type</TextBlock>
            </StackPanel>
        </Button>

        <Button Name="Imprimer_Chariot" HorizontalContentAlignment="Center" HorizontalAlignment="Left" Margin="395,754,0,0" VerticalAlignment="Top" Width="236" Height="70" Click="Print_Chariot" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand">
            <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                <Image VerticalAlignment="Center" Source="images/print.png" Stretch="None" Width="85" />
                <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="135">Imprimer Chariot</TextBlock>
            </StackPanel>
        </Button>

        <Canvas Name="Setting" Visibility="Hidden">
            <Border Width="448" Height="258" Background="#272537" CornerRadius="10" Canvas.Left="204" Canvas.Top="72"/>
            <Canvas>
                <TextBlock Canvas.Left="220" Canvas.Top="82" FontSize="16" Foreground="White"><Run Text="Saisissez le chemin d'accès vers les fichiers InterHM :"/></TextBlock>
                <TextBox Name="Text_Path_InterHM" PreviewMouseDown="Search_Path_InterHM" IsReadOnly="True" Canvas.Left="220" Canvas.Top="108" Width="336" Height="33" HorizontalAlignment="Center" Padding="5" VerticalContentAlignment="Center" FontSize="16" FontStyle="Italic"/>
                <Button Click="Search_Path_InterHM" Canvas.Left="567" Canvas.Top="108" Height="33" Width="34" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/data.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <Canvas>
                <TextBlock Canvas.Left="220" Canvas.Top="163" FontSize="16" Foreground="White"><Run Text="Saisissez le chemin d'accès vers les fichiers Ramasoft :"/></TextBlock>
                <TextBox Name="Text_Path_Ramasoft" PreviewMouseDown="Search_Path_Ramasoft" IsReadOnly="True" Canvas.Left="220" Canvas.Top="189" Width="336" Height="33" HorizontalAlignment="Center" Padding="5" VerticalContentAlignment="Center" FontSize="16" FontStyle="Italic"/>
                <Button Click="Search_Path_Ramasoft" Canvas.Left="567" Canvas.Top="189" Height="33" Width="34" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/data.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <TextBlock Canvas.Left="220" Canvas.Top="309" Foreground="#FF999494" FontSize="10"><Run Text="Veuillez redémarrer l'application après modification"/></TextBlock>
            <Button Click="Refresh_button" Canvas.Left="453" Canvas.Top="304" Height="26" Width="14" Cursor="Hand">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/refresh.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
            <Canvas>
                <TextBlock Canvas.Left="220" Canvas.Top="245" Foreground="White" FontSize="16">Sélection de l'imprimante :</TextBlock>
                <ComboBox Name="ComboBox_Printer" SelectionChanged="New_Printer" Canvas.Left="220" Canvas.Top="271" Width="267" BorderBrush="White"/>
                <Image Source="/images/print.png" Canvas.Left="503" Canvas.Top="267" Height="29" Width="33"/>
            </Canvas>
            <Canvas.Effect>
                <DropShadowEffect BlurRadius="25" Direction="-90" RenderingBias="Quality" ShadowDepth="2"/>
            </Canvas.Effect>
        </Canvas>
    </Grid>
    <Window.Resources>
        <Style x:Key="_ListBoxItemStyle" TargetType="ListBoxItem">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="ListBoxItem">
                        <Border Name="_Border"
                                Padding="2"
                                SnapsToDevicePixels="true">
                            <ContentPresenter />
                        </Border>
                        <ControlTemplate.Triggers>
                            <Trigger Property="IsSelected" Value="true">
                                <Setter TargetName="_Border" Property="Background" Value="#272537"/>
                                <Setter Property="Foreground" Value="White"/>
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>
    </Window.Resources>
</Window>`
application_chariot.innerText += str_chariot;


let application_rotox = document.getElementById("application_rotox");
let str_rotox = `using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using Syncfusion.XlsIO;
using System.Reflection;

namespace TELIO_ROT
{
    public partial class MainWindow : Window
    {
        //-----------------------[STYLE]-----------------------\\

        public void PlaceHolder(object sender, EventArgs e)
        // Supprimer le placeholder à SearchBox.
        {
            if (SearchBox.Text == "Rechercher")
            {
                SearchBox.Text = "";
            }
            SearchBox.Foreground = System.Windows.Media.Brushes.Black;
        }

        public void AddText(object sender, EventArgs e)
        // Ajouter le placeholder à SearchBox.
        {
            if (string.IsNullOrWhiteSpace(SearchBox.Text))
            {
                SearchBox.Text = "Rechercher";
            }
            SearchBox.Foreground = Brushes.Gray;
        }

        //-----------------------[INTERACTION]-----------------------\\

        public class DataChariot
        {
            public string Lot { get; set; }
            public int Nb_Copie { get; set; }
            public string Reference { get; set; }
            public string Longueur { get; set; }
            public string Quantite { get; set; }
            public string Chariot { get; set; }
            public string Case { get; set; }
        }

        public static class Globals
        // Variables globales.
        {
            public static String lot_name = "";
            public static String path_excel = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\Print.xlsx";
            public static List<DataChariot> DataChariot = new List<DataChariot>();
            public static String ROT_PATH = Properties.Settings.Default.path_ROT;
        }

        public MainWindow()
        {
            InitializeComponent();
            // Récupérer les chemins d'accès aux fichiers.
            Text_Path_ROT.Text = Globals.ROT_PATH;

            string path_ROT = Globals.ROT_PATH;

            try
            {
                // Récupérer tous les fichiers ROT présents dans le dossier.
                string[] files = Directory.GetFiles(path_ROT, "*.ROT");
                foreach (string file in files)
                {
                    string fileWExt = System.IO.Path.GetFileNameWithoutExtension(file);
                    ListBox_Lot.Items.Add(fileWExt);
                }
            }
            catch
            {
                System.Windows.MessageBox.Show("Le chemin d'accès vers les fichiers est introuvable");
            }

            // Récupérer les imprimantes connectées.
            ComboBox_Printer.Items.Add(Properties.Settings.Default.printer);
            ComboBox_Printer.SelectedItem = Properties.Settings.Default.printer;

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (printer != Properties.Settings.Default.printer)
                    ComboBox_Printer.Items.Add(printer);
            }

        }

        private void Select_Lot(object sender, RoutedEventArgs e)
        // Action lors de la sélection du lot.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                string lot_name = ListBox_Lot.SelectedValue.ToString();
                Globals.lot_name = lot_name;
                string path_ROT = Globals.ROT_PATH;
                string file_ROT = path_ROT + lot_name + ".ROT";
                List<DataChariot> DataChariot = Globals.DataChariot;
                DataChariot.Clear();

                // Lire les fichiers ROT.
                IEnumerable<string> lines_ROT = File.ReadLines(file_ROT);
                int nb_data = 1;
                int nb_copie = 1;
                foreach(string line in lines_ROT)
                {
                    string[] subs = line.Split('%');

                    // Récuperer les données voulues.
                    var list_data = new List<string>();
                    foreach (string data in subs)
                    {
                        if (data.Contains("PB"))
                        {
                            string result = data.Substring(2);
                            list_data.Add(result);
                        }
                        else if (data.Contains("SL"))
                        {
                            string result = data.Substring(2);
                            string longueur = result.Remove(result.Length - 1, 1).Trim();
                            list_data.Add(longueur);
                        }
                        else if (data.Contains("WG"))
                        {
                            string result = data.Substring(2).Trim();
                            list_data.Add(result);
                        }
                        else if (data.Contains("FN"))
                        {
                            string result = data.Substring(2).Trim(' ', 'D');
                            list_data.Add(result);
                        }
                    }
                    if (nb_data > 45)
                    {
                        ++nb_copie;
                        nb_data = 1;
                    }
                    DataChariot.Add(new DataChariot() { Lot = lot_name, Nb_Copie = nb_copie, Reference = list_data[0], Longueur = list_data[1], Quantite = "2", Chariot = list_data[2], Case = list_data[3] });
                    ++nb_data;
                }
                // Accéder à cette liste avec les fonctions Print.
                Globals.DataChariot = DataChariot;
            }
        }

        private void Refresh_button(object sender, RoutedEventArgs e)
        // Refresh l'application.
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void ShowSetting(object sender, RoutedEventArgs e)
        // Afficher les paramètres.
        {
            if (Setting.Visibility == Visibility.Hidden)
                Setting.Visibility = Visibility.Visible;
            else if (Setting.Visibility == Visibility.Visible)
                Setting.Visibility = Visibility.Hidden;
        }

        private void Search_Path_ROT(object sender, RoutedEventArgs e)
        // Chemin d'accès vers les fichiers InterHM.
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Afficher l'explorateur de fichiers.
            DialogResult result = folderDlg.ShowDialog();
            string new_InterHM_path = folderDlg.SelectedPath + @"\";
            if (new_InterHM_path != @"\")
                Text_Path_ROT.Text = new_InterHM_path;
            Environment.SpecialFolder root = folderDlg.RootFolder;

            // Renvoyer le nouveau chemin d'accès des fichiers XML.
            if (new_InterHM_path != @"\")
            {
                Properties.Settings.Default.path_ROT = new_InterHM_path;
                Properties.Settings.Default.Save();
            }
        }

        private void New_Printer(object sender, SelectionChangedEventArgs e)
        // Imprimante sélectionnée.
        {
            Properties.Settings.Default.printer = ComboBox_Printer.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        // Barre de recherche pour les lots.
        {
            string word = SearchBox.Text.ToUpper();
            var list_lot_search = new List<string>();

            // Récupérer la liste des lots actuels.
            foreach (object liItem in ListBox_Lot.Items)
            {
                string result = liItem.ToString();
                list_lot_search.Add(result);
            }

            ListBox_Lot.Items.Clear();

            // Afficher les résultats correspondant au lot cherché.
            foreach (string lot in list_lot_search)
            {
                if (lot.Contains(word))
                {
                    ListBox_Lot.Items.Add(lot);
                }
            }
        }

        //-----------------------[IMPRIMER]-----------------------\\

        private void Print_File(object sender, EventArgs e)
        // Imprimer le lot sélectionné.
        {
            if (ListBox_Lot.SelectedValue != null)
            {
                // Récupérer la collection du lot sélectionné.
                List<DataChariot> DataChariot = Globals.DataChariot;
                string lot_name = Globals.lot_name;

                // Créer une instance d'ExcelEngine.
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    // Instancier l'objet d'application Excel.
                    IApplication application = excelEngine.Excel;

                    // Définir la version par défaut de l'application.
                    application.DefaultVersion = ExcelVersion.Excel2016;

                    // Charger le classeur Excel existant dans IWorkbook.
                    IWorkbook workbook = application.Workbooks.Open(Assembly.GetExecutingAssembly().GetManifestResourceStream("TELIO_ROT.Setup.xlsx"));

                    // Obtenez la première feuille de calcul du classeur dans IWorksheet.
                    IWorksheet worksheet = workbook.Worksheets[0];

                    // Alimenter les cellules.
                    int ligne = 6;
                    int colonne = 2;
                    double copie = Math.Ceiling(DataChariot.Count / 45.0);
                    int nb_copie = Convert.ToInt32(copie);
                    for (int i = 1; i <= copie; i++)
                    {
                        ligne = 6;
                        worksheet.Range["C6:G50"].Text = "";
                        foreach (var data in DataChariot)
                        {
                            if (lot_name == data.Lot)
                                if (i == data.Nb_Copie)
                                {
                                    worksheet[3, 7].Text = i.ToString();
                                    worksheet[3, 8].Text = data.Lot;
                                    worksheet[ligne, 1 + colonne].Text = data.Reference;
                                    worksheet[ligne, 2 + colonne].Text = data.Longueur;
                                    worksheet[ligne, 3 + colonne].Text = data.Quantite;
                                    worksheet[ligne, 4 + colonne].Text = data.Chariot;
                                    worksheet[ligne, 5 + colonne].Text = data.Case;

                                    ++ligne;
                                }
                        }
                        // Enregistrer le document Excel.
                        workbook.SaveAs(Globals.path_excel);

                        // Imprimer le document Excel.                            
                        SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

                        // Charger le fichier à imprimer.
                        ExcelFile workbook_print = ExcelFile.Load(Globals.path_excel);

                        // Paramétrer les options.
                        System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog() { AllowSomePages = true };
                        PrinterSettings printerSettings = printDialog.PrinterSettings;
                        PrintOptions printOptions = new PrintOptions() { SelectionType = SelectionType.EntireFile };

                        // Définissez les propriétés PrintOptions en fonction des propriétés PrinterSettings.
                        printOptions.CopyCount = printerSettings.Copies;
                        printOptions.FromPage = 0;
                        printOptions.ToPage = 0;
                        string PrinterName = ComboBox_Printer.Text;

                        workbook_print.Print(PrinterName, printOptions);
                        
                        // Supprimer le fichier créé.
                        File.Delete(Globals.path_excel);
                    }
                }
            }
            else
                System.Windows.MessageBox.Show("Aucun lot sélectionné");
        }
    }
}



/*-------------CODE WPF DE L'APPLICATION-------------*/


<Window x:Class="TELIO_ROT.MainWindow"
        WindowStartupLocation="CenterScreen"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        mc:Ignorable="d"
        Title="Télio ROT" Height="1000" Width="400" Background="#272537" WindowState="Normal" ResizeMode="CanResize" MinHeight="1000" MinWidth="400">
    <Grid VerticalAlignment="Center" HorizontalAlignment="Center" Width="400" Height="969" Margin="2,0,-10,0">
        <Canvas>
            <TextBox Name="SearchBox" Text="Rechercher" ToolTip="Rechercher un lot" GotFocus="PlaceHolder" LostFocus="AddText" HorizontalAlignment="Left" Height="33" TextWrapping="Wrap" VerticalAlignment="Top" Width="210" BorderThickness="3" BorderBrush="#FF5D5881" FontSize="18" TextAlignment="Center" KeyUp="SearchBox_KeyUp" Foreground="#99000000" Canvas.Left="9" Canvas.Top="24"/>
            <Button Click="Refresh_button" ToolTip="Annuler la recherche" Cursor="Hand" Canvas.Left="226" Width="44" Height="47" Canvas.Top="25">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/close.png" Margin="0,0,16,15"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
        </Canvas>
        <Canvas>
            <Rectangle Height="732" Width="337" Fill="#CCFFFFFF" Canvas.Left="27" Canvas.Top="90"/>
            <Rectangle Fill="#FF5D5881" Height="50" Margin="10,69,0,0" Width="209" RenderTransformOrigin="0.404,0.46"/>
            <Image Source="/images/lot.png" Height="57" Width="39" Canvas.Left="27" Canvas.Top="66"/>
            <Label Content="Lot" Margin="71,62,0,0" FontSize="36" Foreground="White" RenderTransformOrigin="1.417,0.586"/>
            <ListBox Name="ListBox_Lot" ToolTip="Sélectionnez votre lot" ItemContainerStyle="{DynamicResource _ListBoxItemStyle}" SelectionMode="Single" HorizontalAlignment="Left" VerticalAlignment="Top" Width="307" Background="#FFD4D3D7" BorderBrush="#FFD4D3D7" FontSize="20" Cursor="Hand" MaxHeight="702" Canvas.Left="27" Canvas.Top="120">
                <ListBox.Resources>
                    <Style TargetType="ListBox">
                        <EventSetter Event="MouseUp" Handler="Select_Lot"/>
                    </Style>
                </ListBox.Resources>
            </ListBox>
        </Canvas>
        <Button Name="Imprimer_Lot" HorizontalContentAlignment="Center" Width="236" Height="70" Click="Print_File" Foreground="White" BorderBrush="White" Background="#FF272537" Cursor="Hand" Margin="70,861,82,38">
            <StackPanel Width="243" HorizontalAlignment="Center" Orientation="Horizontal">
                <Image VerticalAlignment="Center" Source="/images/print.png" Stretch="None" Width="85" />
                <TextBlock VerticalAlignment="Center" FontSize="16" FontWeight="Bold" Width="132">Imprimer Fichier</TextBlock>
            </StackPanel>
        </Button>
        <Canvas>
            <Button Click="Refresh_button" ToolTip="Redémarrer l'application" Cursor="Hand" Canvas.Left="267" Width="55" Height="58" Canvas.Top="12">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/refresh.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
            <Button Click="ShowSetting" ToolTip="Paramètres" Cursor="Hand" Canvas.Left="321" Canvas.Top="16">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/setting.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
        </Canvas>
        <Canvas Name="Setting" Visibility="Hidden" Margin="0,0,10,0">
            <Border Width="370" Height="156" Background="#272537" CornerRadius="10" Canvas.Left="10" Canvas.Top="64"/>
            <Canvas>
                <TextBlock Canvas.Left="21" Canvas.Top="69" FontSize="16" Foreground="White"><Run Text="Saisissez le chemin d'accès vers les fichiers ROT :"/></TextBlock>
                <TextBox Name="Text_Path_ROT" PreviewMouseDown="Search_Path_ROT" IsReadOnly="True" Canvas.Left="21" Canvas.Top="95" Width="292" Height="33" HorizontalAlignment="Center" Padding="5" VerticalContentAlignment="Center" FontSize="16" FontStyle="Italic"/>
                <Button Click="Search_Path_ROT" Canvas.Left="327" Canvas.Top="95" Height="33" Width="34" Cursor="Hand">
                    <Button.Template>
                        <ControlTemplate>
                            <Image Source="/images/data.png"/>
                        </ControlTemplate>
                    </Button.Template>
                </Button>
            </Canvas>
            <TextBlock Canvas.Left="24" Canvas.Top="204" Foreground="#FF999494" FontSize="10"><Run Text="Veuillez redémarrer l'application après modification"/></TextBlock>
            <Button Click="Refresh_button" Canvas.Left="255" Canvas.Top="199" Height="26" Width="14" Cursor="Hand">
                <Button.Template>
                    <ControlTemplate>
                        <Image Source="/images/refresh.png"/>
                    </ControlTemplate>
                </Button.Template>
            </Button>
            <Canvas>
                <TextBlock Canvas.Left="22" Canvas.Top="139" Foreground="White" FontSize="16">Sélection de l'imprimante :</TextBlock>
                <ComboBox Name="ComboBox_Printer" SelectionChanged="New_Printer" Canvas.Left="22" Canvas.Top="165" Width="267" BorderBrush="White"/>
                <Image Source="/images/print.png" Canvas.Left="300" Canvas.Top="162" Height="29" Width="33"/>
            </Canvas>
            <Canvas.Effect>
                <DropShadowEffect BlurRadius="25" Direction="-90" RenderingBias="Quality" ShadowDepth="2"/>
            </Canvas.Effect>
        </Canvas>
    </Grid>
    <Window.Resources>
        <Style x:Key="_ListBoxItemStyle" TargetType="ListBoxItem">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="ListBoxItem">
                        <Border Name="_Border"
                                Padding="2"
                                SnapsToDevicePixels="true">
                            <ContentPresenter />
                        </Border>
                        <ControlTemplate.Triggers>
                            <Trigger Property="IsSelected" Value="true">
                                <Setter TargetName="_Border" Property="Background" Value="#272537"/>
                                <Setter Property="Foreground" Value="White"/>
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>
    </Window.Resources>
</Window>`
application_rotox.innerText += str_rotox;

document.getElementById("application_etiquette").style.border = "none";
document.getElementById("application_chariot").style.border = "none";
document.getElementById("application_rotox").style.border = "none";

setTimeout(function(){ !function(){
    (function(){function aa(g){function r(){try{L.doScroll("left")}catch(ba){k.setTimeout(r,50);return}x("poll")}function x(r){if("readystatechange"!=r.type||"complete"==z.readyState)("load"==r.type?k:z)[B](n+r.type,x,!1),!l&&(l=!0)&&g.call(k,r.type||r)}var X=z.addEventListener,l=!1,E=!0,v=X?"addEventListener":"attachEvent",B=X?"removeEventListener":"detachEvent",n=X?"":"on";if("complete"==z.readyState)g.call(k,"lazy");else{if(z.createEventObject&&L.doScroll){try{E=!k.frameElement}catch(ba){}E&&r()}z[v](n+
    "DOMContentLoaded",x,!1);z[v](n+"readystatechange",x,!1);k[v](n+"load",x,!1)}}function T(){U&&aa(function(){var g=M.length;ca(g?function(){for(var r=0;r<g;++r)(function(g){k.setTimeout(function(){k.exports[M[g]].apply(k,arguments)},0)})(r)}:void 0)})}for(var k=window,z=document,L=z.documentElement,N=z.head||z.getElementsByTagName("head")[0]||L,B="",F=z.getElementsByTagName("script"),l=F.length;0<=--l;){var O=F[l],Y=O.src.match(/^[^?#]*\/run_prettify\.js(\?[^#]*)?(?:#.*)?$/);if(Y){B=Y[1]||"";O.parentNode.removeChild(O);
    break}}var U=!0,H=[],P=[],M=[];B.replace(/[?&]([^&=]+)=([^&]+)/g,function(g,r,x){x=decodeURIComponent(x);r=decodeURIComponent(r);"autorun"==r?U=!/^[0fn]/i.test(x):"lang"==r?H.push(x):"skin"==r?P.push(x):"callback"==r&&M.push(x)});l=0;for(B=H.length;l<B;++l)(function(){var g=z.createElement("script");g.onload=g.onerror=g.onreadystatechange=function(){!g||g.readyState&&!/loaded|complete/.test(g.readyState)||(g.onerror=g.onload=g.onreadystatechange=null,--S,S||k.setTimeout(T,0),g.parentNode&&g.parentNode.removeChild(g),
    g=null)};g.type="text/javascript";g.src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/lang-"+encodeURIComponent(H[l])+".js";N.insertBefore(g,N.firstChild)})(H[l]);for(var S=H.length,F=[],l=0,B=P.length;l<B;++l)F.push("https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/skins/"+encodeURIComponent(P[l])+".css");F.push("https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/prettify.css");(function(g){function r(l){if(l!==x){var k=z.createElement("link");k.rel="stylesheet";
    k.type="text/css";l+1<x&&(k.error=k.onerror=function(){r(l+1)});k.href=g[l];N.appendChild(k)}}var x=g.length;r(0)})(F);var ca=function(){"undefined"!==typeof window&&(window.PR_SHOULD_USE_CONTINUATION=!0);var g;(function(){function r(a){function d(e){var a=e.charCodeAt(0);if(92!==a)return a;var c=e.charAt(1);return(a=k[c])?a:"0"<=c&&"7">=c?parseInt(e.substring(1),8):"u"===c||"x"===c?parseInt(e.substring(2),16):e.charCodeAt(1)}function f(e){if(32>e)return(16>e?"\\x0":"\\x")+e.toString(16);e=String.fromCharCode(e);
    return"\\"===e||"-"===e||"]"===e||"^"===e?"\\"+e:e}function c(e){var c=e.substring(1,e.length-1).match(RegExp("\\\\u[0-9A-Fa-f]{4}|\\\\x[0-9A-Fa-f]{2}|\\\\[0-3][0-7]{0,2}|\\\\[0-7]{1,2}|\\\\[\\s\\S]|-|[^-\\\\]","g"));e=[];var a="^"===c[0],b=["["];a&&b.push("^");for(var a=a?1:0,h=c.length;a<h;++a){var m=c[a];if(/\\[bdsw]/i.test(m))b.push(m);else{var m=d(m),p;a+2<h&&"-"===c[a+1]?(p=d(c[a+2]),a+=2):p=m;e.push([m,p]);65>p||122<m||(65>p||90<m||e.push([Math.max(65,m)|32,Math.min(p,90)|32]),97>p||122<m||
    e.push([Math.max(97,m)&-33,Math.min(p,122)&-33]))}}e.sort(function(e,a){return e[0]-a[0]||a[1]-e[1]});c=[];h=[];for(a=0;a<e.length;++a)m=e[a],m[0]<=h[1]+1?h[1]=Math.max(h[1],m[1]):c.push(h=m);for(a=0;a<c.length;++a)m=c[a],b.push(f(m[0])),m[1]>m[0]&&(m[1]+1>m[0]&&b.push("-"),b.push(f(m[1])));b.push("]");return b.join("")}function g(e){for(var a=e.source.match(RegExp("(?:\\[(?:[^\\x5C\\x5D]|\\\\[\\s\\S])*\\]|\\\\u[A-Fa-f0-9]{4}|\\\\x[A-Fa-f0-9]{2}|\\\\[0-9]+|\\\\[^ux0-9]|\\(\\?[:!=]|[\\(\\)\\^]|[^\\x5B\\x5C\\(\\)\\^]+)",
    "g")),b=a.length,d=[],h=0,m=0;h<b;++h){var p=a[h];"("===p?++m:"\\"===p.charAt(0)&&(p=+p.substring(1))&&(p<=m?d[p]=-1:a[h]=f(p))}for(h=1;h<d.length;++h)-1===d[h]&&(d[h]=++r);for(m=h=0;h<b;++h)p=a[h],"("===p?(++m,d[m]||(a[h]="(?:")):"\\"===p.charAt(0)&&(p=+p.substring(1))&&p<=m&&(a[h]="\\"+d[p]);for(h=0;h<b;++h)"^"===a[h]&&"^"!==a[h+1]&&(a[h]="");if(e.ignoreCase&&A)for(h=0;h<b;++h)p=a[h],e=p.charAt(0),2<=p.length&&"["===e?a[h]=c(p):"\\"!==e&&(a[h]=p.replace(/[a-zA-Z]/g,function(a){a=a.charCodeAt(0);
    return"["+String.fromCharCode(a&-33,a|32)+"]"}));return a.join("")}for(var r=0,A=!1,q=!1,I=0,b=a.length;I<b;++I){var t=a[I];if(t.ignoreCase)q=!0;else if(/[a-z]/i.test(t.source.replace(/\\u[0-9a-f]{4}|\\x[0-9a-f]{2}|\\[^ux]/gi,""))){A=!0;q=!1;break}}for(var k={b:8,t:9,n:10,v:11,f:12,r:13},u=[],I=0,b=a.length;I<b;++I){t=a[I];if(t.global||t.multiline)throw Error(""+t);u.push("(?:"+g(t)+")")}return new RegExp(u.join("|"),q?"gi":"g")}function l(a,d){function f(a){var b=a.nodeType;if(1==b){if(!c.test(a.className)){for(b=
    a.firstChild;b;b=b.nextSibling)f(b);b=a.nodeName.toLowerCase();if("br"===b||"li"===b)g[q]="\n",A[q<<1]=r++,A[q++<<1|1]=a}}else if(3==b||4==b)b=a.nodeValue,b.length&&(b=d?b.replace(/\r\n?/g,"\n"):b.replace(/[ \t\r\n]+/g," "),g[q]=b,A[q<<1]=r,r+=b.length,A[q++<<1|1]=a)}var c=/(?:^|\s)nocode(?:\s|$)/,g=[],r=0,A=[],q=0;f(a);return{a:g.join("").replace(/\n$/,""),c:A}}function k(a,d,f,c,g){f&&(a={h:a,l:1,j:null,m:null,a:f,c:null,i:d,g:null},c(a),g.push.apply(g,a.g))}function z(a){for(var d=void 0,f=a.firstChild;f;f=
    f.nextSibling)var c=f.nodeType,d=1===c?d?a:f:3===c?S.test(f.nodeValue)?a:d:d;return d===a?void 0:d}function E(a,d){function f(a){for(var q=a.i,r=a.h,b=[q,"pln"],t=0,A=a.a.match(g)||[],u={},e=0,l=A.length;e<l;++e){var D=A[e],w=u[D],h=void 0,m;if("string"===typeof w)m=!1;else{var p=c[D.charAt(0)];if(p)h=D.match(p[1]),w=p[0];else{for(m=0;m<n;++m)if(p=d[m],h=D.match(p[1])){w=p[0];break}h||(w="pln")}!(m=5<=w.length&&"lang-"===w.substring(0,5))||h&&"string"===typeof h[1]||(m=!1,w="src");m||(u[D]=w)}p=t;
    t+=D.length;if(m){m=h[1];var C=D.indexOf(m),G=C+m.length;h[2]&&(G=D.length-h[2].length,C=G-m.length);w=w.substring(5);k(r,q+p,D.substring(0,C),f,b);k(r,q+p+C,m,F(w,m),b);k(r,q+p+G,D.substring(G),f,b)}else b.push(q+p,w)}a.g=b}var c={},g;(function(){for(var f=a.concat(d),q=[],k={},b=0,t=f.length;b<t;++b){var n=f[b],u=n[3];if(u)for(var e=u.length;0<=--e;)c[u.charAt(e)]=n;n=n[1];u=""+n;k.hasOwnProperty(u)||(q.push(n),k[u]=null)}q.push(/[\0-\uffff]/);g=r(q)})();var n=d.length;return f}function v(a){var d=
    [],f=[];a.tripleQuotedStrings?d.push(["str",/^(?:\'\'\'(?:[^\'\\]|\\[\s\S]|\'{1,2}(?=[^\']))*(?:\'\'\'|$)|\"\"\"(?:[^\"\\]|\\[\s\S]|\"{1,2}(?=[^\"]))*(?:\"\"\"|$)|\'(?:[^\\\']|\\[\s\S])*(?:\'|$)|\"(?:[^\\\"]|\\[\s\S])*(?:\"|$))/,null,"'\""]):a.multiLineStrings?d.push(["str",/^(?:\'(?:[^\\\']|\\[\s\S])*(?:\'|$)|\"(?:[^\\\"]|\\[\s\S])*(?:\"|$)|\`(?:[^\\\`]|\\[\s\S])*(?:\`|$))/,null,"'\"`"]):d.push(["str",/^(?:\'(?:[^\\\'\r\n]|\\.)*(?:\'|$)|\"(?:[^\\\"\r\n]|\\.)*(?:\"|$))/,null,"\"'"]);a.verbatimStrings&&
    f.push(["str",/^@\"(?:[^\"]|\"\")*(?:\"|$)/,null]);var c=a.hashComments;c&&(a.cStyleComments?(1<c?d.push(["com",/^#(?:##(?:[^#]|#(?!##))*(?:###|$)|.*)/,null,"#"]):d.push(["com",/^#(?:(?:define|e(?:l|nd)if|else|error|ifn?def|include|line|pragma|undef|warning)\b|[^\r\n]*)/,null,"#"]),f.push(["str",/^<(?:(?:(?:\.\.\/)*|\/?)(?:[\w-]+(?:\/[\w-]+)+)?[\w-]+\.h(?:h|pp|\+\+)?|[a-z]\w*)>/,null])):d.push(["com",/^#[^\r\n]*/,null,"#"]));a.cStyleComments&&(f.push(["com",/^\/\/[^\r\n]*/,null]),f.push(["com",/^\/\*[\s\S]*?(?:\*\/|$)/,
    null]));if(c=a.regexLiterals){var g=(c=1<c?"":"\n\r")?".":"[\\S\\s]";f.push(["lang-regex",RegExp("^(?:^^\\.?|[+-]|[!=]=?=?|\\#|%=?|&&?=?|\\(|\\*=?|[+\\-]=|->|\\/=?|::?|<<?=?|>>?>?=?|,|;|\\?|@|\\[|~|{|\\^\\^?=?|\\|\\|?=?|break|case|continue|delete|do|else|finally|instanceof|return|throw|try|typeof)\\s*("+("/(?=[^/*"+c+"])(?:[^/\\x5B\\x5C"+c+"]|\\x5C"+g+"|\\x5B(?:[^\\x5C\\x5D"+c+"]|\\x5C"+g+")*(?:\\x5D|$))+/")+")")])}(c=a.types)&&f.push(["typ",c]);c=(""+a.keywords).replace(/^ | $/g,"");c.length&&f.push(["kwd",
    new RegExp("^(?:"+c.replace(/[\s,]+/g,"|")+")\\b"),null]);d.push(["pln",/^\s+/,null," \r\n\t\u00a0"]);c="^.[^\\s\\w.$@'\"`/\\\\]*";a.regexLiterals&&(c+="(?!s*/)");f.push(["lit",/^@[a-z_$][a-z_$@0-9]*/i,null],["typ",/^(?:[@_]?[A-Z]+[a-z][A-Za-z_$@0-9]*|\w+_t\b)/,null],["pln",/^[a-z_$][a-z_$@0-9]*/i,null],["lit",/^(?:0x[a-f0-9]+|(?:\d(?:_\d+)*\d*(?:\.\d*)?|\.\d\+)(?:e[+\-]?\d+)?)[a-z]*/i,null,"0123456789"],["pln",/^\\[\s\S]?/,null],["pun",new RegExp(c),null]);return E(d,f)}function B(a,d,f){function c(a){var b=
    a.nodeType;if(1==b&&!r.test(a.className))if("br"===a.nodeName.toLowerCase())g(a),a.parentNode&&a.parentNode.removeChild(a);else for(a=a.firstChild;a;a=a.nextSibling)c(a);else if((3==b||4==b)&&f){var e=a.nodeValue,d=e.match(n);d&&(b=e.substring(0,d.index),a.nodeValue=b,(e=e.substring(d.index+d[0].length))&&a.parentNode.insertBefore(q.createTextNode(e),a.nextSibling),g(a),b||a.parentNode.removeChild(a))}}function g(a){function c(a,b){var e=b?a.cloneNode(!1):a,p=a.parentNode;if(p){var p=c(p,1),d=a.nextSibling;
    p.appendChild(e);for(var f=d;f;f=d)d=f.nextSibling,p.appendChild(f)}return e}for(;!a.nextSibling;)if(a=a.parentNode,!a)return;a=c(a.nextSibling,0);for(var e;(e=a.parentNode)&&1===e.nodeType;)a=e;b.push(a)}for(var r=/(?:^|\s)nocode(?:\s|$)/,n=/\r\n?|\n/,q=a.ownerDocument,k=q.createElement("li");a.firstChild;)k.appendChild(a.firstChild);for(var b=[k],t=0;t<b.length;++t)c(b[t]);d===(d|0)&&b[0].setAttribute("value",d);var l=q.createElement("ol");l.className="linenums";d=Math.max(0,d-1|0)||0;for(var t=
    0,u=b.length;t<u;++t)k=b[t],k.className="L"+(t+d)%10,k.firstChild||k.appendChild(q.createTextNode("\u00a0")),l.appendChild(k);a.appendChild(l)}function n(a,d){for(var f=d.length;0<=--f;){var c=d[f];V.hasOwnProperty(c)?Q.console&&console.warn("cannot override language handler %s",c):V[c]=a}}function F(a,d){a&&V.hasOwnProperty(a)||(a=/^\s*</.test(d)?"default-markup":"default-code");return V[a]}function H(a){var d=a.j;try{var f=l(a.h,a.l),c=f.a;a.a=c;a.c=f.c;a.i=0;F(d,c)(a);var g=/\bMSIE\s(\d+)/.exec(navigator.userAgent),
    g=g&&8>=+g[1],d=/\n/g,r=a.a,k=r.length,f=0,q=a.c,n=q.length,c=0,b=a.g,t=b.length,v=0;b[t]=k;var u,e;for(e=u=0;e<t;)b[e]!==b[e+2]?(b[u++]=b[e++],b[u++]=b[e++]):e+=2;t=u;for(e=u=0;e<t;){for(var x=b[e],z=b[e+1],w=e+2;w+2<=t&&b[w+1]===z;)w+=2;b[u++]=x;b[u++]=z;e=w}b.length=u;var h=a.h;a="";h&&(a=h.style.display,h.style.display="none");try{for(;c<n;){var m=q[c+2]||k,p=b[v+2]||k,w=Math.min(m,p),C=q[c+1],G;if(1!==C.nodeType&&(G=r.substring(f,w))){g&&(G=G.replace(d,"\r"));C.nodeValue=G;var Z=C.ownerDocument,
    W=Z.createElement("span");W.className=b[v+1];var B=C.parentNode;B.replaceChild(W,C);W.appendChild(C);f<m&&(q[c+1]=C=Z.createTextNode(r.substring(w,m)),B.insertBefore(C,W.nextSibling))}f=w;f>=m&&(c+=2);f>=p&&(v+=2)}}finally{h&&(h.style.display=a)}}catch(y){Q.console&&console.log(y&&y.stack||y)}}var Q="undefined"!==typeof window?window:{},J=["break,continue,do,else,for,if,return,while"],K=[[J,"auto,case,char,const,default,double,enum,extern,float,goto,inline,int,long,register,restrict,short,signed,sizeof,static,struct,switch,typedef,union,unsigned,void,volatile"],
    "catch,class,delete,false,import,new,operator,private,protected,public,this,throw,true,try,typeof"],R=[K,"alignas,alignof,align_union,asm,axiom,bool,concept,concept_map,const_cast,constexpr,decltype,delegate,dynamic_cast,explicit,export,friend,generic,late_check,mutable,namespace,noexcept,noreturn,nullptr,property,reinterpret_cast,static_assert,static_cast,template,typeid,typename,using,virtual,where"],L=[K,"abstract,assert,boolean,byte,extends,finally,final,implements,import,instanceof,interface,null,native,package,strictfp,super,synchronized,throws,transient"],
    M=[K,"abstract,add,alias,as,ascending,async,await,base,bool,by,byte,checked,decimal,delegate,descending,dynamic,event,finally,fixed,foreach,from,get,global,group,implicit,in,interface,internal,into,is,join,let,lock,null,object,out,override,orderby,params,partial,readonly,ref,remove,sbyte,sealed,select,set,stackalloc,string,select,uint,ulong,unchecked,unsafe,ushort,value,var,virtual,where,yield"],K=[K,"abstract,async,await,constructor,debugger,enum,eval,export,from,function,get,import,implements,instanceof,interface,let,null,of,set,undefined,var,with,yield,Infinity,NaN"],
    N=[J,"and,as,assert,class,def,del,elif,except,exec,finally,from,global,import,in,is,lambda,nonlocal,not,or,pass,print,raise,try,with,yield,False,True,None"],O=[J,"alias,and,begin,case,class,def,defined,elsif,end,ensure,false,in,module,next,nil,not,or,redo,rescue,retry,self,super,then,true,undef,unless,until,when,yield,BEGIN,END"],J=[J,"case,done,elif,esac,eval,fi,function,in,local,set,then,until"],P=/^(DIR|FILE|array|vector|(de|priority_)?queue|(forward_)?list|stack|(const_)?(reverse_)?iterator|(unordered_)?(multi)?(set|map)|bitset|u?(int|float)\d*)\b/,
    S=/\S/,T=v({keywords:[R,M,L,K,"caller,delete,die,do,dump,elsif,eval,exit,foreach,for,goto,if,import,last,local,my,next,no,our,print,package,redo,require,sub,undef,unless,until,use,wantarray,while,BEGIN,END",N,O,J],hashComments:!0,cStyleComments:!0,multiLineStrings:!0,regexLiterals:!0}),V={};n(T,["default-code"]);n(E([],[["pln",/^[^<?]+/],["dec",/^<!\w[^>]*(?:>|$)/],["com",/^<\!--[\s\S]*?(?:-\->|$)/],["lang-",/^<\?([\s\S]+?)(?:\?>|$)/],["lang-",/^<%([\s\S]+?)(?:%>|$)/],["pun",/^(?:<[%?]|[%?]>)/],["lang-",
    /^<xmp\b[^>]*>([\s\S]+?)<\/xmp\b[^>]*>/i],["lang-js",/^<script\b[^>]*>([\s\S]*?)(<\/script\b[^>]*>)/i],["lang-css",/^<style\b[^>]*>([\s\S]*?)(<\/style\b[^>]*>)/i],["lang-in.tag",/^(<\/?[a-z][^<>]*>)/i]]),"default-markup htm html mxml xhtml xml xsl".split(" "));n(E([["pln",/^[\s]+/,null," \t\r\n"],["atv",/^(?:\"[^\"]*\"?|\'[^\']*\'?)/,null,"\"'"]],[["tag",/^^<\/?[a-z](?:[\w.:-]*\w)?|\/?>$/i],["atn",/^(?!style[\s=]|on)[a-z](?:[\w:-]*\w)?/i],["lang-uq.val",/^=\s*([^>\'\"\s]*(?:[^>\'\"\s\/]|\/(?=\s)))/],
    ["pun",/^[=<>\/]+/],["lang-js",/^on\w+\s*=\s*\"([^\"]+)\"/i],["lang-js",/^on\w+\s*=\s*\'([^\']+)\'/i],["lang-js",/^on\w+\s*=\s*([^\"\'>\s]+)/i],["lang-css",/^style\s*=\s*\"([^\"]+)\"/i],["lang-css",/^style\s*=\s*\'([^\']+)\'/i],["lang-css",/^style\s*=\s*([^\"\'>\s]+)/i]]),["in.tag"]);n(E([],[["atv",/^[\s\S]+/]]),["uq.val"]);n(v({keywords:R,hashComments:!0,cStyleComments:!0,types:P}),"c cc cpp cxx cyc m".split(" "));n(v({keywords:"null,true,false"}),["json"]);n(v({keywords:M,hashComments:!0,cStyleComments:!0,
    verbatimStrings:!0,types:P}),["cs"]);n(v({keywords:L,cStyleComments:!0}),["java"]);n(v({keywords:J,hashComments:!0,multiLineStrings:!0}),["bash","bsh","csh","sh"]);n(v({keywords:N,hashComments:!0,multiLineStrings:!0,tripleQuotedStrings:!0}),["cv","py","python"]);n(v({keywords:"caller,delete,die,do,dump,elsif,eval,exit,foreach,for,goto,if,import,last,local,my,next,no,our,print,package,redo,require,sub,undef,unless,until,use,wantarray,while,BEGIN,END",hashComments:!0,multiLineStrings:!0,regexLiterals:2}),
    ["perl","pl","pm"]);n(v({keywords:O,hashComments:!0,multiLineStrings:!0,regexLiterals:!0}),["rb","ruby"]);n(v({keywords:K,cStyleComments:!0,regexLiterals:!0}),["javascript","js","ts","typescript"]);n(v({keywords:"all,and,by,catch,class,else,extends,false,finally,for,if,in,is,isnt,loop,new,no,not,null,of,off,on,or,return,super,then,throw,true,try,unless,until,when,while,yes",hashComments:3,cStyleComments:!0,multilineStrings:!0,tripleQuotedStrings:!0,regexLiterals:!0}),["coffee"]);n(E([],[["str",/^[\s\S]+/]]),
    ["regex"]);var U=Q.PR={createSimpleLexer:E,registerLangHandler:n,sourceDecorator:v,PR_ATTRIB_NAME:"atn",PR_ATTRIB_VALUE:"atv",PR_COMMENT:"com",PR_DECLARATION:"dec",PR_KEYWORD:"kwd",PR_LITERAL:"lit",PR_NOCODE:"nocode",PR_PLAIN:"pln",PR_PUNCTUATION:"pun",PR_SOURCE:"src",PR_STRING:"str",PR_TAG:"tag",PR_TYPE:"typ",prettyPrintOne:function(a,d,f){f=f||!1;d=d||null;var c=document.createElement("div");c.innerHTML="<pre>"+a+"</pre>";c=c.firstChild;f&&B(c,f,!0);H({j:d,m:f,h:c,l:1,a:null,i:null,c:null,g:null});
    return c.innerHTML},prettyPrint:g=function(a,d){function f(){for(var c=Q.PR_SHOULD_USE_CONTINUATION?b.now()+250:Infinity;t<r.length&&b.now()<c;t++){for(var d=r[t],k=h,n=d;n=n.previousSibling;){var q=n.nodeType,l=(7===q||8===q)&&n.nodeValue;if(l?!/^\??prettify\b/.test(l):3!==q||/\S/.test(n.nodeValue))break;if(l){k={};l.replace(/\b(\w+)=([\w:.%+-]+)/g,function(a,b,c){k[b]=c});break}}n=d.className;if((k!==h||u.test(n))&&!e.test(n)){q=!1;for(l=d.parentNode;l;l=l.parentNode)if(w.test(l.tagName)&&l.className&&
    u.test(l.className)){q=!0;break}if(!q){d.className+=" prettyprinted";q=k.lang;if(!q){var q=n.match(v),A;!q&&(A=z(d))&&D.test(A.tagName)&&(q=A.className.match(v));q&&(q=q[1])}if(x.test(d.tagName))l=1;else var l=d.currentStyle,y=g.defaultView,l=(l=l?l.whiteSpace:y&&y.getComputedStyle?y.getComputedStyle(d,null).getPropertyValue("white-space"):0)&&"pre"===l.substring(0,3);y=k.linenums;(y="true"===y||+y)||(y=(y=n.match(/\blinenums\b(?::(\d+))?/))?y[1]&&y[1].length?+y[1]:!0:!1);y&&B(d,y,l);H({j:q,h:d,m:y,
    l:l,a:null,i:null,c:null,g:null})}}}t<r.length?Q.setTimeout(f,250):"function"===typeof a&&a()}for(var c=d||document.body,g=c.ownerDocument||document,c=[c.getElementsByTagName("pre"),c.getElementsByTagName("code"),c.getElementsByTagName("xmp")],r=[],k=0;k<c.length;++k)for(var n=0,l=c[k].length;n<l;++n)r.push(c[k][n]);var c=null,b=Date;b.now||(b={now:function(){return+new Date}});var t=0,v=/\blang(?:uage)?-([\w.]+)(?!\S)/,u=/\bprettyprint\b/,e=/\bprettyprinted\b/,x=/pre|xmp/i,D=/^code$/i,w=/^(?:pre|code|xmp)$/i,
    h={};f()}},R=Q.define;"function"===typeof R&&R.amd&&R("google-code-prettify",[],function(){return U})})();return g}();S||k.setTimeout(T,0)})();}()}, 100);