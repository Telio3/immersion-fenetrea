let urlcourante = document.location.href; 
let url = new URL(urlcourante);

let type = url.searchParams.get("type");
let parametre = url.searchParams.get("param");
let taille = url.searchParams.get("taille");
let texte = url.searchParams.get("texte");

if (window.location.search.indexOf('type') > 0) {
    if (window.location.search.indexOf('param') > 0) {
        WebService();
    }
    else {tuto.style.display = "block";}
}
else {tuto.style.display = "block";}

function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); } 

function WebService() {
    if (window.location.search.indexOf('?') < 0) {
        tuto.style.display = "block";
    }
    else if (isNumber(taille) == true || taille == null) {
        let tuto = document.getElementById('tuto');

        if (type.toUpperCase() == 'DM') {
            tuto.style.display = "none";
            if (texte == "non"|| texte == null) {
                const DataDM = () => {
                    let barcode = new ej.barcodegenerator.DataMatrixGenerator
                    (
                        {
                            width: '200px',
                            height: '150px',
                            displayText: { visibility: false },
                            mode: 'SVG',
                            value: parametre,      
                        }
                    );
                    barcode.appendTo('#ask_div');
                };
                DataDM();
            }
            else if (texte == "oui") {
                const DataDM = () => {
                    let barcode = new ej.barcodegenerator.DataMatrixGenerator
                    (
                        {
                            width: '200px',
                            height: '150px',
                            displayText: { visibility: true },
                            mode: 'SVG',
                            value: parametre,
                        }
                    );
                    barcode.appendTo('#ask_div');
                };
                DataDM();
            }
            else {tuto.style.display = "block"; document.getElementById('ask_div').innerHTML = "Erreur de saisie lors de l'affichage du texte"}
            
            setTimeout(function(){let svg = document.querySelector( "svg" );
                let svgData = new XMLSerializer().serializeToString( svg );

                let canvas = document.createElement( "canvas" );
                let ctx = canvas.getContext( "2d" );
                let svgSize = svg.getBoundingClientRect();
                    canvas.width = svgSize.width;
                    canvas.height = svgSize.height;

                let img = document.createElement( "img" );
                img.setAttribute( "src", "data:image/svg+xml;base64," + btoa( svgData ) );

                img.onload = function() {
                    ctx.drawImage( img, 0, 0 );
                
                document.querySelector('#ask_div').innerHTML = '<img width="'+taille+'px" src="'+canvas.toDataURL( "image/png" )+'"/>';
            };}, 100);
        }
        else if (type.toUpperCase() == 'QC') {
            tuto.style.display = "none";
            const qrCode = new QRCodeStyling({
                width: 150,
                height: 150,
                data: parametre,
                image: "",
                dotsOptions: { 
                color: "#000",
                type: "square"
                },
            });

            qrCode.append(document.getElementById('ask_div'));
            document.querySelector('canvas').id = "canvas";

            let canvas = document.getElementById("canvasText");
            setTimeout(function(){let context = canvas.getContext('2d');
            if (texte == "non"|| texte == null) {
                context.drawImage(document.getElementById('canvas'), 0, 0);
                cropImageFromCanvas(context);

                let img = canvas.toDataURL("image/png");
                document.write('<img width="'+taille+'"px src="'+img+'"/>');
            }
            else if (texte == "oui") {
                context.drawImage(document.getElementById('canvas'), 0, 0);
                context.fillStyle = "black";
                context.font = "bold 15px -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif";
                context.fillText(parametre, 0, 170);
                cropImageFromCanvas(context);

                let img = canvas.toDataURL("image/png");
                document.write('<img width="'+taille+'"px src="'+img+'"/>');
            }
            else if (texte != "non" || texte != null || texte != "oui") {tuto.style.display = "block"; document.getElementById('ask_div').innerHTML = "Erreur de saisie lors de l'affichage du texte"}}, 100);
        }
        else if (type.toUpperCase() == 'CB39') {
            tuto.style.display = "none";
            if (texte == "non"|| texte == null) {
                JsBarcode("#ask_img", parametre, {
                    format: "CODE39",
                    displayValue: false
                });
                document.getElementById('ask_img').style.width = taille+"px";
            }
            else if (texte == "oui") {
                JsBarcode ( "#ask_img" ,  parametre ,  { 
                    format : "CODE39" , 
                    displayValue : true 
                } ) ;
                document.getElementById('ask_img').style.width = taille+"px";
            }
            else if (texte != "non" || texte != null || texte != "oui") {tuto.style.display = "block"; document.getElementById('ask_div').innerHTML = "Erreur de saisie lors de l'affichage du texte"}
        }
        else if (type.toUpperCase() == 'CB128') {
            tuto.style.display = "none";
            if (texte == "non"|| texte == null) {
                JsBarcode("#ask_img", parametre, {
                    format: "CODE128",
                    displayValue: false
                });
                document.getElementById('ask_img').style.width = taille+"px";
            }
            else if (texte == "oui") {
                JsBarcode ( "#ask_img" ,  parametre ,  { 
                    format : "CODE128" , 
                    displayValue : true 
                } ) ;
                document.getElementById('ask_img').style.width = taille+"px";
            }
            else if (texte != "non" || texte != null || texte != "oui") {tuto.style.display = "block"; document.getElementById('ask_div').innerHTML = "Erreur de saisie lors de l'affichage du texte"}
        }
        else if (type.toUpperCase() == 'EAN13') {
            tuto.style.display = "none";
            parseInt(parametre);
            if (parametre.length == 12) {
                if (texte == "non"|| texte == null) {
                    JsBarcode("#ask_img", parametre, {
                        format: "EAN13",
                        displayValue: false
                    });
                    document.getElementById('ask_img').style.width = taille+"px";
                }
                else if (texte == "oui") {
                    JsBarcode ( "#ask_img" ,  parametre ,  { 
                        format : "EAN13" , 
                        displayValue : true 
                    } ) ;
                    document.getElementById('ask_img').style.width = taille+"px";
                }
                else if (texte != "non" || texte != null || texte != "oui") {tuto.style.display = "block"; document.getElementById('ask_div').innerHTML = "Erreur de saisie lors de l'affichage du texte"}
            }
            else{document.getElementById('ask_div').innerHTML = "Erreur de saisie du paramètre :<br><br>12 chiffres requis"}
        }
        else {document.getElementById('ask_div').innerHTML = "Erreur de saisie du type :<br><br>DM : DataMatrix<br>QC : QrCode<br>CB39 : Code Barre 39<br>CB128 : Code Barre 128<br>EAN13 : Code Barre EAN 13"}
    }
    else {document.getElementById('ask_div').innerHTML = "Erreur de saisie de la taille -> seulement les chiffres sont pris en charge"}
}


function cropImageFromCanvas(ctx) {
    let canvas = ctx.canvas, 
      w = canvas.width, h = canvas.height,
      pix = {x:[], y:[]},
      imageData = ctx.getImageData(0,0,canvas.width,canvas.height),
      x, y, index;
  
    for (y = 0; y < h; y++) {
      for (x = 0; x < w; x++) {
        index = (y * w + x) * 4;
        if (imageData.data[index+3] > 0) {
          pix.x.push(x);
          pix.y.push(y);
        } 
      }
    }
    pix.x.sort(function(a,b){return a-b});
    pix.y.sort(function(a,b){return a-b});
    let n = pix.x.length-1;
  
    w = 1 + pix.x[n] - pix.x[0];
    h = 1 + pix.y[n] - pix.y[0];
    let cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);
  
    canvas.width = w;
    canvas.height = h;
    ctx.putImageData(cut, 0, 0);
}